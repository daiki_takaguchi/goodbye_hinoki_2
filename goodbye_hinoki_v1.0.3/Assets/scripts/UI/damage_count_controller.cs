﻿using UnityEngine;
using System.Collections;

public class damage_count_controller : MonoBehaviour {

	public string layer_name;
	public int layer_order;
	public Vector3 initial_position;

	// Use this for initialization
	void Start () {
		renderer.sortingLayerName=layer_name;
		renderer.sortingOrder = layer_order;
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Vector3.Distance(initial_position,transform.position)>20){
			Destroy (gameObject);
		}
	}

	public void set_text(string text,Vector3 position){
		GetComponent<TextMesh>().text =text;

		transform.position = position;
		initial_position = position;

		rigidbody2D.velocity = new Vector3 (0, 30, 0);



	}


}
