﻿using UnityEngine;
using System.Collections;

public class text_mesh_controller : MonoBehaviour {

	public string layer_name;
	public bool is_all_text_shown=false;

	//public string text;
	public string all_text;

	public string text_already_shown;
	public string text_to_show;

	public string text_animation;
	public int text_index = 0;



	public bool is_animation_enabled=false;

	public Animator animator;
	// Use this for initialization
	void Start () {
		renderer.sortingLayerName=layer_name;
		renderer.sortingOrder = 0;

		//set_text ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void set_layer(string input_layer_name){
		layer_name = input_layer_name;
		renderer.sortingLayerName=layer_name;
	}

	public void set_text(string input){
		//window = input_window;

		//text_to_show = input;
		GetComponent<TextMesh>().text =input;
		text_animation = input;
		//renderer.enabled = true;
		is_all_text_shown = true;

	}

	public void set_text_animation(string already_shown,string input){

		is_animation_enabled = true;
		is_all_text_shown=false;

		text_to_show = input;
		text_animation = already_shown;
		text_already_shown = already_shown;

		text_index = 0;
		GetComponent<TextMesh> ().text = "";




		//GetComponent<TextMesh> ().text = already_shown;
		//renderer.enabled=false;

		/*

		if(!animator.enabled){
			renderer.enabled=false;
			GetComponent<TextMesh> ().text = already_shown;
		}
		*/
	}

	public void show_all_text(){
		if (is_animation_enabled) {
			GetComponent<TextMesh>().text =text_already_shown+text_to_show;
		}

		animator.enabled = false;
		is_all_text_shown = true;
	}


	public void next_text_animation(){

		if(text_index<text_to_show.Length){
			text_animation+=text_to_show[text_index].ToString();
			GetComponent<TextMesh>().text =text_animation;
			text_index++;

			GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("text");
		}else{

			animator.enabled=false;
			is_all_text_shown = true;
		}

	}

	public void enable_animation(){
		//if(is_animation_enabled){
			GetComponent<TextMesh> ().text = text_animation;
			renderer.enabled = true;
			animator.enabled = true;
		//}
	}

}
/*

public class one_text : MonoBehaviour{
	
	public int text_pointer;
	public string loaded_text;
	public string shown_text;
	public GUIText shown_gui_text;
	
	public void set_text(string input_text){
		loaded_text = input_text;
		shown_text = "";
		text_pointer = 0;
		
		//print(loaded_text.Length);
	}
	
	public bool next_char(){
		
		bool is_end = false;
		
		//print ("ne");
		//print (loaded_text.Length);
		if (loaded_text.Length > text_pointer) {
			
			shown_text += loaded_text[text_pointer].ToString();
			text_pointer++;
			
		}else{
			is_end=true;
		}
		
		
		
		return is_end;
	}
}
*/
