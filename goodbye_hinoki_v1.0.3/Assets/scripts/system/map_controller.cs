﻿using UnityEngine;
using System.Collections;

public class map_controller : MonoBehaviour {
	
	// Use this for initialization

	public int stage_cleared;
	public int stage_reached;
	public int map_no;
	public bool is_test;
	public string total_exp_for_test;
	public int money_for_test;
	public int stage_cleared_for_test;
	public int map_no_for_test;
	public string[] test_swords;
	public save_data_controller save_data;

	//public int temp= 2147483647;

	void Start () {
		//GameObject.Find ("controller").GetComponent<save_data_controller> ().initialize ();
		//PlayerPrefs.DeleteKey("map_test");
		//GameObject.Find ("controller").GetComponent<save_data_controller> ().get_item ("power_up_1");

		//temp++;

		//PlayerPrefs.DeleteKey ("map_006_talk");

		if (save_data.HasKey ("game_over")) {
			save_data.DeleteKey("game_over");

			NendAdInterstitial.Instance.Show ();
		}


		map_no = current_map ();
	
		//save_data.power_up_sword (0,1000000);

		if(PlayerPrefs.HasKey("map_test")){
			is_test=false;
			/*
			GameObject.Find ("controller").GetComponent<save_data_controller> ().initialize_for_test(total_exp_for_test,stage_cleared_for_test);
			for (int i=0;i<test_swords.Length; i++){
				GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword (test_swords[i],0);
			}*/
		}

		if (is_test) {
			GameObject.Find ("controller").GetComponent<save_data_controller> ().initialize_for_test(total_exp_for_test,money_for_test,stage_cleared_for_test);
			map_no=map_no_for_test;
			for (int i=0;i<test_swords.Length; i++){
				GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword (test_swords[i],0);
			}
			PlayerPrefs.SetInt("map_test",1);
		}else{
			//PlayerPrefs.DeleteKey("map_test");
		}






		GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM("map_"+map_no);



		//GameObject.Find ("BGM").GetComponent<BGM_controller> ().set_BGM ("map_"+map_no);
		//GameObject.Find ("BGM").GetComponent<BGM_controller> ().play_BGM ();


		GameObject.Find ("map_cube").GetComponent<map_cube_controller> ().create_front_map (map_no);
		GameObject.Find ("UI").GetComponent<UI_controller> ().map_shown = map_no;

		GameObject.Find ("UI").GetComponent<UI_controller> ().max_map_no = max_map ();



		refresh_hinoki ();

		if(save_data.get_stage_cleared()<5){
			Destroy(GameObject.Find ("button_item"));
			//Destroy(GameObject.Find ("button_shop"));
		}

		//PlayerPrefs.DeleteKey ("is_shop_open");
		if(save_data.get_stage_cleared()<11){
			Destroy(GameObject.Find ("button_shop"));
		}

		//window.set_text_auto_position("tutorial_1");
		//GameObject.Find ("text_mesh(Clone)").GetComponent<TextMesh> ().text = "そうびする　　　　　　　　　せつめい\n\n　　　　　　　　　　　　　　もどる";

	}

	public void refresh_hinoki(){
		/*
		if (save_data.is_boss_009_cleared()) {
			string temp="sword_001_04";
			GameObject.Find ("map_hinoki").GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.Load("images/sword/" +temp+"/"+temp,typeof(Sprite));

		}else{
			string temp = GameObject.Find ("controller").GetComponent<save_data_controller> ().GetString ("map_hinoki");
			GameObject.Find ("map_hinoki").GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.Load("images/sword/" +temp+"/"+temp,typeof(Sprite));
		}
		*/

		string temp = GameObject.Find ("controller").GetComponent<save_data_controller> ().GetString ("map_hinoki");
		GameObject.Find ("map_hinoki").GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.Load("images/sword/" +temp+"/"+temp,typeof(Sprite));
	
	}

	public int max_map(){
		
		int output = 0;
		//print (PlayerPrefs.GetInt("stage_cleared"));
		//print (PlayerPrefs.GetInt("stage_cleard"));

		if(PlayerPrefs.GetInt("stage_cleared")>0&&PlayerPrefs.GetInt("stage_cleared")<=31){
				//print ("aaa"+PlayerPrefs.GetInt("stage_no"));
				output= Mathf.CeilToInt((PlayerPrefs.GetInt("stage_cleared")+1)/4.0f);

		}else if(PlayerPrefs.GetInt("stage_cleared")==0){
			output=1;
		}else if(PlayerPrefs.GetInt("stage_cleared")>=32){
			output=8;
		}
		//print (output);
		return output;
	}

	public int current_map(){
		
		int output = 1;
		//print (PlayerPrefs.GetInt("stage_no"));
		if (PlayerPrefs.HasKey ("shop_stage_no")) {
			save_data.SetInt ("stage_no",save_data.GetInt("shop_stage_no"));
			save_data.DeleteKey("shop_stage_no");
		}


		if(PlayerPrefs.HasKey ("stage_no")){
			if(PlayerPrefs.GetInt("stage_no")>0&&PlayerPrefs.GetInt("stage_no")<=32){
				//print ("aaa"+PlayerPrefs.GetInt("stage_no"));
				output= Mathf.CeilToInt(PlayerPrefs.GetInt("stage_no") /4.0f);
			}
		}else{
			//output= Mathf.CeilToInt(save_data.get_stage_cleared() /4.0f);
		}
		//print (output);
		return output;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnApplicationQuit(){
		PlayerPrefs.DeleteKey("map_test");
	}


}
