﻿using UnityEngine;
using System.Collections;

public class tutorial_sword_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y <= 0) {
			rigidbody.velocity=new Vector3(0,0,0);
			//Destroy (transform.FindChild ("par2").gameObject);
		}
	}

	void OnTriggerEnter (Collider c){

		if(c.tag=="hero"){
		
			GameObject.Find ("hero").GetComponent<hero_controller> ().initialize_hero();
			GameObject.Find ("BGM").GetComponent<BGM_controller>().play_BGM();

			Transform temp =GameObject.Find ("enemy_001").transform.FindChild ("HP_bar").transform;
			temp.FindChild ("red").renderer.enabled=true;
			temp.FindChild ("green").renderer.enabled=true;
			
			temp =GameObject.Find ("enemy_002").transform.FindChild ("HP_bar").transform; 
			temp.FindChild ("red").renderer.enabled=true;
			temp.FindChild ("green").renderer.enabled=true;
			
			temp =GameObject.Find ("enemy_003").transform.FindChild ("HP_bar").transform; 
			temp.FindChild ("red").renderer.enabled=true;
			temp.FindChild ("green").renderer.enabled=true;
			Destroy (GameObject.Find ("tutorial_sword").gameObject);

		}
	}
}
