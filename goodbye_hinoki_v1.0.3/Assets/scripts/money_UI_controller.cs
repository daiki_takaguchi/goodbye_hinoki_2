﻿using UnityEngine;
using System.Collections;

public class money_UI_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
		refresh_money ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void refresh_money(){
		transform.FindChild ("money").GetComponent<TextMesh> ().text = 
			GameObject.Find("controller").GetComponent<save_data_controller>().get_money()+"G";
	}
}
