﻿using UnityEngine;
using System.Collections;

public class enemy_data_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public long get_max_HP(string enemy_no){
		return long.Parse(get_data(enemy_no,"max_HP"));
	}

	public long get_enemy_exp(string enemy_no){
		return long.Parse(get_data(enemy_no,"enemy_exp"));
	}

	public int get_enemy_money(string enemy_no){
		return Mathf.RoundToInt(Random.Range(0.8f,1.2f)* Mathf.Sqrt((float) int.Parse(get_data(enemy_no,"enemy_exp"))));
	}

	public int get_attack_point(string enemy_no){
		return int.Parse(get_data(enemy_no,"attack_point"));
	}

	public int get_defence_point(string enemy_no){
		return int.Parse(get_data(enemy_no,"defence_point"));
	}

	public string get_data(string enemy_no, string type){
		string data="0";
		/*
		if(type=="enemy_exp"){
			data="10";
		}else if(type=="max_HP"){
			data="20";
		}else if(type=="attack_point"){
			data="5";
		}else if(type=="defence_point"){
			data="9";
		}
		*/

		if (enemy_no == "enemy_001") {
			if(type=="enemy_exp"){
				data="4";
			}else if(type=="enemy_money"){
				data="10";
			}else if(type=="max_HP"){
				data="200";
			}else if(type=="attack_point"){
				data="5";
			}else if(type=="defence_point"){
				data="8";
			}
		}else if (enemy_no == "enemy_002") {
			if(type=="enemy_exp"){
				data="6";
			}else if(type=="max_HP"){
				data="250";
			}else if(type=="attack_point"){
				data="5";
			}else if(type=="defence_point"){
				data="9";
			}
		}else if (enemy_no == "enemy_003") {
			if(type=="enemy_exp"){
				data="16";
			}else if(type=="max_HP"){
				data="1500";
			}else if(type=="attack_point"){
				data="12";
			}else if(type=="defence_point"){
				data="11";
			}
		}else if (enemy_no == "enemy_004") {
			if(type=="enemy_exp"){
				data="14";
			}else if(type=="max_HP"){
				data="1000";
			}else if(type=="attack_point"){
				data="10";
			}else if(type=="defence_point"){
				data="9";
			}
		}else if (enemy_no == "enemy_005") {
			if(type=="enemy_exp"){
				data="10";
			}else if(type=="max_HP"){
				data="800";
			}else if(type=="attack_point"){
				data="10";
			}else if(type=="defence_point"){
				data="20";
			}
		}else if (enemy_no == "enemy_006") {
			if(type=="enemy_exp"){
				data="8";
			}else if(type=="max_HP"){
				data="1000";
			}else if(type=="attack_point"){
				data="7";
			}else if(type=="defence_point"){
				data="20";
			}
		}else if (enemy_no == "enemy_007") {
			if(type=="enemy_exp"){
				data="90";
			}else if(type=="max_HP"){
				data="6000";
			}else if(type=="attack_point"){
				data="24";
			}else if(type=="defence_point"){
				data="20";
			}
		}else if (enemy_no == "enemy_008") {
			if(type=="enemy_exp"){
				data="450";
			}else if(type=="max_HP"){
				data="35000";
			}else if(type=="attack_point"){
				data="39";
			}else if(type=="defence_point"){
				data="22";
			}

		}else if (enemy_no == "enemy_009") {
			if(type=="enemy_exp"){
				data="16";
			}else if(type=="max_HP"){
				data="1500";
			}else if(type=="attack_point"){
				data="20";
			}else if(type=="defence_point"){
				data="20";
			}
		}else if (enemy_no == "enemy_010") {
			if(type=="enemy_exp"){
				data="210";
			}else if(type=="max_HP"){
				data="8000";
			}else if(type=="attack_point"){
				data="72";
			}else if(type=="defence_point"){
				data="22";
			}


		}else if (enemy_no == "enemy_011") {
			if(type=="enemy_exp"){
				data="1";
			}else if(type=="max_HP"){
				data="1200";
			}else if(type=="attack_point"){
				data="20";
			}else if(type=="defence_point"){
				data="20";
			}
		}else if (enemy_no == "enemy_012") {
			if(type=="enemy_exp"){
				data="86";
			}else if(type=="max_HP"){
				data="6000";
			}else if(type=="attack_point"){
				data="24";
			}else if(type=="defence_point"){
				data="22";
			}
		}else if (enemy_no == "enemy_013") {
			if(type=="enemy_exp"){
				data="35";
			}else if(type=="max_HP"){
				data="4000";
			}else if(type=="attack_point"){
				data="18";
			}else if(type=="defence_point"){
				data="18";
			}
		}else if (enemy_no == "enemy_014") {
			if(type=="enemy_exp"){
				data="28";
			}else if(type=="max_HP"){
				data="2000";
			}else if(type=="attack_point"){
				data="12";
			}else if(type=="defence_point"){
				data="22";
			}
		}else if (enemy_no == "enemy_015") {
			if(type=="enemy_exp"){
				data="30";
			}else if(type=="max_HP"){
				data="2500";
			}else if(type=="attack_point"){
				data="25";
			}else if(type=="defence_point"){
				data="22";
			}
		}else if (enemy_no == "enemy_016") {
			if(type=="enemy_exp"){
				data="48";
			}else if(type=="max_HP"){
				data="4000";
			}else if(type=="attack_point"){
				data="18";
			}else if(type=="defence_point"){
				data="22";
			}
		}else if (enemy_no == "enemy_017") {
			if(type=="enemy_exp"){
				data="12";
			}else if(type=="max_HP"){
				data="1200";
			}else if(type=="attack_point"){
				data="7";
			}else if(type=="defence_point"){
				data="20";
			}

		}else if (enemy_no == "enemy_018") {
			if(type=="enemy_exp"){
				data="82";
			}else if(type=="max_HP"){
				data="6000";
			}else if(type=="attack_point"){
				data="18";
			}else if(type=="defence_point"){
				data="22";
			}

		}else if (enemy_no == "enemy_019") {
			if(type=="enemy_exp"){
				data="1";
			}else if(type=="max_HP"){
				data="1";
			}else if(type=="attack_point"){
				data="1";
			}else if(type=="defence_point"){
				data="20";
			}
		}else if (enemy_no == "enemy_020") {
			if(type=="enemy_exp"){
				data="25";
			}else if(type=="max_HP"){
				data="3000";
			}else if(type=="attack_point"){
				data="30";
			}else if(type=="defence_point"){
				data="20";
			}

		}else if (enemy_no == "enemy_021") {
			if(type=="enemy_exp"){
				data="30";
			}else if(type=="max_HP"){
				data="3000";
			}else if(type=="attack_point"){
				data="30";
			}else if(type=="defence_point"){
				data="22";
			}

		}else if (enemy_no == "enemy_022") {
			if(type=="enemy_exp"){
				data="52";
			}else if(type=="max_HP"){
				data="5000";
			}else if(type=="attack_point"){
				data="18";
			}else if(type=="defence_point"){
				data="22";
			}

		}else if (enemy_no == "enemy_023") {
			if(type=="enemy_exp"){
				data="64";
			}else if(type=="max_HP"){
				data="4500";
			}else if(type=="attack_point"){
				data="28";
			}else if(type=="defence_point"){
				data="22";
			}

		}else if (enemy_no == "enemy_024") {
			if(type=="enemy_exp"){
				data="80";
			}else if(type=="max_HP"){
				data="20000";
			}else if(type=="attack_point"){
				data="24";
			}else if(type=="defence_point"){
				data="22";
			}

		}else if (enemy_no == "enemy_025") {
			if(type=="enemy_exp"){
				data="700";
			}else if(type=="max_HP"){
				data="80000";
			}else if(type=="attack_point"){
				data="43";
			}else if(type=="defence_point"){
				data="22";
			}
		}else if (enemy_no == "enemy_026") {
			if(type=="enemy_exp"){
				data="120";
			}else if(type=="max_HP"){
				data="5000";
			}else if(type=="attack_point"){
				data="52";
			}else if(type=="defence_point"){
				data="22";
			}

		}else if (enemy_no == "enemy_027") {
			if(type=="enemy_exp"){
				data="230";
			}else if(type=="max_HP"){
				data="12000";
			}else if(type=="attack_point"){
				data="36";
			}else if(type=="defence_point"){
				data="22";
			}
		}else if (enemy_no == "enemy_028") {
			if(type=="enemy_exp"){
				data=GameObject.Find ("controller").GetComponent<level_data_controller>().get_next_level_up_exp().ToString();
			}else if(type=="max_HP"){
				data=(GameObject.Find("hero").GetComponent<hero_controller>().total_attack_point()*20).ToString();
			}else if(type=="attack_point"){
				data=GameObject.Find("hero").GetComponent<hero_controller>().max_HP.ToString();
			}else if(type=="defence_point"){
				data="20";
			}
		}else if (enemy_no == "enemy_029") {
			if(type=="enemy_exp"){
				data="80";
			}else if(type=="max_HP"){
				data="20000";
			}else if(type=="attack_point"){
				data="24";
			}else if(type=="defence_point"){
				data="22";
			}
		}else if (enemy_no == "boss_001") {
			if(type=="enemy_exp"){
				data="20";
			}else if(type=="max_HP"){
				data="2000";
			}else if(type=="attack_point"){
				data="5";
			}else if(type=="defence_point"){
				data="9";
			}
		

		}else if (enemy_no == "boss_002") {
			if(type=="enemy_exp"){
				data="50";
			}else if(type=="max_HP"){
				data="6000";
			}else if(type=="attack_point"){
				data="10";
			}else if(type=="defence_point"){
				data="9";
			}

		}else if (enemy_no == "boss_003") {
			if(type=="enemy_exp"){
				data="100";
			}else if(type=="max_HP"){
				data="15000";
			}else if(type=="attack_point"){
				data="30";
			}else if(type=="defence_point"){
				data="9";
			}
		}else if (enemy_no == "boss_004") {
			if(type=="enemy_exp"){
				data="200";
			}else if(type=="max_HP"){
				data="40000";
			}else if(type=="attack_point"){
				data="20";
			}else if(type=="defence_point"){
				data="9";
			}
		}else if (enemy_no == "boss_005") {
			if(type=="enemy_exp"){
				data="500";
			}else if(type=="max_HP"){
				data="80000";
			}else if(type=="attack_point"){
				data="28";
			}else if(type=="defence_point"){
				data="9";
			}
		}else if (enemy_no == "boss_006") {
			if(type=="enemy_exp"){
				data="700";
			}else if(type=="max_HP"){
				data="120000";
			}else if(type=="attack_point"){
				data="35";
			}else if(type=="defence_point"){
				data="9";
			}
		}else if (enemy_no == "boss_007") {
			if(type=="enemy_exp"){
				data="1000";
			}else if(type=="max_HP"){
				data="300000";
			}else if(type=="attack_point"){
				data="42";
			}else if(type=="defence_point"){
				data="9";
			}

		}else if (enemy_no == "boss_008") {
			if(type=="enemy_exp"){
				data="10000";
			}else if(type=="max_HP"){
				data="1000000";
			}else if(type=="attack_point"){
				data="44";
			}else if(type=="defence_point"){
				data="9";
			}

		}else if (enemy_no == "boss_009") {
			if(type=="enemy_exp"){
				data="20000";
			}else if(type=="max_HP"){
				data="1500000";
			}else if(type=="attack_point"){
				data="35";
			}else if(type=="defence_point"){
				data="9";
			}

		}else if (enemy_no == "boss_010") {
			if(type=="enemy_exp"){
				data="40000";
			}else if(type=="max_HP"){
				data="2000000";
			}else if(type=="attack_point"){
				data="82";
			}else if(type=="defence_point"){
				data="9";
			}
		}else if (enemy_no == "boss_011") {
			if(type=="enemy_exp"){
				data="100000";
			}else if(type=="max_HP"){
				data="1000000";
			}else if(type=="attack_point"){
				data=(Mathf.RoundToInt(GameObject.Find("hero").GetComponent<hero_controller>().max_HP/4)).ToString();
			}else if(type=="defence_point"){
				data="9";
			}
		}
		

		return data;
	}
}
