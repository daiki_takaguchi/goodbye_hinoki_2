﻿using UnityEngine;
using System.Collections;

public class ice_controller : MonoBehaviour {

	// Use this for initialization

	public Transform ice;

	void Start () {
		//transform.FindChild ("ice").renderer.material.color = new Color (1.0f, 1.0f, 1.0f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void set_ice(int width, int height,Transform input_transform){

		for(int i=0;i<width;i++){

			for(int j=0;j<height;j++){
				int x=i*16-(width-1)*8;
				int y=j*16-(height-1)*8;

				//print (i+" "+j);
				Transform temp= (Transform) Instantiate(ice.transform,input_transform.position+new Vector3(x,y,0),Quaternion.identity);
				temp.parent=transform;

				//print (width+" "+height);

				if(width==1){

					if(height==1){
						temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.Load ("images/etc/koori/ice_1_1",typeof(Sprite));
					}else{
						if(j==0){
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_1_3",typeof(Sprite))[2];
						}else if(j==height-1){
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_1_3",typeof(Sprite))[0];
						}else{
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_1_3",typeof(Sprite))[1];

						}
					}
				}else if(height==1){

					if(width==1){
						temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.Load ("images/etc/koori/ice_1_1",typeof(Sprite));
					}else{
						if(i==0){
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_1",typeof(Sprite))[0];
						}else if(i==width-1){
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_1",typeof(Sprite))[2];
						}else{
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_1",typeof(Sprite))[1];
							
						}
					}

				
				}else{
					if(i==0){
						if(j==0){
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_3",typeof(Sprite))[6];
						}else if(j==height-1){
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_3",typeof(Sprite))[0];
						}else{
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_3",typeof(Sprite))[3];
						}
					}else if(i==width-1){

						if(j==0){
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_3",typeof(Sprite))[8];
						}else if(j==height-1){
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_3",typeof(Sprite))[2];
						}else{
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_3",typeof(Sprite))[5];
						}
					
					}else{
						if(j==0){
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_3",typeof(Sprite))[7];
						}else if(j==height-1){
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_3",typeof(Sprite))[1];
						}else{
							temp.GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.LoadAll ("images/etc/koori/ice_3_3",typeof(Sprite))[4];
							
						}
					}


				}

			}

		}

		ice.gameObject.SetActive (false);
	}
}
