﻿using UnityEngine;
using System.Collections;

public class title_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {

		//GameObject.Find ("text_mesh").GetComponent<TextMesh> ().text = "これまでのプレイデータが\nきえてしまいますが\nよろしいですか？";

		string apiKey = "25de43d2e9ccb105704ad7b99101e060976a65e9";
		string spotId = "242372";

		if(Application.platform == RuntimePlatform.Android){
			apiKey = "061184e295a76de6c6482bb7c60b7f76943d74db";
			spotId = "242374";
		}


		NendAdInterstitial.Instance.Load(apiKey, spotId);

		Transform temp = GameObject.Find ("cloud_1").transform;
		temp.GetComponent<object_controller>().set_screen_limit(
			temp.position.x-80,
			temp.position.x +80,
			temp.position.y-16,
			temp.position.y +16,
			64,
			64);
		temp.rigidbody.velocity = new Vector3 (-0.5f, 0, 0);
		temp.GetComponent<object_controller> ().is_bounse = true;


		temp = GameObject.Find ("cloud_2").transform;
		temp.GetComponent<object_controller>().set_screen_limit(
			temp.position.x-80,
			temp.position.x +80,
			temp.position.y-16,
			temp.position.y +16,
			64,
			64);
		temp.rigidbody.velocity = new Vector3 (0.25f, 0, 0);
		temp.GetComponent<object_controller> ().is_bounse = true;

		temp = GameObject.Find ("cloud_3").transform;
		temp.GetComponent<object_controller>().set_screen_limit(
			temp.position.x-80,
			temp.position.x +80,
			temp.position.y-16,
			temp.position.y +16,
			32,
			32);
		temp.rigidbody.velocity = new Vector3 (0.25f, 0, 0);
		temp.GetComponent<object_controller> ().is_bounse = true;

		temp = GameObject.Find ("cloud_4").transform;
		temp.GetComponent<object_controller>().set_screen_limit(
			temp.position.x-80,
			temp.position.x +80,
			temp.position.y-16,
			temp.position.y +16,
			32,
			32);
		temp.rigidbody.velocity = new Vector3 (0.125f, 0, 0);
		temp.GetComponent<object_controller> ().is_bounse = true;

		temp = GameObject.Find ("cloud_5").transform;
		temp.GetComponent<object_controller>().set_screen_limit(
			temp.position.x-80,
			temp.position.x +80,
			temp.position.y-16,
			temp.position.y +16,
			32,
			32);
		temp.rigidbody.velocity = new Vector3 (0.125f, 0, 0);
		temp.GetComponent<object_controller> ().is_bounse = true;
	}
	
	// Update is called once per frame
	void Update () {
		//Mathf.RoundToInt()
	}
}
