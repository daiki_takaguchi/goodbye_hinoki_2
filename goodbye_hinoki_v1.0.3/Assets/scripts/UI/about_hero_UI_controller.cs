﻿using UnityEngine;
using System.Collections;

public class about_hero_UI_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {


		save_data_controller save_data = GameObject.Find ("controller").GetComponent<save_data_controller> ();

		long total_attack_point = 0;
			

		if(GameObject.Find("hero")!=null){
			total_attack_point =GameObject.Find("hero").GetComponent<hero_controller>().total_attack_point();

		}else{

			total_attack_point = GameObject.Find ("controller").GetComponent<level_data_controller> ().get_attack_point () +
				GameObject.Find ("controller").GetComponent<item_data_controller> ().sword_attack_point (save_data.get_sword_no (save_data.get_sword_equipped_slot ())) +
					save_data.get_power_up (save_data.get_sword_equipped_slot ());

		}


		transform.FindChild ("level").GetComponent<TextMesh> ().text=
			GameObject.Find ("controller").GetComponent<level_data_controller> ().get_level ()
				+"\n　　　　　　　　　";
		transform.FindChild ("money").GetComponent<TextMesh> ().text =
			GameObject.Find ("controller").GetComponent<save_data_controller> ().get_money ()
				+"G\n　　　　　　　　　";

		transform.FindChild ("max_HP").GetComponent<TextMesh> ().text=
			GameObject.Find ("controller").GetComponent<level_data_controller> ().get_max_HP()
				+"\n　　　　　　　　　　　　　　　　　　 ";

		transform.FindChild ("attack_point").GetComponent<TextMesh> ().text =
			GameObject.Find ("controller").GetComponent<level_data_controller> ().get_attack_point()
				+"\n　　　　　　　　　　　　　　　　　　 ";

		transform.FindChild ("total_attack_point").GetComponent<TextMesh> ().text =

				total_attack_point.ToString()
				+"\n　　　　　　　　　　　　　　　　　　 ";

		transform.FindChild ("total_exp").GetComponent<TextMesh> ().text =
			GameObject.Find ("controller").GetComponent<level_data_controller> ().get_total_exp ()
				+"\n　　　　　　　　　　　　　　　　　　 ";

		transform.FindChild ("next_level_up_exp").GetComponent<TextMesh> ().text =
			GameObject.Find ("controller").GetComponent<level_data_controller> ().get_next_level_up_exp ()
				+"\n　　　　　　　　　　　　　　　　　　 ";
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
