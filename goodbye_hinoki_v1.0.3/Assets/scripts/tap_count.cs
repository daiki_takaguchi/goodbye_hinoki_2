﻿using UnityEngine;
using System.Collections;

public class tap_count : MonoBehaviour {

	// Use this for initialization

	public int count=0;
	public float start_time=0;
	public bool is_count_enabled=false;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(is_count_enabled){
			if (Input.GetMouseButtonDown (0)) {
				count++;

			}
		GetComponent<TextMesh>().text=""+count+"\n"+(Time.time-start_time);
		}
	}

	public void enable_count(){
		is_count_enabled = true;
		start_time = Time.time;
	}

	public void disable_count(){
		is_count_enabled = false;
		//start_time = Time.time;
	}
}
