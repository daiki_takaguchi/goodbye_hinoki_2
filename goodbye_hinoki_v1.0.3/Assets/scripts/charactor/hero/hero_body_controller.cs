﻿using UnityEngine;
using System.Collections;

public class hero_body_controller : MonoBehaviour {

	// Use this for initialization
	public hero_controller hero;


	void Start () {
		hero=GameObject.Find ("hero").GetComponent<hero_controller> ();
		gameObject.layer = 11;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider c){
		object_enter (c);
	}

	void OnTriggerStay(Collider c){
		/*
		string input_state = "invincible_start";
		if (c.tag == "enemy") {
			hero.hit(c.GetComponent<enemy_controller>().attack_point,input_state);
			//hero.damage_controller ("invincible_start");
		}
		*/
		object_enter (c);

	}


	public void object_enter(Collider c){

		if (c.tag == "enemy") {
			hero.hit(c.GetComponent<enemy_controller>().attack_point);
			//hero.damage_controller ("invincible_start");
		}else if(c.tag=="enemy_magic"){
			
			if(c.GetComponent<enemy_magic_controller>().type=="boss_005_iceball"&&Random.Range (0,2)==0){
				//input_state="start_freeze";
				GameObject.Find ("hero").GetComponent<hero_controller>().damage_controller("start_freeze");
			}



			hero.hit(c.GetComponent<enemy_magic_controller>().attack_point);
			Destroy (c.gameObject);
			
			
			
		}else if(c.tag=="enemy_magic_remain"){

			if(c.GetComponent<enemy_magic_controller>().type=="enemy_003_tongue"){
				//input_state="start_paralyze";
				GameObject.Find ("hero").GetComponent<hero_controller>().damage_controller("start_paralyze");
			}

			hero.hit(c.GetComponent<enemy_magic_controller>().attack_point);
			//Destroy (c.gameObject);
		}else if(c.tag=="item"){
			
			string input_type=c.GetComponent<item_controller>().type;
			string input_sword_no=c.GetComponent<item_controller>().sword_no;
			
			c.GetComponent<item_controller>().got_item=true;
			
			if(input_type=="coin"){
				GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("coin");
				GameObject.Find("controller").GetComponent<save_data_controller>().AddMoney (c.GetComponent<item_controller>().money);
				
				Transform temp= (Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/text/money_count",  typeof(Transform)) , c.transform.position,Quaternion.identity );
				temp.GetComponent<damage_count_controller> ().set_text(c.GetComponent<item_controller>().money+"G",c.transform.position);
				//print (c.GetComponent<item_controller>().money+"G");
				//GameObject.Find ()
				
			}else if(input_type=="sword"){
				
				
				if(GameObject.Find ("controller").GetComponent<save_data_controller>().add_sword(input_sword_no,0)=="fail"){
					GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("error");
				}else{
					GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("buy");
				};
				
			}
			
			Destroy (c.gameObject);
			
		}
	}
}
