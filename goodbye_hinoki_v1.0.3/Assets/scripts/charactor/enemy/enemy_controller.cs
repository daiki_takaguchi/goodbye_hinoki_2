﻿using UnityEngine;
using System.Collections;

public class enemy_controller : MonoBehaviour {

	public long remaining_HP;
	public long max_HP;
	public long enemy_exp;
	public int enemy_money;

	public int HP_bar_x=0;
	public int HP_bar_y=0;


	public string enemy_no;
	public int attack_point;
	public int sword_drop_level=1;

	public bool is_yellow=false;
	public bool is_red=false;

	public int defence_point;
	public float last_damaged_time;
	public int width;
	public int height;

	public HP_bar_controller HP_bar ;

	public bool is_HP_bar_shown = true;
	public bool is_dead=false;

	public float next_sprite_switch_time;
	public float next_sprite_switch_interval= 0.5f;

	public float next_attack_time=0;
	public float next_attack_time_interval;

	public string next_attack_state;

	public string state;

	public Sprite[] sprite;
	public int sprite_index;
	public int sprite_first_index;
	public int sprite_last_index;

	public bool is_sprite_swicher_enabled=true;
	public bool is_sprite_swicher_once_enabled = false;

	public bool is_prefab=true;

	public bool is_freeze=false;
	public float freeze_start_time;
	public float freeze_time_interval =1.0f;

	private bool is_stop=false;
	private Vector3 prev_velocity;

	private Vector3 prev_hero_position;


	private float limit_x_left=-80;
	private float limit_x_right=80;
	private float limit_y_top=48;
	private float limit_y_bottom=-64;

	private float last_fire_time;

	private Color enemy_color=new Color(1.0f,1.0f,1.0f,1.0f);

	// Use this for initialization
	void Start () {

		gameObject.layer = 9;

		freeze_time_interval = 1.0f;
		sprite_last_index = sprite.Length-1;
		sprite_first_index =0;
		if(is_prefab){
			next_sprite_switch_time = Time.time + next_sprite_switch_interval;
			sprite_index = 0;
				





			enemy_data_controller temp= GameObject.Find ("controller").GetComponent<enemy_data_controller>();

			if(GetComponent<object_controller>()!=null){
				GetComponent<object_controller>().parent_enemy_controller=this;
			}

			enemy_exp= temp.get_enemy_exp (enemy_no);

			if(GameObject.Find ("controller").GetComponent<level_data_controller>().get_level()>99){
				enemy_exp=0;
			}

			max_HP=temp.get_max_HP(enemy_no);
			remaining_HP=max_HP;
			enemy_money=temp.get_enemy_money (enemy_no);


			if(is_yellow){
				max_HP*=8;
				remaining_HP*=8;
				enemy_money*=16;
				enemy_exp*=16;

				Color temp_color= new Color (1.0f, 1.0f, 0.5f);
				enemy_color= temp_color;
				change_color( temp_color);

				if(enemy_no=="boss_005"){

					transform.FindChild("boss_005_ice").FindChild("ice_1").GetComponent<SpriteRenderer>().material.color=temp_color;
					transform.FindChild("boss_005_ice").FindChild("ice_2").GetComponent<SpriteRenderer>().material.color=temp_color;
					transform.FindChild("boss_005_ice").FindChild("ice_3").GetComponent<SpriteRenderer>().material.color=temp_color;
					transform.FindChild("boss_005_ice").FindChild("ice_4").GetComponent<SpriteRenderer>().material.color=temp_color;

				}
			}

			if(is_red){
				max_HP*=16;
				remaining_HP*=16;
				enemy_money*=56;
				enemy_exp*=56;

				Color temp_color= new Color (1.0f, 0.5f, 0.5f);
				enemy_color= temp_color;
				change_color( temp_color);

				if(enemy_no=="boss_005"){
					
					transform.FindChild("boss_005_ice").FindChild("ice_1").GetComponent<SpriteRenderer>().material.color=temp_color;
					transform.FindChild("boss_005_ice").FindChild("ice_2").GetComponent<SpriteRenderer>().material.color=temp_color;
					transform.FindChild("boss_005_ice").FindChild("ice_3").GetComponent<SpriteRenderer>().material.color=temp_color;
					transform.FindChild("boss_005_ice").FindChild("ice_4").GetComponent<SpriteRenderer>().material.color=temp_color;
					
				}

			}


			if((enemy_no=="boss_007"||enemy_no=="boss_008"||enemy_no=="boss_009")&&GameObject.Find ("controller").GetComponent<battle_controller>().stage_no==-9){
				max_HP*=4;
				remaining_HP*=4;

				
				Color temp_color= new Color (0.5f, 0.5f, 1.0f);
				enemy_color= temp_color;
				change_color( temp_color);

			}


			add_HP_bar ();



			attack_point=temp.get_attack_point(enemy_no);
			defence_point=temp.get_defence_point(enemy_no);

			if(enemy_no=="enemy_002"){
				next_attack_state="jump_left";
			}else if(enemy_no=="enemy_003"){
				//next_attack_state="normal";
			}else if(enemy_no=="enemy_004"){
				//next_attack_state="move_up" ;
			}else if(enemy_no=="enemy_005"){
				next_attack_state="teleport_wait";
				next_attack_time=Time.time+next_attack_time_interval;
				GetComponent<object_controller>().set_screen_limit(0,80, limit_y_top,limit_y_bottom,16,16);
			}else if(enemy_no=="enemy_006"){
				next_attack_state="move";
				GetComponent<object_controller>().set_screen_limit(0,80,48,-64,16,16);

			}else if(enemy_no=="enemy_007"){
				next_attack_state="damage_0";
				//GetComponent<object_controller>().set_screen_limit(0,80,64,-64,16,16);
			}else if(enemy_no=="enemy_008"){
				//next_attack_state="move";
				//GetComponent<object_controller>().set_screen_limit(0,80,48,-64,32,32);
			
			}else if(enemy_no=="enemy_009"){
				rigidbody.velocity= new Vector3( 16.0f,16.0f,0);
				GetComponent<object_controller>().set_screen_limit(limit_x_left,limit_x_right,limit_y_top,limit_y_bottom,16,16);
				GetComponent<object_controller>().is_bounse=true;

			}else if(enemy_no=="enemy_010"){
				next_attack_state="sleep";
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,16,16);
				sprite_last_index=1;
				is_sprite_swicher_enabled=false;
			}else if(enemy_no=="enemy_011"){
				next_attack_time_interval=4.0f;
				next_attack_time=Time.time+next_attack_time_interval;
				//GetComponent<object_controller>().set_screen_limit(limit_x_left,limit_x_right,limit_y_top,limit_y_bottom,16,16);
			}else if(enemy_no=="enemy_012"){
				next_attack_state="move";
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,16,32);
			
			}else if(enemy_no=="enemy_013"){

				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,32,32);
				GetComponent<object_controller>().is_bounse=true;

			}else if(enemy_no=="enemy_014"){
				next_attack_state="move";
				GetComponent<object_controller>().set_screen_limit(0,80,48,-64,16,16);
			
			}else if(enemy_no=="enemy_015"){
				next_attack_state="move";
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,32,16);
				next_sprite_switch_interval=0.25f;
			}else if(enemy_no=="enemy_016"){
				next_attack_state="move";
				GetComponent<object_controller>().set_screen_limit(0,80,48,-64,16,16);

			}else if(enemy_no=="enemy_018"){
				next_attack_state="move,0";
				//next_attack_time_interval=1.0f;
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,16,16);

			}else if(enemy_no=="enemy_022"){
				next_attack_state="move";
				GetComponent<object_controller>().set_screen_limit(0,80,48,-64,16,16);

			}else if(enemy_no=="enemy_023"){
				sprite_last_index=1;
				next_attack_state="move";
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,32,16);
				//next_sprite_switch_interval=0.25f;
			}else if(enemy_no=="enemy_024"){
				sprite_last_index = 1;
				next_attack_state="move";
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,16,32);
			}else if(enemy_no=="enemy_025"){
				next_attack_state="wait_2";
				//next_attack_time=Time.time+2.0f;
				is_sprite_swicher_enabled=false;
			}else if(enemy_no=="enemy_026"){

				next_attack_state="move"; 
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,32,16);

			}else if(enemy_no=="enemy_029"){
				//sprite_last_index = 2;
				next_attack_state="move";
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,16,16);
			}else if(enemy_no=="boss_001"){

				next_attack_time=Time.time+1.0f;
				next_attack_time_interval=6.0f;
			}else if(enemy_no=="boss_002"){
				next_attack_state="move";
				GetComponent<object_controller>().set_screen_limit(0,80,48,-64,32,32);
			}else if(enemy_no=="boss_003"){
				next_attack_state="attack_1";

			}else if(enemy_no=="boss_004"){
				next_attack_time_interval=1.0f;
				next_attack_time=Time.time+next_attack_time_interval;
				next_attack_state="heal";

				rigidbody.velocity= new Vector3( 0,16.0f,0);
				GetComponent<object_controller>().set_screen_limit(limit_x_left,limit_x_right,limit_y_top,limit_y_bottom,64,32);
				GetComponent<object_controller>().is_bounse=true;
			}else if(enemy_no=="boss_005"){
				next_attack_state="frozen";
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,32,32);
			
			}else if(enemy_no=="boss_006"){
				//next_attack_state="revive";
				next_attack_state="move,revive";
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,16,32);
				//renderer.material.color=new Color(1.0f,0,5f,1.0f);
				change_color( new Color (1.0f, 0.5f, 1.0f));
			}else if(enemy_no=="boss_007"){
				//next_attack_state="move";

				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,32,32);
				GetComponent<object_controller>().is_bounse=true;
				rigidbody.velocity=Quaternion.Euler(new Vector3(0,0,30))*new Vector3(64,0,0);
			}else if(enemy_no=="boss_008"){
				//next_attack_state="teleport_wait";
				next_attack_time=Time.time+1.0f;;
				next_attack_state="normal";
				is_sprite_swicher_enabled=false;
			}else if(enemy_no=="boss_009"){
				//next_attack_state="teleport_wait";


				next_attack_state="normal";
				next_attack_time=Time.time+1.0f;


			}else if(enemy_no=="boss_010"){
				
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,32,32);
				transform.rigidbody.velocity=new Vector3(0,32,0);
				next_attack_time_interval=2.0f;
				GetComponent<object_controller>().is_bounse=true;

			}else if(enemy_no=="boss_011"){
				//next_attack_state="teleport_wait";
				next_attack_state="move,normal";
				next_attack_time=Time.time+1.0f;
				GetComponent<object_controller>().set_screen_limit(-80,80,48,-64,16,32);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!is_stop){
			if(!is_freeze){
				if(enemy_no=="enemy_006"){
				}else if(enemy_no=="enemy_003"){
				}else if(enemy_no=="enemy_007"){
				}else if(enemy_no=="enemy_015"){
				}else if(enemy_no=="boss_001"){
				}else if(enemy_no=="boss_002"){

				}else{
					automatic_sprite_swicther ();
				}
				sprite_swicther_once();
				update_enemy();
			}else{
				if(Time.time>freeze_start_time+freeze_time_interval){
					//is_freeze=false;
					end_freeze();
					transform.GetComponent< object_controller>().end_freeze();

					Destroy (transform.FindChild ("ice").gameObject);
				}
			}
		}
	}

	public void enable_stop(){
		prev_velocity=rigidbody.velocity;
		rigidbody.velocity=new Vector3(0,0,0);
		is_stop = true;
	
	}

	public void disable_stop(){
		rigidbody.velocity = prev_velocity;
		is_stop = false;
	}
	public void automatic_sprite_swicther(){
		if(sprite.Length!=0&&is_sprite_swicher_enabled){
			if (Time.time > next_sprite_switch_time) {

				GetComponent<SpriteRenderer>().sprite = sprite[sprite_switcher()];
				next_sprite_switch_time = Time.time + next_sprite_switch_interval;
				
			}
		}
	}

	public void sprite_swicther_once(){
		if(sprite.Length!=0&&is_sprite_swicher_once_enabled){

			if (Time.time > next_sprite_switch_time) {

				if(sprite_index<sprite.Length){
					GetComponent<SpriteRenderer>().sprite = sprite[sprite_index++];
					next_sprite_switch_time = Time.time + next_sprite_switch_interval;
				}else{
					sprite_index=0;
					GetComponent<SpriteRenderer>().sprite=sprite[0];
					is_sprite_swicher_once_enabled=false;
					//sprite_swicther_once
				}
				
			}
		}
	}

	public void update_enemy(){
		if(enemy_no=="enemy_002"){
			//if(next_attack_time<Time.time){
			if(next_attack_state=="jump_left"){

				next_attack_state="jumping_left";
				//next_attack_time=Time.time+3.0f;
				transform.GetComponent< object_controller>().jump_to_position_y (
				new Vector3(0,transform.position.y,0),
				new Vector3(0, -200,0),
				new Vector3(-40,100,0),
				1.0f);

			}else if(next_attack_state=="jump_right"){
				next_attack_state="jumping_right";
				//next_attack_time=Time.time+3.0f;
				transform.GetComponent< object_controller>().jump_to_position_y (
					new Vector3(0,transform.position.y,0),
					new Vector3(0, -200,0),
					new Vector3(40,100,0),
					1.0f);

			}

	

		}else if(enemy_no=="enemy_003"){
			if(Time.time>next_attack_time){
				next_attack_time=Time.time+next_attack_time_interval;
				int temp=sprite_switcher();
				GetComponent<SpriteRenderer>().sprite = sprite[temp];

				if(temp==1){
					//next_attack_state="normal";
					Transform tongue= (Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_003/enemy_003_tongue",  typeof(Transform)) ,transform.position+new Vector3(-16,0,0),Quaternion.identity );
					tongue.name="enemy_003_tongue";
					tongue.GetComponent<enemy_magic_controller>().is_remaining_life_time_enabled=false;
					tongue.parent=transform;
				}else{
					Destroy(transform.FindChild("enemy_003_tongue").gameObject);

				}


			}
		
		}else if(enemy_no=="enemy_004"){
			if(next_attack_state=="move_up"){
				
				next_attack_state="moving_up";
				//next_attack_time=Time.time+3.0f;
				transform.GetComponent< object_controller>().move_to_position(
					new Vector3(transform.position.x,limit_y_top-height/2,0),
					0.5f,
					1.0f);
				Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_004/enemy_004_bullet",  typeof(Transform)) ,transform.position,Quaternion.identity );
				//temp.parent=transform;
				
			}else if(next_attack_state=="move_down"){
				next_attack_state="moving_down";
				//next_attack_time=Time.time+3.0f;
				transform.GetComponent< object_controller>().move_to_position(
					new Vector3(transform.position.x,limit_y_bottom+height/2,0),
					0.5f,
					1.0f);
				Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_004/enemy_004_bullet",  typeof(Transform)) ,transform.position,Quaternion.identity );
				//temp.parent=transform;

			}
		}else if(enemy_no=="enemy_005"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="teleport_wait"){

					next_attack_state="teleport";
					next_attack_time=Time.time + 0.5f;
					change_color(new Color(enemy_color.r,enemy_color.g,enemy_color.b,0.5f));

				}else if(next_attack_state=="teleport"){
					next_attack_state="teleport_wait";
					next_attack_time=Time.time+2.0f;
					change_color(new Color(enemy_color.r,enemy_color.g,enemy_color.b,1.0f));

					transform.GetComponent< object_controller>().teleport ();
				}
			}
		}else if(enemy_no=="enemy_006"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="stop";
					GetComponent<SpriteRenderer>().sprite =sprite[0];
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					

					transform.GetComponent< object_controller>().move_to_position_limit(
					rotation*(new Vector3(20,0,0))+transform.position,
					1.0f,
					1.0f,
					0);

				//}else if (next_attack_state="stop"){


				}
			}

		}else if(enemy_no=="enemy_007"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="damage_1"){

					next_attack_state="damage_0";
					next_attack_time=Time.time+1.0f;

					GetComponent<SpriteRenderer>().sprite =sprite[0];

				}else if(next_attack_state=="damage_2"){
					next_attack_state="damage_1";
					next_attack_time=Time.time+1.0f;
					GetComponent<SpriteRenderer>().sprite =sprite[1];

				}else if(next_attack_state=="damage_3"){
					next_attack_state="damage_2";
					next_attack_time=Time.time+1.0f;
					GetComponent<SpriteRenderer>().sprite =sprite[2];

				}
			}
		}else if(enemy_no=="enemy_008"){
			if(Time.time>next_attack_time){

				next_attack_time=Time.time+next_attack_time_interval;
				Vector3 temp_position= transform.position+new Vector3(-16,0,0);
				Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_008/enemy_008_fire",typeof(Transform)) ,temp_position,Quaternion.identity );
				//temp.parent=transform;
				//temp.parent=transform;

				temp.GetComponent<enemy_magic_controller>().state="up_left,1,1";
				temp.GetComponent<enemy_magic_controller>().next_fire_position=
					(GameObject.Find ("hero").transform.position-temp_position).normalized*16.0f;


			}
		}else if(enemy_no=="enemy_010"){
			//if(Time.time>next_attack_time){
			if(Time.time>next_attack_time){
				next_attack_state="sleep";
				GetComponent<SpriteRenderer>().sprite =sprite[2];
			}

			if(next_attack_state=="move"){
				next_attack_state="moving";

				//next_attack_time=Time.time+4.0f;

				Vector3 random= new Vector3(0,0,Random.Range(0,360));
				Quaternion rotation = Quaternion.identity;
				rotation.eulerAngles= random;
				
				transform.GetComponent< object_controller>().move_to_position_limit(
					rotation*(new Vector3(32,0,0))+transform.position,
					1.0f,
					1.0f,
					0);
				
			}
				
			
		}else if(enemy_no=="enemy_011"){
			if(Time.time>next_attack_time){
				
				Vector3 random= new Vector3(0,0,Random.Range(0,360));
				Quaternion rotation = Quaternion.identity;
				rotation.eulerAngles= random;

				GameObject[] enemies=GameObject.FindGameObjectsWithTag("enemy");

				if(enemies.Length<10){
					Transform temp=enemy_magic_instantiate(enemy_no,"enemy_011_spore",transform.position);
					temp.rigidbody.velocity=rotation*(new Vector3(32,0,0));
				}

				//Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_011/enemy_011",  typeof(Transform)) ,
				//            new Vector3(Random.Range(0,limit_x_right),Random.Range(limit_y_top,limit_y_bottom),0),Quaternion.identity );

			
				next_attack_time=Time.time+next_attack_time_interval;
			}


		}else if(enemy_no=="enemy_012"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="moving";
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(32,0,0))+transform.position,
						1.0f,
						1.0f,
						0);
					
				}
				
			}
		}else if(enemy_no=="enemy_013"){
			/*
			if(next_attack_state=="move_left"){
				
				next_attack_state="moving_left";
				//next_attack_time=Time.time+3.0f;
				transform.GetComponent< object_controller>().move_to_position(
					new Vector3(-64,transform.position.x,0),
					0.5f,
					1.0f);
				//Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_004/enemy_004_bullet",  typeof(Transform)) ,transform.position,Quaternion.identity );
				
				
			}else if(next_attack_state=="move_right"){
				next_attack_state="moving_right";
				//next_attack_time=Time.time+3.0f;
				transform.GetComponent< object_controller>().move_to_position(
					new Vector3(64,transform.position.x,0),
					0.5f,
					1.0f);
				//Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_004/enemy_004_bullet",  typeof(Transform)) ,transform.position,Quaternion.identity );
				
			}
		*/
		}else if(enemy_no=="enemy_014"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="stop";
					//GetComponent<SpriteRenderer>().sprite =sprite[0];
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(16,0,0))+transform.position,
						1.0f,
						1.0f,
						0);
					

					
				}
			}
		}else if(enemy_no=="enemy_015"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="moving";
					//GetComponent<SpriteRenderer>().sprite =sprite[0];

					if(((transform.position.x-GameObject.Find ("hero").transform.position.x)<40)&&
					   ((transform.position.x-GameObject.Find ("hero").transform.position.x)>0)&&
					(Mathf.Abs(transform.position.y-GameObject.Find ("hero").transform.position.y))<32){
						next_attack_state="attack";
						//print ("ok");
					is_sprite_swicher_once_enabled=true;
					transform.GetComponent< object_controller>().move_to_position_limit(
						transform.position+ new Vector3(-32,0,0),
						1.0f,
						1.0f,
						0);

					}else {

					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(8,0,0))+transform.position,
						1.0f,
						1.0f,
						0);
					
					}
					
				}else if(next_attack_state=="back"){
					next_attack_state="backing";
					transform.GetComponent< object_controller>().move_to_position_limit(
						transform.position+ new Vector3(32,0,0),
						0.5f,
						2.0f,
						0);

				}
			}
		}else if(enemy_no=="enemy_016"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="moving";
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;

					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(8,0,0))+transform.position,
						1.0f,
						1.0f,
						0);
					
				}

			}
		
		}else if(enemy_no=="enemy_017"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="attack_1"){
					
					next_attack_state="attack_2";
					next_attack_time=Time.time+ 0.5f;

					Transform temp= (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_017/enemy_017_bullet",  typeof(Transform)) ,transform.position,Quaternion.identity );
					//temp.parent=transform;
					//GetComponent<SpriteRenderer>().sprite =sprite[0];
					
				}else if(next_attack_state=="attack_2"){

					next_attack_state="attack_3";
					next_attack_time=Time.time+ 0.5f;

					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_017/enemy_017_bullet",  typeof(Transform)) ,transform.position,Quaternion.identity );
					//temp.parent=transform;
					//GetComponent<SpriteRenderer>().sprite =sprite[1];
					
				}else if(next_attack_state=="attack_3"){

					next_attack_state="attack_4";
					next_attack_time=Time.time+ 0.5f;

					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_017/enemy_017_bullet",  typeof(Transform)) ,transform.position,Quaternion.identity );
					//temp.parent=transform;
					//GetComponent<SpriteRenderer>().sprite =sprite[2];
				}else{
					next_attack_state="attack_1";
					next_attack_time=Time.time+3.0f;

				}
			}
		}else if(enemy_no=="enemy_018"){
			if(Time.time>next_attack_time){

				string temp_next_attack_state_1="";
				string temp_next_attack_state_2="";
				
				string[] arr=next_attack_state.Split(new char[] {','});


				temp_next_attack_state_1=arr[0];
				temp_next_attack_state_2=arr[1];

				if(int.Parse(arr[1])>7){
					temp_next_attack_state_1="moving";
					temp_next_attack_state_2="0";
					/*
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(16,0,0))+transform.position,
						1.0f,
						1.0f,
						0);

*/					//Vector3 random= new Vector3(0,0,Random.Range(-45.0f,45.0f));
					Vector3 random= new Vector3(0,0,Random.Range(-5.0f,5.0f));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						//rotation*(new Vector3(16,0,0))+transform.position,
						//(rotation*(GameObject.Find ("hero").transform.position-transform.position).normalized*32.0f)+transform.position,

						rotation*GameObject.Find ("hero").transform.position,
						1.0f,
						1.0f,
						0);


				}else{
					if(arr[0]=="move"){
						temp_next_attack_state_1="moving";
						temp_next_attack_state_2=(int.Parse(arr[1])+1).ToString();
						Vector3 random= new Vector3(0,0,Random.Range(0,360));
						Quaternion rotation = Quaternion.identity;
						rotation.eulerAngles= random;
						
						transform.GetComponent< object_controller>().move_to_position_limit(
							rotation*(new Vector3(16,0,0))+transform.position,
							1.0f,
							1.0f,
							0);
						
					}

				}

				next_attack_state=temp_next_attack_state_1+","+temp_next_attack_state_2;
				//next_attack_time=Time.time+next_attack_time_interval;
				
			}
		}else if(enemy_no=="enemy_022"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="moving";
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(8,0,0))+transform.position,
						1.0f,
						1.0f,
						0);
					
				}
				
			}
		}else if(enemy_no=="enemy_023"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="moving";
					//GetComponent<SpriteRenderer>().sprite =sprite[0];
					
					if(((transform.position.x-GameObject.Find ("hero").transform.position.x)<40)&&
					   ((transform.position.x-GameObject.Find ("hero").transform.position.x)>0)&&
					   (Mathf.Abs(transform.position.y-GameObject.Find ("hero").transform.position.y))<32){
						next_attack_state="attack";
						GetComponent<SpriteRenderer>().sprite=sprite[2];
						is_sprite_swicher_enabled=false;

						//print ("ok");
						//is_sprite_swicher_once_enabled=true;
						transform.GetComponent< object_controller>().move_to_position_limit(
							transform.position+ new Vector3(-32,0,0),
							1.0f,
							1.0f,
							0);
						
					}else {
						
						Vector3 random= new Vector3(0,0,Random.Range(0,360));
						Quaternion rotation = Quaternion.identity;
						rotation.eulerAngles= random;
						
						
						transform.GetComponent< object_controller>().move_to_position_limit(
							rotation*(new Vector3(8,0,0))+transform.position,
							1.0f,
							1.0f,
							0);
						
					}
					
				}else if(next_attack_state=="back"){

					next_attack_state="backing";
					sprite_index=0;
					is_sprite_swicher_enabled=true;

					transform.GetComponent< object_controller>().move_to_position_limit(
						transform.position+ new Vector3(32,0,0),
						0.5f,
						2.0f,
						0);
					
				}
			}
		}else if(enemy_no=="enemy_024"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="moving";
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(8,0,0))+transform.position,
						1.0f,
						1.0f,
						0);
					
				}
				
			}
		}else if(enemy_no=="enemy_025"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="wait_1"){
					next_attack_state="wait_2";
					next_attack_time=Time.time+0.5f;
				}else if(next_attack_state=="wait_2"){
					next_attack_state="attack_1";
					next_attack_time=Time.time+2.0f;

					GetComponent<SpriteRenderer>().sprite=sprite[0];
				}else if(next_attack_state=="attack_1"){
					next_attack_state="attack_2";
					next_attack_time=Time.time+0.2f;

					GetComponent<SpriteRenderer>().sprite=sprite[1];

					Vector3 temp_position= transform.position+new Vector3(-16,0,0);
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_025/enemy_025_leaf",typeof(Transform)) ,temp_position,Quaternion.identity );
					//temp.GetComponent<enemy_magic_controller>().state="up_left,1";
					temp.GetComponent<enemy_magic_controller>().rigidbody.velocity=
						(GameObject.Find ("hero").transform.position-temp_position).normalized*64.0f;
					//temp.parent=transform;

				}else if(next_attack_state=="attack_2"){
					next_attack_state="attack_3";
					next_attack_time=Time.time+0.2f;

					Vector3 temp_position= transform.position+new Vector3(-16,0,0);
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_025/enemy_025_leaf",typeof(Transform)) ,temp_position,Quaternion.identity );
					//temp.GetComponent<enemy_magic_controller>().state="up_left,1";
					temp.GetComponent<enemy_magic_controller>().rigidbody.velocity=
						(GameObject.Find ("hero").transform.position-temp_position).normalized*64.0f;
					//temp.parent=transform;

				}else if(next_attack_state=="attack_3"){
					next_attack_state="wait_1";

					//next_attack_time=Time.time+0.2f;

					Vector3 temp_position= transform.position+new Vector3(-16,0,0);
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_025/enemy_025_leaf",typeof(Transform)) ,temp_position,Quaternion.identity );
					//temp.GetComponent<enemy_magic_controller>().state="up_left,1";
					temp.GetComponent<enemy_magic_controller>().rigidbody.velocity=
						(GameObject.Find ("hero").transform.position-temp_position).normalized*64.0f;
					//temp.parent=transform;

				}
				
			}
		}else if(enemy_no=="enemy_026"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="moving";
					Vector3 random= new Vector3(0,0,Random.Range(-45.0f,45.0f));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;

					transform.GetComponent< object_controller>().move_to_position_limit(
						//rotation*(new Vector3(16,0,0))+transform.position,
						(rotation*(GameObject.Find ("hero").transform.position-transform.position).normalized*16.0f)+transform.position,
						1.0f,
						1.0f,
						0);
					
				}
				
			}
		}else if(enemy_no=="enemy_029"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="moving";
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(64,0,0))+transform.position,
						1.0f,
						1.0f,
						0);
					
				}
				
			}
		
		}else if(enemy_no=="boss_001"){
			if(Time.time>next_attack_time){

				next_attack_time=Time.time+next_attack_time_interval;
				Transform temp= (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_001/boss_001_sword",  typeof(Transform)) ,new Vector3( 32,GameObject.Find ("hero").transform.position.y+16,0),Quaternion.identity );
				//temp.parent=transform;
			}
		}else if(enemy_no=="boss_002"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="stop";

					//GetComponent<SpriteRenderer>().sprite =sprite[0];
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(20,0,0))+transform.position,
						1.0f,
						1.0f,
						2.0f);

					Vector3 temp_position= transform.position+new Vector3(-16,0,0);

					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_002/boss_002_fire",typeof(Transform)) ,temp_position,Quaternion.identity );
					temp.GetComponent<enemy_magic_controller>().state="up_left,1,1";
					//temp.GetComponent<enemy_magic_controller>().next_fire_position=
					//	(GameObject.Find ("hero").transform.position-temp_position).normalized*16.0f;

					temp.GetComponent<enemy_magic_controller>().next_fire_position=new Vector3(-16,0,0);
					//temp.parent=transform;

					/*
					temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_002/boss_002_fire",typeof(Transform)) ,transform.position,Quaternion.identity );
					temp.GetComponent<enemy_magic_controller>().state="left,1";

					temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_002/boss_002_fire",typeof(Transform)) ,transform.position,Quaternion.identity );
					temp.GetComponent<enemy_magic_controller>().state="down_left,1";
					*/


					//}else if (next_attack_state="stop"){
					
					
				}
			}
		}else if(enemy_no=="boss_003"){
			if(Time.time>next_attack_time){
				next_attack_time=Time.time+next_attack_time_interval;
				if(next_attack_state=="attack_1"){
					next_attack_state="attack_2";
					next_attack_time=Time.time+0.2f;
				}else if(next_attack_state=="attack_2"){
					next_attack_state="attack_3";
					next_attack_time=Time.time+0.2f;
				}else{
					next_attack_state="attack_1";
				}


				Vector3 temp_position= transform.position+new Vector3(-16,0,0);
				Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_003/boss_003_leaf",typeof(Transform)) ,temp_position,Quaternion.identity );
				//temp.GetComponent<enemy_magic_controller>().state="up_left,1";
				temp.GetComponent<enemy_magic_controller>().rigidbody.velocity=
					(GameObject.Find ("hero").transform.position-temp_position).normalized*64.0f;
				//temp.parent=transform;

			}
		}else if(enemy_no=="boss_004"){
			if(Time.time>next_attack_time){

				next_attack_time=Time.time+next_attack_time_interval;

				if(next_attack_state=="heal"){
					next_attack_state="wait";
					int heal_point=200;
					remaining_HP+=heal_point;

					if(remaining_HP>max_HP){
						remaining_HP=max_HP;
					}

					HP_bar.show (remaining_HP,max_HP);
					/*
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/text/heal_count",  typeof(Transform)) , transform.position,Quaternion.identity );
					
					temp.GetComponent<damage_count_controller> ().set_text(heal_point.ToString (),transform.position);
*/

					change_color(new Color(enemy_color.r/2,enemy_color.g,enemy_color.b/2,1.0f));
					GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("heal");

					if(GameObject.Find ("controller").GetComponent<battle_controller>().stage_no!=-9){
						GameObject[] enemies=GameObject.FindGameObjectsWithTag("enemy");
						if(enemies.Length<5){
							Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_015/enemy_015",  typeof(Transform)) ,new Vector3(-32,0,0),Quaternion.identity );
							//Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_015/enemy_015",  typeof(Transform)) ,new Vector3(-32,-32,0),Quaternion.identity );
						}
					}

				}else{
					next_attack_state="heal";
					change_color(new Color(enemy_color.r,enemy_color.g,enemy_color.b,1.0f));
					//renderer.material.color=new Color(1.0f,1.0f,1.0f);
				}
			}
		}else if(enemy_no=="boss_005"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="start_move"){
					next_attack_state="move";
					is_sprite_swicher_enabled=true;
				}else if(next_attack_state=="move"){
					next_attack_state="moving";
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(16,0,0))+transform.position,
						1.0f,
						1.0f,
						0);
					Transform temp=enemy_magic_instantiate("boss_005","boss_005_iceball",transform.position);
					temp.rigidbody.velocity=(GameObject.Find("hero").transform.position-transform.position).normalized*64.0f;
				}else if(next_attack_state=="break_ice"){	 

					Transform ice= transform.FindChild("boss_005_ice");
					
					ice.FindChild ("ice_1").GetComponent<enemy_magic_controller>().enable_life_time(2.0f);
					ice.FindChild ("ice_1").GetComponent<object_controller>().move_to_position(
						transform.position+new Vector3(-16,16,0),0.5f,0);
					ice.FindChild ("ice_2").GetComponent<enemy_magic_controller>().enable_life_time(2.0f);
					ice.FindChild ("ice_2").GetComponent<object_controller>().move_to_position(
						transform.position+new Vector3(16,16,0),0.5f,0);
					ice.FindChild ("ice_3").GetComponent<enemy_magic_controller>().enable_life_time(2.0f);
					ice.FindChild ("ice_3").GetComponent<object_controller>().move_to_position(
						transform.position+new Vector3(-16,-16,0),0.5f,0);
					ice.FindChild ("ice_4").GetComponent<enemy_magic_controller>().enable_life_time(2.0f);
					ice.FindChild ("ice_4").GetComponent<object_controller>().move_to_position(
						transform.position+new Vector3(16,-16,0),0.5f,0);
					
					next_attack_state="start_move";
					next_attack_time=Time.time+2.0f;

				}
				
			}
		}else if(enemy_no=="boss_006"){
			string temp_next_attack_state_1="";
			string temp_next_attack_state_2="";


			string[] arr=next_attack_state.Split(new char[] {','});
		
			if(arr[0]=="move"){

				temp_next_attack_state_1="moving";
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(16,0,0))+transform.position,
						1.0f,
						1.0f,
						0);
				if(GameObject.Find ("controller").GetComponent<battle_controller>().stage_no!=-9){	
					GameObject[] enemies=GameObject.FindGameObjectsWithTag("enemy");
					if(enemies.Length<3){
						Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_024/enemy_024",  typeof(Transform)) ,new Vector3(0,0,0),Quaternion.identity );
					}
				}

			}else{
				temp_next_attack_state_1="moving";
			}

			if(arr[1]=="revive"){
				temp_next_attack_state_2="revive";
				
			}else{
				temp_next_attack_state_2="normal";
				if(Time.time>next_attack_time){

					temp_next_attack_state_2="revive";
					change_color( new Color (1.0f, 0.5f, 1.0f));
					//renderer.material.color=new Color(1.0f,0,5f,1.0f);
				}

			}


			next_attack_state=temp_next_attack_state_1+","+temp_next_attack_state_2;
		}else if(enemy_no=="boss_007"){
			/*
			if(Time.time>next_attack_time){
				if(next_attack_state=="move"){
					next_attack_state="moving";
					Vector3 random= new Vector3(0,0,Random.Range(0,360));
					Quaternion rotation = Quaternion.identity;
					rotation.eulerAngles= random;
					
					transform.GetComponent< object_controller>().move_to_position_limit(
						rotation*(new Vector3(32,0,0))+transform.position,
						1.0f,
						1.0f,
						0);
					
					
				}
			}
			*/
		}else if(enemy_no=="boss_008"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="beam_1"){
					
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_008/boss_008_beam",typeof(Transform)) ,transform.position,Quaternion.identity );

					//temp.localRotation=Quaternion.Euler(new Vector3(0,0,Random.Range(-90.0f,90.0f)));

					temp.localRotation=Quaternion.Euler(0,0,0)*Quaternion.FromToRotation(new Vector3(1,0,0),(transform.position-prev_hero_position).normalized);


					next_attack_state="beam_2";
					next_attack_time=Time.time+0.5f;
				}else if(next_attack_state=="beam_2"){
					
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_008/boss_008_beam",typeof(Transform)) ,transform.position,Quaternion.identity );
					//temp.localRotation=Quaternion.Euler(new Vector3(0,0,Random.Range(-90.0f,90.0f)));

					temp.localRotation=Quaternion.Euler(0,0,-30)*Quaternion.FromToRotation(new Vector3(1,0,0),(transform.position-prev_hero_position).normalized);


					next_attack_state="beam_3";
					next_attack_time=Time.time+0.5f;

				}else if(next_attack_state=="beam_3"){

					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_008/boss_008_beam",typeof(Transform)) ,transform.position,Quaternion.identity );
					//temp.localRotation=Quaternion.Euler(new Vector3(0,0,Random.Range(-90.0f,90.0f)));

					temp.localRotation=Quaternion.Euler(0,0,30)*Quaternion.FromToRotation(new Vector3(1,0,0),(transform.position-prev_hero_position).normalized);


					next_attack_state="normal";
					next_attack_time=Time.time+6.0f;
					GetComponent<SpriteRenderer>().sprite = sprite[0];

				}else if(next_attack_state=="normal"){

					next_attack_state="beam_1";

					next_attack_time=Time.time+1.0f;
					GetComponent<SpriteRenderer>().sprite = sprite[1];
					
					prev_hero_position= GameObject.Find ("hero").transform.position;

				}
			}
		}else if(enemy_no=="boss_009"){
			if(Time.time>next_attack_time){
				if(next_attack_state=="ball"){
					next_attack_time=Time.time+4.0f;
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_009/boss_009_ball",typeof(Transform)) ,transform.position,Quaternion.identity );
					temp.GetComponent<object_controller>().move_to_position(GameObject.Find ("hero").transform.position,1.0f,0);

					next_attack_state="normal";

				}else if(next_attack_state=="beam_1"){
					
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_009/boss_009_beam",typeof(Transform)) ,transform.position,Quaternion.identity );


					//Mathf.Acos(transform.position-GameObject.Find ("hero").transform.position).normalized.x)*180/Mathf.PI

					//temp.localRotation=Quaternion.Euler(new Vector3(0,0,Random.Range(-90.0f,90.0f)));

					temp.localRotation=Quaternion.Euler(0,0,-45)*Quaternion.FromToRotation(new Vector3(1,0,0),(transform.position-prev_hero_position).normalized);

					next_attack_state="beam_2";
					next_attack_time=Time.time+0.5f;
				}else if(next_attack_state=="beam_2"){
					
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_009/boss_009_beam",typeof(Transform)) ,transform.position,Quaternion.identity );

					temp.localRotation=Quaternion.Euler(0,0,45)*Quaternion.FromToRotation(new Vector3(1,0,0),(transform.position-prev_hero_position).normalized);

					next_attack_state="beam_3";
					next_attack_time=Time.time+0.5f;
					
				}else if(next_attack_state=="beam_3"){
					
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_009/boss_009_beam",typeof(Transform)) ,transform.position,Quaternion.identity );


					temp.localRotation=Quaternion.Euler(0,0,0)*Quaternion.FromToRotation(new Vector3(1,0,0),(transform.position-prev_hero_position).normalized);

					next_attack_state="normal";
					next_attack_time=Time.time+4.0f;
					
				}else if(next_attack_state=="normal"){

					if(Random.Range (0,2)==0){
						next_attack_state="beam_1";
						prev_hero_position= GameObject.Find ("hero").transform.position;
					}else{
						next_attack_state="ball";
					}

					
					
					
				}
			}
		}else if(enemy_no=="boss_010"){
			if(Time.time>next_attack_time){
				
				next_attack_time=Time.time+next_attack_time_interval;
				Vector3 temp_position=transform.position;
				Transform temp;
				if(Random.Range(0,4)==0){

					temp= (Transform)Instantiate(( Transform) Resources.Load ("prefab/item/coin",  typeof(Transform)),temp_position ,Quaternion.identity );
					temp.GetComponent<item_controller>().money=Random.Range(1,100);
				}else{
					temp= (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_010/boss_010_match",  typeof(Transform)),temp_position ,Quaternion.identity );

				}
				temp.rigidbody.velocity=(GameObject.Find ("hero").transform.position-temp_position).normalized*64.0f;
			}

		}else if(enemy_no=="boss_011"){
			string temp_next_attack_state_1="";
			string temp_next_attack_state_2="";
			
			
			string[] arr=next_attack_state.Split(new char[] {','});


			if(arr[0]=="move"){


				temp_next_attack_state_1="moving";
				Vector3 random= new Vector3(0,0,Random.Range(0,360));
				Quaternion rotation = Quaternion.identity;
				rotation.eulerAngles= random;
				
				transform.GetComponent< object_controller>().move_to_position_limit(
					rotation*(new Vector3(32,0,0))+transform.position,
					1.0f,
					1.0f,
					0);


			}else{
				temp_next_attack_state_1="moving";
			}
			
			if(Time.time>next_attack_time){

				if(arr[1]=="ball"){
					next_attack_time=Time.time+4.0f;
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_011/boss_011_ball",typeof(Transform)) ,transform.position,Quaternion.identity );
					temp.GetComponent<object_controller>().move_to_position(GameObject.Find ("hero").transform.position,1.0f,0);

					temp.GetComponent<enemy_magic_controller>().attack_point=attack_point;
					temp_next_attack_state_2="normal";
					
				}else if(arr[1]=="beam_1"){
					
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_011/boss_011_beam",typeof(Transform)) ,transform.position,Quaternion.identity );

					temp.GetComponent<enemy_magic_controller>().attack_point=attack_point;

					temp.localRotation=Quaternion.Euler(0,0,-30)*Quaternion.FromToRotation(new Vector3(1,0,0),(transform.position-GameObject.Find ("hero").transform.position).normalized);

					temp.parent=transform;


					temp_next_attack_state_2="beam_2";
					next_attack_time=Time.time+0.5f;

				}else if(arr[1]=="beam_2"){
					
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_011/boss_011_beam",typeof(Transform)) ,transform.position,Quaternion.identity );

					temp.GetComponent<enemy_magic_controller>().attack_point=attack_point;

					temp.localRotation=Quaternion.Euler(0,0,30)*Quaternion.FromToRotation(new Vector3(1,0,0),(transform.position-GameObject.Find ("hero").transform.position).normalized);

					temp.parent=transform;

					temp_next_attack_state_2="beam_3";
					next_attack_time=Time.time+0.5f;
					
				}else if(arr[1]=="beam_3"){
					
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_011/boss_011_beam",typeof(Transform)) ,transform.position,Quaternion.identity );

					temp.GetComponent<enemy_magic_controller>().attack_point=attack_point;

					temp.localRotation=Quaternion.FromToRotation(new Vector3(1,0,0),(transform.position-GameObject.Find ("hero").transform.position).normalized);

					temp.parent=transform;

					temp_next_attack_state_2="normal";
					next_attack_time=Time.time+4.0f;

				}else if(arr[1]=="teleport_1"){
					//GetComponent<object_controller>().teleport();
					temp_next_attack_state_2="teleport_2";

					change_color(new Color(enemy_color.r,enemy_color.g,enemy_color.b,0.5f));
					next_attack_time=Time.time+0.5f;

				}else if(arr[1]=="teleport_2"){

					temp_next_attack_state_2="normal";

					GetComponent<object_controller>().teleport();
					change_color(new Color(enemy_color.r,enemy_color.g,enemy_color.b,1.0f));

					next_attack_time=Time.time+4.0f;

				}else if(arr[1]=="normal"){

					int r=Random.Range (0,3);

					if(r==0){

						temp_next_attack_state_2="teleport_1";
					}else if(r==1){

						temp_next_attack_state_2="beam_1";
						
					}else{
						temp_next_attack_state_2="ball";
					}
					
					
					
					
				}

			}else{
				temp_next_attack_state_2=arr[1];
			}

			
			
			next_attack_state=temp_next_attack_state_1+","+temp_next_attack_state_2;
		}



	}

	public int sprite_switcher(){


		sprite_index++;
		if (sprite_index > sprite_last_index) {
			sprite_index=sprite_first_index;
		}

		return sprite_index;
	}





	public void add_HP_bar(){
		Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/HP_bar/HP_bar",  typeof(Transform)) ,new Vector3(transform.position. x -8 + HP_bar_x,transform.position.y+height/2+4+HP_bar_y,0),Quaternion.identity );

		temp.parent = transform;
		temp.name = "HP_bar";
		HP_bar = temp.GetComponent<HP_bar_controller> ();
		HP_bar.set_scale(1.0f,0.25f);

		if(!is_HP_bar_shown){
			temp.FindChild ("red").renderer.enabled=false;
			temp.FindChild ("green").renderer.enabled=false;
		}
		 
	}


	public void hit(long damage,Collider c){

		if(!GameObject.Find ("hero").GetComponent<hero_controller>().is_dead){
		

			remaining_HP -=  Mathf.RoundToInt(damage*Random.Range (0.8f,1.2f));

			if(enemy_no=="boss_009"&&GameObject.Find ("controller").GetComponent<battle_controller>().stage_no==-4){
				if(remaining_HP<=max_HP/4){

					bool flg=false;

					if(c.GetComponent<sword_controller>()!=null){
						if(c.GetComponent<sword_controller>().sword_no=="sword_001_03"){
							flg=true;
						}
					}

					if(flg){


					}else{
						remaining_HP=max_HP/4;

						if(!GameObject.Find("hero").GetComponent<hero_controller>().is_boss_009_barrier_enablaed){
							Transform temp =(Transform) Instantiate(Resources.Load ("prefab/enemy/boss_009/boss_009_barrier",typeof(Transform)));
							temp.parent=transform;
							temp.name="boss_009_barrier";
							temp.localPosition=new Vector3(0,0,0);
							GameObject.Find("hero").GetComponent<hero_controller>().is_boss_009_barrier_enablaed=true;
						}
					}
				}
			}

			if(HP_bar!=null){
				HP_bar.show (remaining_HP,max_HP );
			}

			GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("enemy_hit");


			if(enemy_no=="enemy_007"){
				if(next_attack_state=="damage_0"){

					next_attack_state="damage_1";
					GetComponent<SpriteRenderer>().sprite =sprite[1];
					
				}else if(next_attack_state=="damage_1"){

					next_attack_state="damage_2";
					GetComponent<SpriteRenderer>().sprite =sprite[2];

				}else if(next_attack_state=="damage_2"){
				/*		
					next_attack_state="damage_3";
					GetComponent<SpriteRenderer>().sprite =sprite[3];

				}else if(next_attack_state=="damage_3"){
				*/

					next_attack_state="damage_1";
					GetComponent<SpriteRenderer>().sprite =sprite[1];

					Transform rock;
					Vector3 temp_velocity;

					for (int i=0;i<3; i++){

						rock=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007_rock",typeof(Transform)) ,transform.position,Quaternion.identity );

						int rnd =Random.Range(0,360);

						temp_velocity=Quaternion.Euler(0,0,rnd)*new Vector3(64,0,0);
						rock.rigidbody.velocity= temp_velocity; 

						//next_attack_state="damage_0";
						//rock.parent=transform;
					}
				}
			} else if(enemy_no=="enemy_010"){
				if(next_attack_state=="sleep"){
					is_sprite_swicher_enabled=true;
					next_attack_state="move";
				}
				next_attack_time=Time.time+next_attack_time_interval;
			} else if(enemy_no=="enemy_021"){
				if(Random.Range (0,4)==0){
					Transform temp_2=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_021/enemy_021_seed",typeof(Transform)) ,transform.position+ new Vector3(0,8,0),Quaternion.identity );
					temp_2.GetComponent<object_controller>().move_to_position(transform.position+ new Vector3(0,-8,0),1.0f,0);
					//temp_2.parent=transform;
					//temp_2.rigidbody.velocity= new Vector3(0,-8,0);
				}
			} else if(enemy_no=="enemy_024"){
				if(remaining_HP <=max_HP/2&&sprite_last_index==1){
					sprite_first_index=2;
					sprite_last_index=3;
					sprite_index=2;
					GetComponent<SpriteRenderer>().sprite=sprite[2];
					Transform temp= (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_029/enemy_029",  typeof(Transform)) ,transform.position+new Vector3(0,8,0),Quaternion.identity );

					temp.GetComponent<enemy_controller>().is_red=is_red;
					temp.GetComponent<enemy_controller>().is_yellow=is_yellow;

				}
			
			} else if(enemy_no=="boss_005"){
				if(next_attack_state=="frozen"){
					if(remaining_HP>0&&remaining_HP<max_HP*7/8){
						next_attack_state="break_ice";
						/*
						Transform ice= transform.FindChild("boss_005_ice");
						
						ice.FindChild ("ice_1").GetComponent<enemy_magic_controller>().enable_life_time(2.0f);
						ice.FindChild ("ice_1").GetComponent<object_controller>().move_to_position(
							transform.position+new Vector3(-16,16,0),0.5f,0);
						ice.FindChild ("ice_2").GetComponent<enemy_magic_controller>().enable_life_time(2.0f);
						ice.FindChild ("ice_2").GetComponent<object_controller>().move_to_position(
							transform.position+new Vector3(16,16,0),0.5f,0);
						ice.FindChild ("ice_3").GetComponent<enemy_magic_controller>().enable_life_time(2.0f);
						ice.FindChild ("ice_3").GetComponent<object_controller>().move_to_position(
							transform.position+new Vector3(-16,-16,0),0.5f,0);
						ice.FindChild ("ice_4").GetComponent<enemy_magic_controller>().enable_life_time(2.0f);
						ice.FindChild ("ice_4").GetComponent<object_controller>().move_to_position(
							transform.position+new Vector3(16,-16,0),0.5f,0);

						next_attack_state="start_move";
						next_attack_time=Time.time+2.0f;
						*/
					}
				}

			} else if(enemy_no=="boss_006"){



				if(remaining_HP <=0){
				string[] arr=next_attack_state.Split(new char[] {','});

					if(arr[1]=="revive"){
						remaining_HP=max_HP;
						HP_bar.show (remaining_HP,max_HP );

						GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("revive");

						string temp_next_attack_state="normal";
						change_color(enemy_color);
						next_attack_time=Time.time+8.0f;
						next_attack_state=arr[0]+","+temp_next_attack_state;

					}

				}
			} else if(enemy_no=="boss_008"){
				if(Random.Range (0,2)==0){
					GameObject.Find ("hero").GetComponent<hero_controller>().damage_controller("start_paralyze");
				}

			}




			if (remaining_HP <= 0) {
				//print ("dead");
				dead();
		}

		}
	}

	public void move_end(){

		if(enemy_no=="enemy_004"){
			//if(next_attack_time<Time.time){
			if(next_attack_state=="moving_up"){
				next_attack_state="move_down";
				next_attack_time=Time.time+2.0f;

			}else if (next_attack_state=="moving_down"){
				next_attack_state="move_up";
				next_attack_time=Time.time+2.0f;

			}
			//}
		}else if (enemy_no=="enemy_006"){
			next_attack_state="move";
			next_attack_time=Time.time+2.0f;
			GetComponent<SpriteRenderer>().sprite =sprite[1];

		}else if (enemy_no=="enemy_010"){
			next_attack_state="move";
		}else if (enemy_no=="enemy_012"){
			next_attack_state="move";
		}else if (enemy_no=="enemy_014"){
			next_attack_state="move";
			//next_attack_time=Time.time+2.0f;
			//GetComponent<SpriteRenderer>().sprite =sprite[1];
			Transform temp= (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_014/enemy_014_sumi",typeof(Transform)) ,transform.position,Quaternion.identity );
			temp.parent=transform;
		}else if (enemy_no=="enemy_015"){
			if(next_attack_state=="attack"){
				next_attack_state="back";
			}else if(next_attack_state=="backing"){
				next_attack_state="move";
			}else{
				next_attack_state="move";
			}
		}else if (enemy_no=="enemy_016"){
			next_attack_state="move";
		}else if (enemy_no=="enemy_018"){

			string[] arr=next_attack_state.Split(new char[] {','});
			next_attack_state="move,"+arr[1];

		}else if (enemy_no=="enemy_022"){
			next_attack_state="move";
		}else if (enemy_no=="enemy_023"){
			if(next_attack_state=="attack"){
				next_attack_state="back";
			}else if(next_attack_state=="backing"){
				next_attack_state="move";
			}else{
				next_attack_state="move";
			}
		}else if (enemy_no=="enemy_024"){
			next_attack_state="move";
		}else if (enemy_no=="enemy_026"){
			next_attack_state="move";
		}else if (enemy_no=="enemy_029"){
			next_attack_state="move";
		}else if (enemy_no=="boss_002"){
			//print ("end");
			next_attack_state="move";
			next_attack_time=Time.time+2.0f;
			//GetComponent<SpriteRenderer>().sprite =sprite[1];
		}else if (enemy_no=="boss_005"){
			next_attack_state="move";
		}else if(enemy_no=="boss_006"){

			string[] arr=next_attack_state.Split(new char[] {','});
			next_attack_state="move"+","+arr[1];

		}else if(enemy_no=="boss_011"){
			
			string[] arr=next_attack_state.Split(new char[] {','});
			next_attack_state="move"+","+arr[1];
		}else if(enemy_no=="enemy_013"){
			/*
			//if(next_attack_time<Time.time){
			if(next_attack_state=="moving_left"){
				next_attack_state="move_right";
				next_attack_time=Time.time+1.0f;
				
			}else if (next_attack_state=="moving_right"){
				next_attack_state="move_left";
				next_attack_time=Time.time+1.0f;
				
			}
			*/
		}


	}

	public void jump_end(){
		if(enemy_no=="enemy_002"){
			//if(next_attack_time<Time.time){
			if(next_attack_state=="jumping_left"){
				next_attack_state="jump_right";
				//next_attack_time=Time.time+2.0f;

			}else if (next_attack_state=="jumping_right"){
				next_attack_state="jump_left";
				//next_attack_time=Time.time+2.0f;
			}
			//}
		}
	}



	public void dead(){
		if(!is_dead){

			is_dead = true;

			Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/text/exp_count",  typeof(Transform)) , transform.position,Quaternion.identity );
			
			temp.GetComponent<damage_count_controller> ().set_text("EXP "+enemy_exp,transform.position);

			
			bool is_boss=false;


			if(GameObject.Find ("controller").GetComponent<battle_controller>().stage_no==-9){
				is_yellow=false;
				is_red=true;
			}

			if (enemy_no == "boss_001") {
				string temp_sword_no_1="sword_002";
				string temp_sword_no_2="";
				
				if(is_yellow){
					temp_sword_no_2="_02";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else if(is_red){
				 	temp_sword_no_2="_03";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else{
					temp_sword_no_2="_01";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}
				
				temp.tag = "item";
				temp.GetComponent<item_controller>().type="sword";
				temp.GetComponent<item_controller>().sword_no=temp_sword_no_1+temp_sword_no_2;
				is_boss=true;


				if(PlayerPrefs.GetInt("stage_cleared")==3){
					temp.GetComponent<item_controller>().is_remaining=true;
				}


			}else if(enemy_no == "boss_002") {

				string temp_sword_no_1="sword_003";
				string temp_sword_no_2="";

				if(is_yellow){
					temp_sword_no_2="_02";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else if(is_red){
					temp_sword_no_2="_03";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else{
					temp_sword_no_2="_01";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}

				temp.tag = "item";
				temp.GetComponent<item_controller>().type="sword";
				temp.GetComponent<item_controller>().sword_no=temp_sword_no_1+temp_sword_no_2;
				is_boss=true;

			}else if(enemy_no == "boss_003") {
				string temp_sword_no_1="sword_004";
				string temp_sword_no_2="";
				
				if(is_yellow){
					temp_sword_no_2="_02";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else if(is_red){
					temp_sword_no_2="_03";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else{
					temp_sword_no_2="_01";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}

				temp.tag = "item";
				temp.GetComponent<item_controller>().type="sword";
				temp.GetComponent<item_controller>().sword_no=temp_sword_no_1+temp_sword_no_2;
				is_boss=true;

			}else if(enemy_no == "boss_004") {

				string temp_sword_no_1="sword_005";
				string temp_sword_no_2="";

				if(is_yellow){
					temp_sword_no_2="_02";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else if(is_red){
					temp_sword_no_2="_03";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else{
					temp_sword_no_2="_01";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}

				temp.tag = "item";
				temp.GetComponent<item_controller>().type="sword";
				temp.GetComponent<item_controller>().sword_no=temp_sword_no_1+temp_sword_no_2;
				is_boss=true;

			}else if(enemy_no == "boss_005") {

				string temp_sword_no_1="sword_006";
				string temp_sword_no_2="";

				if(is_yellow){
					temp_sword_no_2="_02";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else if(is_red){
					temp_sword_no_2="_03";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else{
					temp_sword_no_2="_01";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}

				temp.tag = "item";
				temp.GetComponent<item_controller>().type="sword";
				temp.GetComponent<item_controller>().sword_no=temp_sword_no_1+temp_sword_no_2;
				is_boss=true;

			}else if(enemy_no == "boss_006") {

				string temp_sword_no_1="sword_007";
				string temp_sword_no_2="";
				
				if(is_yellow){
					temp_sword_no_2="_02";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else if(is_red){
					temp_sword_no_2="_03";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else{
					temp_sword_no_2="_01";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}
				
				temp.tag = "item";
				temp.GetComponent<item_controller>().type="sword";
				temp.GetComponent<item_controller>().sword_no=temp_sword_no_1+temp_sword_no_2;
				is_boss=true;

			}else if(enemy_no == "boss_007") {

				string temp_sword_no_1="sword_008";
				string temp_sword_no_2="";
				
				if(is_yellow){
				 	temp_sword_no_2="_02";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else if(is_red){
					temp_sword_no_2="_03";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}else{
					temp_sword_no_2="_01";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no_1+temp_sword_no_2+"/"+temp_sword_no_1+temp_sword_no_2, typeof(Transform)), transform.position, Quaternion.identity);
				}
				
				temp.tag = "item";
				temp.GetComponent<item_controller>().type="sword";
				temp.GetComponent<item_controller>().sword_no=temp_sword_no_1+temp_sword_no_2;
				is_boss=true;

			}else if(enemy_no == "boss_009") {

				if(GameObject.Find ("controller").GetComponent<battle_controller>().stage_no==-3){
					string temp_sword_no="sword_011_01";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no+"/"+temp_sword_no, typeof(Transform)), transform.position, Quaternion.identity);
					temp.tag = "item";
					temp.GetComponent<item_controller>().type="sword";
					temp.GetComponent<item_controller>().sword_no=temp_sword_no;
					temp.GetComponent<item_controller>().is_remaining=true;

					is_boss=true;
				
				}else if(GameObject.Find ("controller").GetComponent<battle_controller>().stage_no==-4){
					is_boss=true;

				}else if(GameObject.Find ("controller").GetComponent<battle_controller>().stage_no==-9){
					string temp_sword_no="sword_011_02";
					temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no+"/"+temp_sword_no, typeof(Transform)), transform.position, Quaternion.identity);
					temp.tag = "item";
					temp.GetComponent<item_controller>().type="sword";
					temp.GetComponent<item_controller>().sword_no=temp_sword_no;
					temp.GetComponent<item_controller>().is_remaining=true;
					
					is_boss=true;
				}


			}else if(enemy_no == "boss_010") {

				string temp_sword_no="sword_010_01";
				temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/sword/"+temp_sword_no+"/"+temp_sword_no, typeof(Transform)), transform.position, Quaternion.identity);
				temp.tag = "item";
				temp.GetComponent<item_controller>().type="sword";
				temp.GetComponent<item_controller>().sword_no=temp_sword_no;
				temp.GetComponent<item_controller>().is_remaining=true;
				is_boss=true;

			}else if(enemy_no == "enemy_028") {
				GameObject.Find ("controller").GetComponent<save_data_controller>().unchi_dead();
			}



			if(!is_boss){
				temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/item/coin", typeof(Transform)), transform.position, Quaternion.identity);
				temp.GetComponent<item_controller>().money=enemy_money;

				//temp.tag = "item";
			}
		//GameObject.Find ("controller").GetComponent<battle_controller> ().enemy_dead (enemy_exp);
		}

		Destroy (gameObject);

	}



	void OnDestroy(){
		if(is_dead){
			GameObject.Find ("controller").GetComponent<battle_controller> ().enemy_dead (this,enemy_exp);
		}

	}


	public void start_freeze(float input_freeze_time){

		is_freeze = true;
		freeze_time_interval = input_freeze_time;
		freeze_start_time = Time.time;
		GetComponent<object_controller> ().start_freeze ();
	}

	public void end_freeze(){
		is_freeze = false;
		next_attack_time += Time.time - freeze_start_time;

	}

	public void change_color(Color input_color){

		renderer.material.color = input_color;
	}

	public Transform enemy_magic_instantiate(string input_enemy_no,string input_magic,Vector3 input_position){
		Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/"+input_enemy_no+ "/"+input_magic,typeof(Transform)) ,input_position,Quaternion.identity );
		temp.parent=transform;
		//print (input_position.x+","+input_position.y);
		return temp;


	}


	void object_enter (Collider c){
		
		//print (c.tag);
		//print (c.tag);
		
		if (c.tag == "sword") {
			if(enemy_no=="boss_005"&&next_attack_state=="frozen"){

				GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("barrier");

			}else{

				if(enemy_no=="enemy_002"&&(c.GetComponent<sword_controller>().sword_no=="sword_006_01"
				                           ||c.GetComponent<sword_controller>().sword_no=="sword_006_02"
				                           ||c.GetComponent<sword_controller>().sword_no=="sword_006_03")){
					
					hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point()*4,c);

				}else if(enemy_no=="enemy_008"&&(c.GetComponent<sword_controller>().sword_no=="sword_006_01"
				                                 ||c.GetComponent<sword_controller>().sword_no=="sword_006_02"
				                                 ||c.GetComponent<sword_controller>().sword_no=="sword_006_03")){
					
					hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point()*4,c);

				}else if(enemy_no=="enemy_024"&&(c.GetComponent<sword_controller>().sword_no=="sword_005_01"
					                           ||c.GetComponent<sword_controller>().sword_no=="sword_005_02"
					                           ||c.GetComponent<sword_controller>().sword_no=="sword_005_03")){

					hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point()*4,c);

				} else if(enemy_no=="enemy_027"){
					if(Random.Range (0,2)==0){
						GameObject.Find ("hero").GetComponent<hero_controller>().damage_controller("start_paralyze");
					}

					hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point(),c);

				}else if(enemy_no=="enemy_029"&&(c.GetComponent<sword_controller>().sword_no=="sword_005_01"
				                                 ||c.GetComponent<sword_controller>().sword_no=="sword_005_02"
				                                 ||c.GetComponent<sword_controller>().sword_no=="sword_005_03")){
						
					hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point()*4,c);

				
				}else if(enemy_no=="boss_002"&&(c.GetComponent<sword_controller>().sword_no=="sword_006_01"
				                                 ||c.GetComponent<sword_controller>().sword_no=="sword_006_02"
				                                 ||c.GetComponent<sword_controller>().sword_no=="sword_006_03")){
					
					hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point()*4,c);
				

				}else if(enemy_no=="boss_005"&&(c.GetComponent<sword_controller>().sword_no=="sword_007_01"
				                                ||c.GetComponent<sword_controller>().sword_no=="sword_007_02"
				                                ||c.GetComponent<sword_controller>().sword_no=="sword_007_03")){
					
					hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point()*4,c);

				}else if(enemy_no=="boss_006"&&(c.GetComponent<sword_controller>().sword_no=="sword_005_01"
					   ||c.GetComponent<sword_controller>().sword_no=="sword_005_02"
					   ||c.GetComponent<sword_controller>().sword_no=="sword_005_03")){


						if(GameObject.Find ("hero").GetComponent<hero_controller>().is_using_sword){
							string[] arr=next_attack_state.Split(new char[] {','});
							
							string temp_next_attack_state="normal";
							renderer.material.color=new Color(1.0f,1.0f,1.0f);
							next_attack_time=Time.time+8.0f;
							next_attack_state=arr[0]+","+temp_next_attack_state;
						}

						hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point()*4,c);
				/*
				}else if(enemy_no=="enemy_007"&&(c.GetComponent<sword_controller>().sword_no=="sword_002_01"
				                                 ||c.GetComponent<sword_controller>().sword_no=="sword_002_02"
				                                 ||c.GetComponent<sword_controller>().sword_no=="sword_002_03")){
					
					hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point()*4,c);
				*/
			


				
				}else if(enemy_no=="boss_009"){

					if(GameObject.Find ("controller").GetComponent<battle_controller>().stage_no==-4){
						if(remaining_HP<=max_HP/4){
							if(c.GetComponent<sword_controller>().sword_no=="sword_001_03"){
								hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point()*4,c);
								if(transform.FindChild("boss_009_barrier")!=null){
									Destroy (transform.FindChild("boss_009_barrier").gameObject);
								}
							
							}else{
								GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("barrier");
							}

						}else{

							hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point(),c);

						
						}
					}else{
						hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point(),c);
					}

				}else{

					hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point(),c);
				}
				

				if(GameObject.Find ("hero").GetComponent<hero_controller>().enemy_freeze_time>0.0f){

					if(transform.FindChild ("ice")==null){
						Transform temp = (Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/etc/ice",  typeof(Transform)) , transform.position,Quaternion.identity );
						temp.name="ice";
						temp.parent=transform;
						temp.GetComponent<ice_controller>().set_ice(width/16,height/16,transform);
					}


					start_freeze(GameObject.Find ("hero").GetComponent<hero_controller>().enemy_freeze_time);

				}


				if(enemy_no=="enemy_028"){
					GameObject.Find ("hero").GetComponent<hero_controller>().unchi_hit();
				}
			}

		}else if(c.tag=="magic"){
			if(enemy_no=="boss_005"&&next_attack_state=="frozen"){
				if(c.GetComponent<magic_controller>().type=="fire"){
					hit (c.GetComponent<magic_controller>().attack_point,c);
				}

			}else if(enemy_no=="boss_004"&&c.GetComponent<magic_controller>().type=="leaf"){
				hit (c.GetComponent<magic_controller>().attack_point*2,c);
			}else if(enemy_no=="enemy_008"&&c.GetComponent<magic_controller>().type=="leaf"){
				hit (c.GetComponent<magic_controller>().attack_point*4,c);
			}else if(enemy_no=="boss_009"){

				if(GameObject.Find ("controller").GetComponent<battle_controller>().stage_no==-4){
					if(remaining_HP<=max_HP/4){
						/*
						if(c.GetComponent<sword_controller>().sword_no=="sword_001_03"){
							hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point()*4,c);
						}else{
							GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("barrier");
						}
						*/
					}else{
						
						hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point(),c);
						
						
					}
				}else{
					hit (GameObject.Find ("hero").GetComponent<hero_controller>().total_attack_point(),c);
				}

			}else{
				hit (c.GetComponent<magic_controller>().attack_point,c);


			}

			if(c.GetComponent<magic_controller>().type=="leaf"){
				Destroy (c.gameObject);
			}

		}else if(c.tag=="enemy_magic_remain"){
			if(c.GetComponent<enemy_magic_controller>()!=null){

				if(c.GetComponent<enemy_magic_controller>().type=="enemy_021_bomb"){
					hit (100,c);
				}
			}

		}
	}

	void OnTriggerEnter (Collider c){
		object_enter (c);
	}

	void OnTriggerStay(Collider c){
	 	if(c.tag=="magic"){
			if(c.GetComponent<magic_controller>().type=="fire"){
				if(Time.time-last_fire_time>0.5f){
					object_enter (c);
					last_fire_time=Time.time;
				}
			}
		}
	}





}
