﻿using UnityEngine;
using System.Collections;

public class  save_data_controller : MonoBehaviour {
	
	public string hero_name;
	public int stage_reached;
	public int stage_cleared;
	public int shop_reached;
	
	public string total_exp;
	public long next_level_up_exp;
	
	public int level;
	public int max_HP;
	public int attack_point;
	
	public int unchi_stage;
	
	//public int defence_point;
	public int money;
	
	//public item_data_controller item_data;
	
	private string[] item_names=item_data_controller.get_item_names();
	
	
	// Use this for initialization
	void Start () {
		if(PlayerPrefs.HasKey("is_using_sword")){
			if(PlayerPrefs.GetInt ("is_using_sword")!=0 ){


				if(get_sword_no(get_sword_equipped_slot())=="sword_001_01"||
				   get_sword_no(get_sword_equipped_slot())=="sword_001_02"||
				   get_sword_no(get_sword_equipped_slot())=="sword_001_03"||
				   get_sword_no(get_sword_equipped_slot())=="sword_001_04"){

					disable_is_using_sword();
				  }else{
					sword_use_end();
				}


			}
		}

		for (int i=0; i<=GetInt("max_slot"); i++) {
			if(get_type(i)=="sword"&&get_sword_no(i)=="sword_001_03"){
				PlayerPrefs.SetString("item_slot_"+i+"_sword_no","sword_001_01");
			}
		}

		refresh_public_variables ();
	}

	public void change_hinoki(){
		for (int i=0; i<=GetInt("max_slot"); i++) {
			if(get_type(i)=="sword"&&(get_sword_no(i)=="sword_001_01"||
			                          get_sword_no(i)=="sword_001_02")){


				PlayerPrefs.SetString("item_slot_"+i+"_sword_no","sword_001_03");


			}
		}
	}

	public void change_cypress(){
		for (int i=0; i<=GetInt("max_slot"); i++) {
			if(get_type(i)=="sword"&&(get_sword_no(i)=="sword_001_03")){
				
				
				PlayerPrefs.SetString("item_slot_"+i+"_sword_no","sword_001_04");
				PlayerPrefs.SetString ("map_hinoki","sword_001_04");
				
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	
	public void save(){
		PlayerPrefs.Save ();
		
	}
	
	public void enable_is_using_sword(){
		PlayerPrefs.SetInt ("is_using_sword", 1); 
	}

	public void disable_is_using_sword(){
		PlayerPrefs.SetInt ("is_using_sword", 0); 
	}


	public void initialize_for_test(string input_total_exp,int input_money,int input_stage_cleared){

		PlayerPrefs.SetInt ("is_initialized", 1);
		
		PlayerPrefs.SetString ("hero_name", "しゅじんこう");
		PlayerPrefs.SetString ("map_hinoki","sword_001_01");
		
		PlayerPrefs.SetInt ("unchi_stage", -100); 
		
		PlayerPrefs.SetInt ("stage_no", 0);
		
		PlayerPrefs.SetInt ("is_using_sword", 0); 
		
		PlayerPrefs.SetInt ("stage_reached", 0); 
		PlayerPrefs.SetInt ("stage_cleared", input_stage_cleared);
		PlayerPrefs.SetInt ("shop_reached", 0); 
		
		PlayerPrefs.SetString ("total_exp", "0");
		AddExp (input_total_exp);

		PlayerPrefs.SetInt ("money", input_money);

		PlayerPrefs.SetInt("max_slot", 15);
		
		PlayerPrefs.SetString("item_slot_0_type","sword");
		PlayerPrefs.SetString("item_slot_0_sword_no","sword_001_01");
		PlayerPrefs.SetInt("item_slot_0_power_up",0);
		
		for (int i=1; i<=15; i++) {
			PlayerPrefs.SetString("item_slot_"+i+"_type","null");
			PlayerPrefs.SetString("item_slot_"+i+"_sword_no","null");
			PlayerPrefs.SetInt("item_slot_"+i+"_power_up",0);
			
		}
		
		PlayerPrefs.SetInt("sword_equipped",0);
		
	}


	public void initialize(){
		
		
		
		PlayerPrefs.SetInt ("is_initialized", 1);
		
		PlayerPrefs.SetString ("hero_name", "しゅじんこう");
		PlayerPrefs.SetString ("map_hinoki","sword_001_01");
		
		PlayerPrefs.SetInt ("unchi_stage", -100); 
		
		PlayerPrefs.SetInt ("stage_no", 0);

		PlayerPrefs.SetInt ("is_using_sword", 0); 

		PlayerPrefs.SetInt ("stage_reached", 0); 
		PlayerPrefs.SetInt ("stage_cleared", 0);
		
		PlayerPrefs.SetString ("total_exp", "0");
		AddExp ("0");
				
		PlayerPrefs.SetInt ("money", 0);
		
		
		PlayerPrefs.SetInt("max_slot", 15);
		
		PlayerPrefs.SetString("item_slot_0_type","sword");
		PlayerPrefs.SetString("item_slot_0_sword_no","sword_001_01");
		PlayerPrefs.SetInt("item_slot_0_power_up",0);
		
		for (int i=1; i<=15; i++) {
			PlayerPrefs.SetString("item_slot_"+i+"_type","null");
			PlayerPrefs.SetString("item_slot_"+i+"_sword_no","null");
			PlayerPrefs.SetInt("item_slot_"+i+"_power_up",0);
			
		}


		
		PlayerPrefs.SetString("killed_enemy_list","");
		
		PlayerPrefs.Save();
		
		
	}
	


	public string get_money(){
		return PlayerPrefs.GetInt ("money").ToString ();
	}


	
	public void enemy_kill_count(string input_enemy){
		if (!PlayerPrefs.HasKey (input_enemy)) {
			PlayerPrefs.SetString("killed_enemy_list",PlayerPrefs.GetString ("killed_enemy_list")+","+input_enemy);
			PlayerPrefs.SetInt (input_enemy,0);
		}
		PlayerPrefs.SetInt (input_enemy, PlayerPrefs.GetInt (input_enemy) + 1);
		PlayerPrefs.Save ();
	}
	
	public string GetString(string data_label){
		return PlayerPrefs.GetString (data_label);
	}
	
	public void SetString(string data_label,string input_string){
		PlayerPrefs.SetString (data_label,input_string);
		PlayerPrefs.Save ();
	}
	
	public int GetInt(string data_label){
		return PlayerPrefs.GetInt (data_label);
	}
	
	
	public void SetInt(string data_label,int input){
		
		PlayerPrefs.SetInt (data_label, input);
		
		PlayerPrefs.Save();
		refresh_public_variables();

	}

	public void DeleteKey(string data_label){
		PlayerPrefs.DeleteKey (data_label);
	}

	public bool HasKey(string data_label){
		return PlayerPrefs.HasKey (data_label);
	}

	public string AddMoney(int input){
		
		string text = "fail";
		
		if(PlayerPrefs.HasKey("money")){
			int temp=PlayerPrefs.GetInt ("money");
			temp+=input;
			if(temp>999999999){
				temp=999999999;
			}

			PlayerPrefs.SetInt("money",temp);
			text="sucess";
			PlayerPrefs.Save ();
		}
		
		
		return text;
	}

	public string set_stage_reached(int input_stage){
		string text = "fail";
		if (input_stage > GetInt ("stage_reached")) {
			text="sucess";
			SetInt ("stage_reached",input_stage);
		}
		return text;
	}

	public string set_stage_cleared(int input_stage){

		string text = "fail";
		if (input_stage > GetInt ("stage_cleared")) {
			text="sucess";
			SetInt ("stage_cleared",input_stage);
		}
		return text;
	}

	public int get_stage_cleared(){
		return GetInt ("stage_cleared");
	}
	
	public string AddExp(string input){
		
		string text = "fail";


		if(GameObject.Find ("controller").GetComponent<level_data_controller>().get_level()>99){

		}else{

			if(PlayerPrefs.HasKey("total_exp")){
				text="success";
				
				long temp=  long.Parse(PlayerPrefs.GetString ("total_exp"));
				long total=temp+ long.Parse(input);
				
				PlayerPrefs.SetString ("total_exp", total.ToString());
				
				PlayerPrefs.SetString("next_level_up_exp",
				                      GameObject.Find ("controller").GetComponent<level_data_controller>().total_exp_to_next_level_up_exp(total).ToString());
				
				PlayerPrefs.SetInt("max_HP",
				                   GameObject.Find ("controller").GetComponent<level_data_controller>().total_exp_to_max_HP(total));
				
				
				PlayerPrefs.SetInt("attack_point",
				                   GameObject.Find ("controller").GetComponent<level_data_controller>().total_exp_to_attack_point(total));
				
				
				
				int previous_level=PlayerPrefs.GetInt("level");
				
				PlayerPrefs.SetInt("level",
				                   GameObject.Find ("controller").GetComponent<level_data_controller>().total_exp_to_level(total));
				if(PlayerPrefs.GetInt ("level")>previous_level){
					text="level_up";
				}
				
				
				PlayerPrefs.Save();
				refresh_public_variables();
			}
		}
		
		return text;
	}
	
	
	
	public string AddInt(string data_label, string input){
		
		string text = "";
		
		if (data_label == "total_exp") {
			
			long temp=  long.Parse(PlayerPrefs.GetString (data_label));
			long total=temp+ long.Parse(input);
			PlayerPrefs.SetString (data_label, total.ToString());
			
			PlayerPrefs.SetString("next_level_up_exp",
			                      GameObject.Find ("controller").GetComponent<level_data_controller>().total_exp_to_next_level_up_exp(total).ToString());
			
			PlayerPrefs.SetInt("max_HP",
			                   GameObject.Find ("controller").GetComponent<level_data_controller>().total_exp_to_max_HP(total));
			
			
			PlayerPrefs.SetInt("attack_point",
			                   GameObject.Find ("controller").GetComponent<level_data_controller>().total_exp_to_attack_point(total));
			
			//PlayerPrefs.SetInt("defence_point",
			//                   GameObject.Find ("controller").GetComponent<level_data_controller>().total_exp_to_attack_point);
			
			
			int previous_level=PlayerPrefs.GetInt("level");
			
			PlayerPrefs.SetInt("level",
			                   GameObject.Find ("controller").GetComponent<level_data_controller>().total_exp_to_level(total));
			if(PlayerPrefs.GetInt ("level")>previous_level){
				text="level_up,"+(PlayerPrefs.GetInt ("level")-previous_level);
			}
			
		}else if(!PlayerPrefs.HasKey(data_label)){
			text="fail";
		}else{
			int temp = PlayerPrefs.GetInt (data_label);
			int total = temp + int.Parse (input);
			
			PlayerPrefs.SetInt (data_label, total);
		}
		
		PlayerPrefs.Save();
		refresh_public_variables();
		
		return text;
	}
	
	public string get_type(int input_slot){
		string output = "error";
		
		if(PlayerPrefs.HasKey("item_slot_"+input_slot+"_type")){
			output=PlayerPrefs.GetString("item_slot_"+input_slot+"_type");
		}
		
		return output;
	}
	
	public string get_sword_no(int input_slot){
		string output = "error";
		
		if(PlayerPrefs.HasKey("item_slot_"+input_slot+"_sword_no")){
			output=PlayerPrefs.GetString("item_slot_"+input_slot+"_sword_no");
		}
		
		return output;
	}
	
	public int get_power_up(int input_slot){
		int output = -1;
		
		if(PlayerPrefs.HasKey("item_slot_"+input_slot+"_power_up")){
			output=PlayerPrefs.GetInt("item_slot_"+input_slot+"_power_up");
		}
		
		return output;
	}
	
	public long get_total_exp(){
		long output = -1;
		
		if(PlayerPrefs.HasKey("total_exp")){
			output=long.Parse(PlayerPrefs.GetString ("total_exp"));
		}
		
		return output;
	}
	
	
	public void refresh_public_variables(){
		
		if (PlayerPrefs.HasKey ("is_initialized")) {
			hero_name = PlayerPrefs.GetString ("hero_name");
			stage_reached = PlayerPrefs.GetInt ("stage_reached");
			stage_cleared = PlayerPrefs.GetInt ("stage_cleared");
			shop_reached = PlayerPrefs.GetInt ("shop_reached"); 
			
			
			total_exp = PlayerPrefs.GetString ("total_exp");
			next_level_up_exp = long.Parse (PlayerPrefs.GetString ("next_level_up_exp"));
			max_HP = PlayerPrefs.GetInt ("max_HP");
			attack_point = PlayerPrefs.GetInt ("attack_point");
			
			level = PlayerPrefs.GetInt ("level");
			money = PlayerPrefs.GetInt ("money");
			
			unchi_stage=PlayerPrefs.GetInt ("unchi_stage");
		}
	}
	
	public int get_sword_equipped_slot(){
		
		//print(PlayerPrefs.GetInt ("sword_equipped"));
		return GetInt ("sword_equipped");
	}
	
	public int get_empty_slot(){
		//bool output = false;
		int empty_slot = -1;
		for (int i=0; i<=PlayerPrefs.GetInt ("max_slot"); i++) {
			if(PlayerPrefs.GetString("item_slot_"+i+"_type")=="null"){
				empty_slot=i;
				break;
			}
		}
		
		
		return empty_slot;
	}

	public string buy_item(string input_item){
		string output = "no_money";

		print (input_item);

		if (input_item == "hatena") {
			
				if (check_has_empty_slot ()) {
						GameObject.Find ("soomla_controller").GetComponent<soomla_controller> ().try_purchase_sword ();
						output = "waiting";
				} else {
						output = "slot_full";
				}

		} else if(GetInt ("money")>=GameObject.Find ("controller").GetComponent<item_data_controller>().item_price(input_item)){

			if(input_item=="match"){

				output = "match_success";
				AddMoney(-GameObject.Find ("controller").GetComponent<item_data_controller>().item_price(input_item));

			

			}else if(input_item=="secret"){
				if(is_boss_009_cleared()){
					output = "secret";
					AddMoney(-GameObject.Find ("controller").GetComponent<item_data_controller>().item_price(input_item));

				}else{
					output = "match_fail";
				}
			}else if(input_item=="ticket"){
				if(is_boss_009_cleared()){
					output = "ticket";
					AddMoney(-GameObject.Find ("controller").GetComponent<item_data_controller>().item_price(input_item));
					
				}else{
					output = "match_fail";
				}
			}else{
				if(add_item(input_item)=="success"){

					output="success";
					AddMoney(-GameObject.Find ("controller").GetComponent<item_data_controller>().item_price(input_item));

				}else{
					output="slot_full";
				}
			}
			//add_item(input_item)
		}

	//	output="sucess";

		return output;
	}




	public string add_item(string input_type){
		string output="fail";
		int empty_slot = -1;
		for (int i=0; i<=PlayerPrefs.GetInt ("max_slot"); i++) {
			if(PlayerPrefs.GetString("item_slot_"+i+"_type")=="null"){
				empty_slot=i;
				break;
			}
		}
		
		if(empty_slot>=0){
			
			if(is_item(input_type)){
				
				PlayerPrefs.SetString("item_slot_"+empty_slot+"_type",input_type);
				PlayerPrefs.SetString("item_slot_"+empty_slot+"_sword_no","null");
				PlayerPrefs.SetInt("item_slot_"+empty_slot+"_power_up",0);
				output = "success";
			}
		}

		PlayerPrefs.Save();
		print (output);
		return output;
		
	}

	public bool check_has_empty_slot(){
		bool output = false;
		int empty_slot = -1;


		for (int i=0; i<=PlayerPrefs.GetInt ("max_slot"); i++) {
			if(PlayerPrefs.GetString("item_slot_"+i+"_type")=="null"){
				empty_slot=i;
				break;
			}
		}

		if (empty_slot >= 0) {
			output=true;
		}

		return output;

	}
	
	public string add_sword( string sword_no,int power_up){
		string output = "success";
		int empty_slot = -1;
		for (int i=0; i<=PlayerPrefs.GetInt ("max_slot"); i++) {
			if(PlayerPrefs.GetString("item_slot_"+i+"_type")=="null"){
				empty_slot=i;
				break;
			}
		}

		if(empty_slot>=0){
			
			PlayerPrefs.SetString("item_slot_"+empty_slot+"_type","sword");
			PlayerPrefs.SetString("item_slot_"+empty_slot+"_sword_no",sword_no);
			PlayerPrefs.SetInt("item_slot_"+empty_slot+"_power_up",power_up);
			
			
		}else{
			output="fail";
		}
		PlayerPrefs.Save();
		return output;
		
		
	}

	
	public bool is_item(string input_type){
		bool output = false;

		
		foreach (string x in item_names){
		
			if(input_type==x){
				output=true;
			}
		}
		
		return output;
	}
	
	public void sword_use_end(){

		disable_is_using_sword ();

		int slot = get_sword_equipped_slot();
		//string temp= GetString("item_slot_"+slot+"_sword_no");
		
		SetString("item_slot_"+slot+"_type","null");
		SetString("item_slot_"+slot+"_sword_no","null");
		SetInt("item_slot_"+slot+"_power_up",0);
		//SetString("item_slot_"+slot+"_sword_no","null");
		
		int temp_slot =-1;
		
		for (int i=0; i<=GetInt("max_slot"); i++) {
			if((GetString("item_slot_"+i+"_type")=="sword"&&GetString("item_slot_"+i+"_sword_no")=="sword_001_01")
			   ||(GetString("item_slot_"+i+"_type")=="sword"&&GetString("item_slot_"+i+"_sword_no")=="sword_001_02")
			   ||(GetString("item_slot_"+i+"_type")=="sword"&&GetString("item_slot_"+i+"_sword_no")=="sword_001_03")
			   ||(GetString("item_slot_"+i+"_type")=="sword"&&GetString("item_slot_"+i+"_sword_no")=="sword_001_04")){
				temp_slot=i;
				
			}
			
		}

		if(temp_slot==-1){
			SetString("item_slot_"+slot+"_type","sword");
			SetString("item_slot_"+slot+"_sword_no","sword_001_01");
		}else{
			SetInt ("sword_equipped",temp_slot);
		}
		
		PlayerPrefs.Save ();
		
	}
	
	public void unchi_hit(){
		int slot = get_sword_equipped_slot();

		if(GetString("item_slot_"+slot+"_sword_no")=="sword_001_01"){
			PlayerPrefs.SetString("item_slot_"+slot+"_sword_no","sword_001_02");
			//PlayerPrefs.SetInt("item_slot_"+slot+"_power_up",0);
			PlayerPrefs.SetString ("map_hinoki","sword_001_02");
			PlayerPrefs.Save ();
		}
		
	}
	
	public void unchi_dead(){
		PlayerPrefs.SetInt ("unchi_stage", Random.Range(1,GetInt ("stage_cleared"))); 
		PlayerPrefs.Save ();
	}


	//public check_


	public string use_item(string input_type,int sword_slot,int previous_slot){

		string output = "";
		//print (input_type);
		if (input_type == "power_up_1") {

			int rnd=Random.Range (5,61);

			power_up_sword(sword_slot,rnd);

			output="+ "+rnd.ToString ();

			if(get_sword_no (sword_slot)=="sword_001_02"){
				SetString("item_slot_"+sword_slot+"_sword_no","sword_001_01");
				PlayerPrefs.SetString ("map_hinoki","sword_001_01");
				if(GameObject.Find ("controller").GetComponent<map_controller>()!=null){
					GameObject.Find ("controller").GetComponent<map_controller>().refresh_hinoki ();
				}

			}

		}else if(input_type == "power_up_10"){

			int rnd=Random.Range (15,181);
			power_up_sword(sword_slot,rnd);

			output="+ "+rnd.ToString();

		}else if(input_type == "sword_up_1"){
			if(sword_rank_up(input_type,get_sword_no (sword_slot))!="null"){
				PlayerPrefs.SetString("item_slot_"+sword_slot+"_sword_no",sword_rank_up(input_type,get_sword_no (sword_slot)));
			
				output="LEVEL UP!";
			}else{
				power_up_sword(sword_slot,150);
				output="+ 150";
			}
		}else if(input_type == "sword_up_2"){

			if(sword_rank_up(input_type,get_sword_no (sword_slot))!="null"){
				PlayerPrefs.SetString("item_slot_"+sword_slot+"_sword_no",sword_rank_up(input_type,get_sword_no (sword_slot)));
				output="LEVEL UP!";
			}else{
				power_up_sword(sword_slot,300);
				output="+ 300";
			}
		}

		PlayerPrefs.SetString("item_slot_"+previous_slot+"_type","null");
		PlayerPrefs.SetString("item_slot_"+previous_slot+"_sword_no","null");
		PlayerPrefs.SetInt("item_slot_"+previous_slot+"_power_up",0);
		PlayerPrefs.Save ();

		return output;
	}

	public string sword_rank_up(string input_item_used,string target_sword ){
		string output = "null";
		
		if(input_item_used=="sword_up_1"){
			if(target_sword=="sword_002_01"){
				output="sword_002_02";
			}else if(target_sword=="sword_003_01"){
				output="sword_003_02";
			}else if(target_sword=="sword_004_01"){
				output="sword_004_02";
			}else if(target_sword=="sword_005_01"){
				output="sword_005_02";
			}else if(target_sword=="sword_006_01"){
				output="sword_006_02";
			}else if(target_sword=="sword_007_01"){
				output="sword_007_02";
			}else if(target_sword=="sword_008_01"){
				output="sword_008_02";
			}
			
		}else if(input_item_used=="sword_up_2"){
			if(target_sword=="sword_002_02"){
				output="sword_002_03";
			}else if(target_sword=="sword_003_02"){
				output="sword_003_03";
			}else if(target_sword=="sword_004_02"){
				output="sword_004_03";
			}else if(target_sword=="sword_005_02"){
				output="sword_005_03";
			}else if(target_sword=="sword_006_02"){
				output="sword_006_03";
			}else if(target_sword=="sword_007_02"){
				output="sword_007_03";
			}else if(target_sword=="sword_008_02"){
				output="sword_008_03";
			}
			
			
		}
		
		return output;
	}


	public void power_up_sword(int input_slot,int power_up_point){

		int temp=GetInt ("item_slot_"+input_slot+"_power_up");
		int temp_power_up_point = power_up_point;
		int limit = 999999;

		if(temp+temp_power_up_point>limit){
			temp_power_up_point=limit-temp;
		}

		AddInt("item_slot_"+input_slot+"_power_up",temp_power_up_point.ToString());

	}
	
	public void replace_item(int input_slot_1, int input_slot_2){
		
		if((input_slot_1>=0&&input_slot_1<=GetInt("max_slot"))&&input_slot_2>=0&&input_slot_2<=GetInt("max_slot")){
			string slot_1_type= GetString("item_slot_"+input_slot_1+"_type");
			string slot_1_sword_no= GetString("item_slot_"+input_slot_1+"_sword_no");
			int slot_1_power_up= GetInt("item_slot_"+input_slot_1+"_power_up");
			
			PlayerPrefs.SetString("item_slot_"+input_slot_1+"_type",GetString("item_slot_"+input_slot_2+"_type"));
			PlayerPrefs.SetString("item_slot_"+input_slot_1+"_sword_no",GetString("item_slot_"+input_slot_2+"_sword_no"));
			PlayerPrefs.SetInt("item_slot_"+input_slot_1+"_power_up",GetInt("item_slot_"+input_slot_2+"_power_up"));
			
			PlayerPrefs.SetString("item_slot_"+input_slot_2+"_type",slot_1_type);
			PlayerPrefs.SetString("item_slot_"+input_slot_2+"_sword_no",slot_1_sword_no);
			PlayerPrefs.SetInt("item_slot_"+input_slot_2+"_power_up",slot_1_power_up);
			
			if(get_sword_equipped_slot()==input_slot_1){
				PlayerPrefs.SetInt("sword_equipped",input_slot_2);
				
				if(GameObject.Find ("hero")!=null){
					
					GameObject.Find ("hero").GetComponent<hero_controller>().sword_slot=input_slot_2;
				}
				//GameObject.Find("controller").GetComponent<item_controller>().refresh_equipment();
			}else if(get_sword_equipped_slot()==input_slot_2){
				PlayerPrefs.SetInt("sword_equipped",input_slot_1);
				//GameObject.Find("controller").GetComponent<item_controller>().refresh_equipment();
				
				if(GameObject.Find ("hero")!=null){
					
					GameObject.Find ("hero").GetComponent<hero_controller>().sword_slot=input_slot_1;
				}
			}
			
			if(GameObject.Find ("item_UI")!=null){
				Destroy(GameObject.Find ("item_UI").gameObject);
				GameObject.Find("controller").GetComponent<item_data_controller>().show_item_list();
			}
			PlayerPrefs.Save ();
			
		}
	}

	public void met_boss_002(){
		PlayerPrefs.SetInt ("met_boss_002", 0);
	}

	public bool is_met_boss_002(){
		bool output = false;
		if(PlayerPrefs.HasKey("met_boss_002")){
			output=true;
		}
		
		return output;
	}

	public void map_001_talk_end(){
		PlayerPrefs.SetInt ("map_001_talk", 0);
	}
	
	public bool is_map_001_talk_end(){
		bool output = false;
		if(PlayerPrefs.HasKey("map_001_talk")){
			output=true;
		}
		
		return output;
	}

	public void map_005_talk_end(){
		PlayerPrefs.SetInt ("map_005_talk", 0);
	}
	
	public bool is_map_005_talk_end(){
		bool output = false;
		if(PlayerPrefs.HasKey("map_005_talk")){
			output=true;
		}
		
		return output;
	}

	public void map_006_talk_end(){
		PlayerPrefs.SetInt ("map_006_talk", 0);
	}
	
	public bool is_map_006_talk_end(){
		bool output = false;
		if(PlayerPrefs.HasKey("map_006_talk")){
			output=true;
		}
		
		return output;
	}

	public void map_007_talk_end(){
		PlayerPrefs.SetInt ("map_007_talk", 0);
	}
	
	public bool is_map_007_talk_end(){
		bool output = false;
		if(PlayerPrefs.HasKey("map_007_talk")){
			output=true;
		}
		
		return output;
	}

	public void map_008_talk_end(){
		PlayerPrefs.SetInt ("map_008_talk", 0);
	}
	
	public bool is_map_008_talk_end(){
		bool output = false;
		if(PlayerPrefs.HasKey("map_008_talk")){
			output=true;
		}
		
		return output;
	}

	public void boss_009_clear(){
		PlayerPrefs.SetInt ("boss_009_cleared", 0);
	}



	public bool is_boss_009_cleared(){
		bool output = false;
		if(PlayerPrefs.HasKey("boss_009_cleared")){
			output=true;
		}

		return output;
	}
	
	
	
}
