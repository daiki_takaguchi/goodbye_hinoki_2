﻿using UnityEngine;
using System.Collections;

public class map_hinoki_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {

		string temp = GameObject.Find ("controller").GetComponent<save_data_controller> ().GetString ("map_hinoki");
		GetComponent<SpriteRenderer>().sprite=(Sprite) Resources.Load("images/sword/" +temp+"/"+temp,typeof(Sprite));

		if(!GameObject.Find("controller").GetComponent<save_data_controller>().HasKey("met_hinoki")){
			Destroy (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
