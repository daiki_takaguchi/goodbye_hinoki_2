﻿using UnityEngine;
using System.Collections;

public class object_controller : MonoBehaviour {

	Vector3 pause_velocity;

	Vector3 target_position;


	Vector3 acceleration;
//	float move_end_time;


	Vector3 previous_velocity;


	float target_time;

	//float random_walk_next_move_time;

	


	public string state="playing";
	public bool move_to_position_flg=false;
	public bool move_to_position_limit_flg = false;
	public bool jump_to_position_y_flg=false;
	public bool random_walk_flg=false;
	public bool is_bounse=false;
	public bool is_screen_limit_set = false;



	public enemy_controller parent_enemy_controller;
	public enemy_magic_controller enemy_magic;


	public  int screen_width=160;
	public int screen_height=128;

	public float limit_x_left;
	public float limit_x_right;
	public float limit_y_up;
	public float limit_y_down;

	public  float charactor_width=16;
	public  float charactor_height=32;

	public float previous_distance;

	public bool is_freeze=false;
	public float freeze_start_time;
	public Vector3 freeze_start_velocity;

	public float delay_time;

	public bool is_timer_set=false;
	public float[] timer_target_time;
	public string[] timer_target;


	private Vector3 move_to_position_target_position;
	private float move_to_position_speed;

	private Vector3 move_to_position_limit_target_position;
	private float move_to_position_limit_speed;
	private float move_to_position_limit_target_time;

	private Vector3 jump_to_position_y_target_position;
	private Vector3 jump_to_position_y_acceleration;
	private Vector3 jump_to_position_y_velocity;




	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(!is_freeze){
			if (move_to_position_flg) {
				move_object();
			}else if(move_to_position_limit_flg){
				move_object_limit();
			}else if(jump_to_position_y_flg){
				jump_object();
			}else if(random_walk_flg){

			}else if(is_bounse){
				//if(is_screen_limit_set){
				check_bounce();


				//}

			}


		}

		//if(is_timer_set){
		check_timer ();
		//}

		 
		
	}

	public void jump_to_position_y(Vector3 input_target_position, Vector3 input_acceleration, Vector3 velocity,float delay){

		//parent_enemy_controller = event_end_call;

		//StartCoroutine (jump_to_position_y_delay(input_target_position,input_acceleration,velocity,delay));

		jump_to_position_y_target_position=input_target_position;
		jump_to_position_y_acceleration=input_acceleration;
		jump_to_position_y_velocity=velocity;

		set_timer (delay,"jump_to_position_y");

	}

	public IEnumerator jump_to_position_y_delay(Vector3 input_target_position, Vector3 input_acceleration, Vector3 velocity,float delay){
		
		yield return new WaitForSeconds(delay);
		
		target_position = input_target_position;
		acceleration = input_acceleration;
		
		rigidbody.velocity = velocity;
		rigidbody.AddForce ( acceleration, ForceMode.Force);
		
		
		jump_to_position_y_flg = true;
		//previous_distance= transform.position.y-target_position.y;
		
		target_position=input_target_position;
		jump_object ();
	}

	public void jump_to_position_y_timer(){
	
		target_position = jump_to_position_y_target_position;
		acceleration = jump_to_position_y_acceleration;
		rigidbody.velocity = jump_to_position_y_velocity;

		rigidbody.AddForce ( acceleration, ForceMode.Acceleration);
	
		jump_to_position_y_flg = true;


		//previous_distance= transform.position.y-target_position.y;
		
		//target_position=input_target_position;
		jump_object ();
	}


	public void jump_object(){

	
		if(transform.position.y-target_position.y<0){
			jump_to_position_y_flg=false;
			rigidbody.velocity=new Vector3(0,0,0);
			transform.position=new Vector3 (transform.position.x,target_position.y,transform.position.z);
			if(parent_enemy_controller!=null){
				parent_enemy_controller.jump_end();
			}
		
		}else{
			rigidbody.AddForce ( acceleration, ForceMode.Acceleration);
		}

		//print (transform.position.y-target_position.y);
		//if((Time.time>target_time&&(transform.position.y,target_position.y> previous_distance)||previous_distance==0){
	}

	public void move_to_position(Vector3 input_target_position, float speed,float delay){
		//StartCoroutine (move_to_position_delay(input_target_position,speed,delay));

		move_to_position_target_position = input_target_position;
		move_to_position_speed = speed;

		set_timer (delay,"move_to_position");
	}





	public IEnumerator move_to_position_delay(Vector3 input_target_position, float speed,float delay){

		yield return new WaitForSeconds(delay);

		Vector3 vel = input_target_position - transform.position;
		target_position = input_target_position;
		rigidbody.velocity = vel * speed;

		move_to_position_flg=true;
		
		previous_distance=Vector3.Distance (transform.position, target_position);
		move_object();
	}

	public void move_to_position_timer(){
		Vector3 vel = move_to_position_target_position - transform.position;
		target_position = move_to_position_target_position;
		rigidbody.velocity = vel * move_to_position_speed;

		move_to_position_flg = true;

		previous_distance=Vector3.Distance (transform.position, target_position);

		move_object ();

	}


	public void move_object(){

		if(Vector3.Distance(transform.position, target_position)>previous_distance||previous_distance==0){
			transform.position=target_position;
			move_to_position_flg=false;
			rigidbody.velocity = new Vector3(0,0,0);

			if(parent_enemy_controller!=null){
				parent_enemy_controller.move_end();
			}

			if(enemy_magic!=null){
				enemy_magic.move_end();
			}
		}
		
		previous_distance=Vector3.Distance(transform.position, target_position);

	}

	public void set_screen_limit(float input_limit_x_left,float input_limit_x_right, float input_limit_y_up, float input_limit_y_down,float input_charactor_width,float input_charactor_height){
		
		is_screen_limit_set = true;
		
		limit_x_left = input_limit_x_left;
		limit_x_right = input_limit_x_right;
		limit_y_up = input_limit_y_up;
		limit_y_down=input_limit_y_down;
		charactor_width = input_charactor_width;
		charactor_height = input_charactor_height;
	}

	public void move_to_position_limit(Vector3 input_target_position, float speed,float input_target_time,float delay){

		//StartCoroutine (move_to_position_limit_delay(input_target_position,speed,input_target_time,delay));
		move_to_position_limit_target_position = input_target_position;
		move_to_position_limit_speed = speed;
		move_to_position_limit_target_time=input_target_time;
	
		set_timer (delay,"move_to_position_limit");

	}
	
	public void move_to_position_limit_timer(){

		Vector3 vel = move_to_position_limit_target_position - transform.position;
		target_position = move_to_position_limit_target_position;
		target_time = Time.time+move_to_position_limit_target_time;

		rigidbody.velocity = vel * move_to_position_limit_speed;

		move_to_position_limit_flg = true;
		
		previous_distance=Vector3.Distance (transform.position, target_position);
		
		move_object_limit();
		
	}
	
	public IEnumerator move_to_position_limit_delay(Vector3 input_target_position, float speed,float input_target_time,float delay){
		
		yield return new WaitForSeconds(delay);

		target_time = Time.time+input_target_time;
		Vector3 vel = input_target_position - transform.position;
		target_position = input_target_position;
		rigidbody.velocity = vel * speed;
		
		move_to_position_limit_flg=true;
		
		previous_distance=Vector3.Distance (transform.position, target_position);
		move_object_limit();
	}




	public void move_object_limit(){
		move_limit ();
		if(Vector3.Distance(transform.position, target_position)>previous_distance||previous_distance==0||Time.time>target_time){
			//transform.position=target_position;
			move_to_position_limit_flg=false;
			rigidbody.velocity = new Vector3(0,0,0);
			
			if(parent_enemy_controller!=null){
				parent_enemy_controller.move_end();
			}
			
		}
		
		previous_distance=Vector3.Distance(transform.position, target_position);
		
	}


	public void random_walk(){
		random_walk_flg = true;
	}

	public void random_walk_update(){
	
	}

	public void move_limit(){
		
		//GameObject target = GameObject.Find("hero");

		if(is_screen_limit_set){
			Vector3 velocity = rigidbody.velocity;
			Vector3 position = transform.position;
			
			if ((transform.position.x<=limit_x_left + (charactor_width/2))&&(velocity.x<0)) {

				velocity.x=0;
				position.x=limit_x_left+(charactor_width/2);

			}else if((transform.position.x>=limit_x_right-(charactor_width/2))&&(velocity.x>0)){
				velocity.x=0;
				position.x=limit_x_right-(charactor_width/2);
			}
			
			if ((transform.position.y<=limit_y_down+(charactor_height/2))&&(velocity.y<0)) {
				velocity.y=0;
				position.y=limit_y_down+(charactor_height/2);
			}else if((transform.position.y>=limit_y_up-(charactor_height/2))&&(velocity.y>0)){
				velocity.y=0;
				position.y=limit_y_up-(charactor_height/2);
			}
			
			transform.position = position;
			rigidbody.velocity = velocity;
		}
		
	}

	public void teleport(){
		float random_x=Random.Range (limit_x_left + (charactor_width/2),limit_x_right-(charactor_width/2));
		float random_y=Random.Range (limit_y_down+(charactor_height/2),limit_y_up-(charactor_height/2));

		transform.position = new Vector3 (random_x, random_y, 0);
	}



	public void check_bounce(){

		if(is_screen_limit_set){
			Vector3 velocity = rigidbody.velocity;
			//Vector3 position = transform.position;
			
			if ((transform.position.x + rigidbody.velocity.x*Time.deltaTime<=limit_x_left + (charactor_width/2))&&(velocity.x<0)) {
				
				velocity.x=-velocity.x;
				//position.x=limit_x_left+(charactor_width/2);
				
			}else if((transform.position.x+rigidbody.velocity.x*Time.deltaTime>=limit_x_right-(charactor_width/2))&&(velocity.x>0)){
				velocity.x=-velocity.x;
			}
			
			if ((transform.position.y +rigidbody.velocity.y*Time.deltaTime<=limit_y_down+(charactor_height/2))&&(velocity.y<0)) {
				velocity.y=-velocity.y;
				//position.y=limit_y_down+(charactor_height/2);
			}else if((transform.position.y+rigidbody.velocity.y*Time.deltaTime>=limit_y_up-(charactor_height/2))&&(velocity.y>0)){
				velocity.y=-velocity.y;
				//position.y=limit_y_up-(charactor_height/2);
			}

				rigidbody.velocity = velocity;
		}

	}

	public void start_freeze(){

		if(!is_freeze){
			freeze_start_velocity = rigidbody.velocity;
			rigidbody.velocity = new Vector3 (0,0,0);
		}

		is_freeze = true;

		freeze_start_time = Time.time;

		start_freeze_timer ();
	}

	public void start_freeze_timer(){
		is_timer_set = false;
		
	}
	
	public void end_freeze(){
		is_freeze = false;
		rigidbody.velocity = freeze_start_velocity;
		//next_attack_time += Time.time - freeze_start_time;
		//Destroy (transform.FindChild ("ice").gameObject);

		end_freeze_timer ();
	}

	public void end_freeze_timer(){
		
		if(timer_target.Length>0){
			
			is_timer_set=true;
			
			for(int i=0;i<timer_target_time.Length;i++){
				timer_target_time[i]+=Time.time-freeze_start_time;
			}
		}
		
	}

	public void set_timer(float input_timer_after ,string input_timer_target){
		is_timer_set = true;

		float[] temp_timer_target_time= new float[timer_target_time.Length+1];
		string[] temp_timer_target=new string[timer_target.Length+1];//=timer_target;

		//string[] 

		int j = 0;

		for(int i=0;i<timer_target.Length;i++){
			j++;
			temp_timer_target_time[i]=timer_target_time[i];
			temp_timer_target[i]=timer_target[i];

		}

		temp_timer_target_time [j] = Time.time + input_timer_after;
		temp_timer_target [j] = input_timer_target;

		timer_target_time = temp_timer_target_time;
		timer_target = temp_timer_target;
	}

	public void check_timer(){
		if (is_timer_set) {
			int non_empty_length=0;
			int[] non_empty= new int[timer_target.Length];

			for(int i=0;i<timer_target.Length;i++){
				if(Time.time>timer_target_time[i]){
					if(timer_target[i]=="jump_to_position_y"){
						jump_to_position_y_timer();
					}else if(timer_target[i]=="move_to_position"){
						move_to_position_timer();
					}else if(timer_target[i]=="move_to_position_limit"){
						move_to_position_limit_timer();
					}

				}else{
					non_empty[non_empty_length]=i;
					non_empty_length++;

				}


			}


			float[] temp_timer_target_time= new float[non_empty_length];
			string[] temp_timer_target=new string[non_empty_length];//=timer_tar

			for(int i=0;i<non_empty_length;i++){
				temp_timer_target_time[i]=timer_target_time[non_empty[i]];
				temp_timer_target[i]=timer_target[non_empty[i]];
			}
			timer_target_time=temp_timer_target_time;
			timer_target=temp_timer_target;

			if(timer_target.Length==0){
				is_timer_set=false;
			}
		

		}


	}





	//public 


	/*
	void OnBecameInvisible(){
		Destroy (gameObject);
		print ("deleted!");
	}
	*/
}
