﻿using UnityEngine;
using System.Collections;

public class BGM_text_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
		refresh_text ();
	}

	public void refresh_text(){
		if (GameObject.Find ("BGM").GetComponent<BGM_controller> ().mute) {
			GetComponent<TextMesh>().text="BGM OFF";
		} else {
			GetComponent<TextMesh>().text="BGM ON";
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
