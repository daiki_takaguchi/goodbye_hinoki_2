﻿using UnityEngine;
using System.Collections;

public class camera_controller : MonoBehaviour {

	public string type;
	public buttons_controller buttons;

	// Use this for initialization
	void Start () {

		//print (camera.aspect);
		//print (Screen.height);


		Application.targetFrameRate = 60;



		//transform.position=new Vector3(0,0,-Screen.height/4);

		if(type=="3D"){
			if(camera.aspect-0.01f<=9.0f/16.0f&&camera.aspect+0.01f>=9.0f/16.0f){
				//print (Screen.height / 2);
			//camera.orthographicSize = (Screen.height / 2);
				//camera.orthographicSize =284 /2;

				transform.position=new Vector3(0,0,-80*(1/(9.0f/16.0f)));
			}else if(camera.aspect-0.01f<=3.0f/4.0f&&camera.aspect+0.01f>=3.0f/4.0f){


				transform.position=new Vector3(0,0,-120);
			}else if(camera.aspect-0.01f<=3.0f/5.0f&&camera.aspect+0.01f>=3.0f/5.0f){


				//camera.orthographicSize =267 /2 ;
				//print (Screen.height / 2);
				//camera.orthographicSize = (Screen.height / 2);

				transform.position=new Vector3(0,0,-80*(1/(3.0f/5.0f)));
			}else if(camera.aspect-0.01f<=5.0f/8.0f&&camera.aspect+0.01f>=5.0f/8.0f){
				//camera.orthographicSize =256/2 ;

				transform.position=new Vector3(0,0,-80*(1/(5.0f/8.0f)));
			}else if(camera.aspect-0.01f<=2.0f/3.0f&&camera.aspect+0.01f>=2.0f/3.0f){
				//print (Screen.height / 2);

				//camera.orthographicSize =240 /2;


				transform.position=new Vector3(0,0,-120);
				//camera.orthographicSize = (Screen.height / 2);
			}else{

				transform.position=new Vector3(0,0,-120);
				//camera.orthographicSize =240 /2;
			}
		}else if(type=="2D"){
			if(camera.aspect-0.01f<=9.0f/16.0f&&camera.aspect+0.01f>=9.0f/16.0f){
				//print (Screen.height / 2);
				//camera.orthographicSize = (Screen.height / 2);
				camera.orthographicSize =284/2;
				
			}else if(camera.aspect-0.01f<=3.0f/4.0f&&camera.aspect+0.01f>=3.0f/4.0f){

			}else if(camera.aspect-0.01f<=3.0f/5.0f&&camera.aspect+0.01f>=3.0f/5.0f){
				
				
				camera.orthographicSize =267/2;
				//print (Screen.height / 2);
				//camera.orthographicSize = (Screen.height / 2);
			}else if(camera.aspect-0.01f<=5.0f/8.0f&&camera.aspect+0.01f>=5.0f/8.0f){
				camera.orthographicSize =256/2;
				
			}else if(camera.aspect-0.01f<=2.0f/3.0f&&camera.aspect+0.01f>=2.0f/3.0f){
				//print (Screen.height / 2);
				
				camera.orthographicSize =240/2;
				
				//camera.orthographicSize = (Screen.height / 2);
			}else{
				camera.orthographicSize =240/2;
			}

			if(buttons!=null){
				buttons.set_position(camera.orthographicSize);
			}

		}



	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
