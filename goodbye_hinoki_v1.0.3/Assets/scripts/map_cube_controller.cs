﻿using UnityEngine;
using System.Collections;

public class map_cube_controller : MonoBehaviour {

	// Use this for initialization

	public Vector3 previous_angle=new Vector3(0,0,0);
	public Vector3 rotated_value =new Vector3(0,0,0);
	public string state="normal";
	//public float position_z;

	void Start () {
		//rigidbody.centerOfMass = new Vector3(0, 0, -60);
		//position_z = transform.position.z;

		//create_front_map ();
	}


	public void start_rotation(string input_state,int map_no){

		GameObject.Find("map_hinoki").layer=5;
		state = input_state;

		create_side_map (map_no);

	}

	public void delete_stage_select_buttons(Transform input_transform){

		Transform parent = input_transform.FindChild ("stage_select_buttons").transform;
		foreach(Transform child in parent){
			if(child.tag=="stage_select_button"){
				//child.GetComponent<UI_button_controller>().

				if(child.GetComponent<UI_button_controller>().stage_no<=PlayerPrefs.GetInt("stage_cleared")){
					child.GetComponent<MeshRenderer>().material=
						(Material) Resources.Load("images/map/Materials/select_stage_2",typeof(Material));
				}


				if(child.GetComponent<UI_button_controller>().stage_no==1){
				
				}else if(child.GetComponent<UI_button_controller>().stage_no>PlayerPrefs.GetInt("stage_cleared")+1){	
					Destroy(child.gameObject);
				}
			}
		}
	}

	public void create_front_map(int map_no){
		Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/map/map_"+map_no,typeof(Transform)) ,
		   	new Vector3(0,0,transform.position.z-80), 
		    Quaternion.Euler(0,0,0));
			temp.parent = transform;
			temp.name = "map_" + (map_no);


			delete_stage_select_buttons (temp);

		/*
		foreach(Transform child in transform){
			if(child.tag=="stage_select_button"){
				
			}
		}
		*/
			
	}

	public void create_side_map(int map_no){

		if(map_no>1){
			Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/map/map_"+(map_no-1), typeof(Transform)) ,
			                                        new Vector3(-80,0,transform.position.z), 
			                                        Quaternion.Euler(0,90,0));
			temp.parent = transform;
			temp.name = "map_" + (map_no-1);
			

			
			delete_stage_select_buttons (temp);
			
			
		}
		
		if(map_no<GameObject.Find ("UI").GetComponent<UI_controller>().max_map_no){
			Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/map/map_"+(map_no+1),   typeof(Transform)) ,
			                                        new Vector3(80,0,transform.position.z), 
			                                        Quaternion.Euler(0,270,0));
			temp.parent = transform;
			temp.name = "map_" + (map_no+1);

			delete_stage_select_buttons (temp);

		}
	}

	// Update is called once per frame
	void Update () {

		float rotation_speed = 60;

		if(state=="map_rotate_right"){

			transform.Rotate(new Vector3(0, rotation_speed * Time.deltaTime, 0));

			rotated_value=rotated_value+new  Vector3(0,rotation_speed * Time.deltaTime, 0);

			if(rotated_value.magnitude>90){

				GameObject.Find("map_hinoki").layer=0;

				//int max_map_no=1;
				int map_no=GameObject.Find ("UI").GetComponent<UI_controller>().map_shown;


				if(map_no>1){
					Destroy(GameObject.Find ("map_"+(map_no-1)).gameObject);
				}

				if(map_no>2){
					Destroy(GameObject.Find ("map_"+(map_no-2)).gameObject);
				}



				GameObject.Find ("UI").GetComponent<UI_controller>().state="normal";
				state="normal";
				rotated_value=new Vector3(0,0,0);
				/*
				transform.position = new Vector3(
					round_position(transform.position.x),
					round_position(transform.position.y),
					round_position(transform.position.z));
				*/

				transform.rotation= Quaternion.Euler(
					round_angle(transform.rotation.eulerAngles.x),
					round_angle(transform.rotation.eulerAngles.y),
					round_angle(transform.rotation.eulerAngles.z));


				GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM("map_"+map_no);
			}


		}else if(state=="map_rotate_left"){

			transform.Rotate(new Vector3(0, -rotation_speed * Time.deltaTime, 0));
			
			rotated_value=rotated_value+new  Vector3(0,-rotation_speed * Time.deltaTime, 0);


			if(rotated_value.magnitude>90){

				GameObject.Find("map_hinoki").layer=0;
				//int max_map_no=1;
				int max_map_no=GameObject.Find ("UI").GetComponent<UI_controller>().max_map_no;
				int map_no=GameObject.Find ("UI").GetComponent<UI_controller>().map_shown;
				

				if(map_no<max_map_no){
					Destroy(GameObject.Find ("map_"+(map_no+1)).gameObject);
				}
				
				if(map_no<max_map_no-1){
					Destroy(GameObject.Find ("map_"+(map_no+2)).gameObject);
				}
				

				
				GameObject.Find ("UI").GetComponent<UI_controller>().state="normal";
				state="normal";
				rotated_value=new Vector3(0,0,0);
				/*
				transform.position = new Vector3(
					round_position(transform.position.x),
					round_position(transform.position.y),
					round_position(transform.position.z));
				*/
				transform.rotation= Quaternion.Euler(
					round_angle(transform.rotation.eulerAngles.x),
					round_angle(transform.rotation.eulerAngles.y),
					round_angle(transform.rotation.eulerAngles.z));

				GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM("map_"+map_no);
				
			}
		}else if(state=="map_move_up"){
			transform.position=transform.position+ new Vector3(0,160*Time.deltaTime,0);
			if(transform.position.y>200){
				transform.parent.GetComponent<UI_controller>().state="normal";
				Destroy (gameObject);
			}
		}
	}

	public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion angle) {
		return angle * ( point - pivot) + pivot;
	}

	public float round_position(float input){
		return Mathf.Round(input/40.0f)*40.0f;
	}

	public float round_angle(float input){
		return Mathf.Round(input/30.0f)*30.0f;
	}

	public void move_up(){
		state = "map_move_up";

		Instantiate(( Transform) Resources.Load ("prefab/UI/etc/last_stage_select_button",  typeof(Transform)) , new Vector3(0,-24,-1),Quaternion.identity );
		//transform.Rotate (new Vector3 (0,10,0));
		//StartCoroutine (destroy_cube());
	}


}
