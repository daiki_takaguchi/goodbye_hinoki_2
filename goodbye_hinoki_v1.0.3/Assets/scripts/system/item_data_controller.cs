﻿using UnityEngine;
using System.Collections;

public class item_data_controller : MonoBehaviour {

	public Transform item_box;
	public Transform item_box_for_use;
	
	public Transform[] item_box_array;
	
	int[] item_box_position = { -51, -17, 17,51};
	Transform equipped;
	
	public save_data_controller save_data;

	private static string[] item_names={"hatena","secret","power_up_1","power_up_10","sword_up_1","sword_up_2","match","ticket"};

	 

	// Use this for initialization
	void Start () {
		//item_box = (Transform) Resources.Load ("prefab/UI/common_windows/item_box", typeof(Transform));
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public bool is_item(string input_type){
		bool output = false;
		
		
		foreach (string x in item_names){
			//print (x);
			if(input_type==x){
				output=true;
			}
		}
		
		return output;
	}
	
	public void show_shop_item_list(){
		
		
		GameObject temp_gameobject= new GameObject();
		temp_gameobject.name = "shop_UI";
		Transform temp = temp_gameobject.transform;
		temp.parent = GameObject.Find ("UI").transform;
		
		
		int[] shop_item_position = {-29, -63};
		Transform[] shop_item_box_array= new Transform[16];
		
		//Transform child;
		UI_button_controller button;
		
		for (int i=0; i<=1; i++) {
			for (int j=0; j<=3; j++) {
				
				int item_slot_no= i*4+j;
				
				shop_item_box_array[item_slot_no] = (Transform) Instantiate(item_box, new Vector3(item_box_position[j], shop_item_position[i], 0), Quaternion.identity);
				shop_item_box_array[item_slot_no].parent= temp;
				
				shop_item_box_array[item_slot_no].tag="shop_item_box";
				shop_item_box_array[item_slot_no].GetComponent<SpriteRenderer>().sortingLayerName="shop_window";
				shop_item_box_array[item_slot_no].FindChild ("item").GetComponent<SpriteRenderer>().sortingLayerName="shop_text";
				
				//child=shop_item_box_array[item_slot_no].FindChild("item");
				
				button=shop_item_box_array[item_slot_no].GetComponent<UI_button_controller>();
				button.type="shop_item_box";
				button.item_type="null";
				button.item_slot_no=item_slot_no;
				button.set_layer_no (0);
				
				
				
			}
		}
		
		shop_item_box_array[0].FindChild ("item").GetComponent<SpriteRenderer>().sprite=
			(Sprite) Resources.Load("images/item/hatena", typeof(Sprite));
		shop_item_box_array[0].GetComponent<UI_button_controller>().item_type="hatena";

		shop_item_box_array[1].FindChild ("item").GetComponent<SpriteRenderer>().sprite=
			(Sprite) Resources.Load("images/item/secret", typeof(Sprite));
		shop_item_box_array[1].GetComponent<UI_button_controller>().item_type="secret";

		shop_item_box_array[2].FindChild ("item").GetComponent<SpriteRenderer>().sprite=
			(Sprite) Resources.Load("images/item/power_up_1", typeof(Sprite));
		shop_item_box_array[2].GetComponent<UI_button_controller>().item_type="power_up_1";
		
		shop_item_box_array[3].FindChild ("item").GetComponent<SpriteRenderer>().sprite=
			(Sprite) Resources.Load("images/item/power_up_10", typeof(Sprite));
		shop_item_box_array[3].GetComponent<UI_button_controller>().item_type="power_up_10";
		
		shop_item_box_array[4].FindChild ("item").GetComponent<SpriteRenderer>().sprite=
			(Sprite) Resources.Load("images/item/sword_up_1", typeof(Sprite));
		shop_item_box_array[4].GetComponent<UI_button_controller>().item_type="sword_up_1";
		
		shop_item_box_array[5].FindChild ("item").GetComponent<SpriteRenderer>().sprite=
			(Sprite) Resources.Load("images/item/sword_up_2", typeof(Sprite));
		shop_item_box_array[5].GetComponent<UI_button_controller>().item_type="sword_up_2";



		shop_item_box_array[6].FindChild ("item").GetComponent<SpriteRenderer>().sprite=
			(Sprite) Resources.Load("images/item/match", typeof(Sprite));
		shop_item_box_array[6].GetComponent<UI_button_controller>().item_type="match";

		shop_item_box_array[7].FindChild ("item").GetComponent<SpriteRenderer>().sprite=
			(Sprite) Resources.Load("images/item/ticket", typeof(Sprite));
		shop_item_box_array[7].GetComponent<UI_button_controller>().item_type="ticket";
		
		//button=shop_item_box_array[0].GetComponent<UI_button_controller>();
		
		
		
		
	}
	
	
	public void show_item_list_for_use(string input_type,int input_slot){
		
		save_data_controller save_data=GameObject.Find ("controller").GetComponent<save_data_controller> ();
		
		GameObject temp_gameobject= new GameObject();
		temp_gameobject.name = "item_UI";
		Transform temp = temp_gameobject.transform;
		
		temp.parent = GameObject.Find ("UI").transform;
		
		item_box_array= new Transform[16];
		Transform child;
		UI_button_controller button;
		
		for (int i=0; i<=3; i++) {
			for (int j=0; j<=3; j++) {
				
				int item_slot_no= i*4+j;
				
				item_box_array[item_slot_no] = (Transform) Instantiate(item_box, new Vector3(item_box_position[j], item_box_position[3-i], 0), Quaternion.identity);
				item_box_array[item_slot_no].name="item_box_for_use";
				item_box_array[item_slot_no].parent= temp;



				child=item_box_array[item_slot_no].FindChild("item");
				
				button=item_box_array[item_slot_no].GetComponent<UI_button_controller>();
				
				button.type="item_box_for_use";
				button.item_slot_no=item_slot_no;
				button.item_type=save_data.GetString("item_slot_"+item_slot_no+"_type");
				button.item_sword_no=save_data.GetString("item_slot_"+item_slot_no+"_sword_no");
				
				if(save_data.GetInt("sword_equipped")==item_slot_no){
					
					equipped=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/equipped",  typeof(Transform)) , item_box_array[item_slot_no].position,Quaternion.identity );
					equipped.parent=item_box_array[item_slot_no];
				}
				
				if (button.item_type == "sword"){

					bool applicable=true;

					if(GameObject.Find ("hero")!=null){
						if(save_data.get_sword_equipped_slot()==item_slot_no&&GameObject.Find ("hero").GetComponent<hero_controller>().is_using_sword){
					
						string sword_no=button.item_sword_no;

						child.GetComponent<SpriteRenderer>().sprite= (Sprite) Resources.Load("images/sword/"+ sword_no + "/"+ sword_no, typeof(Sprite));
						child.renderer.material.color=new Color(0.5f,0.5f,0.5f);
						Destroy (item_box_array[item_slot_no].GetComponent<UI_button_controller>());
						applicable=false;
						}
					}
					
					if(applicable){
						string sword_no=button.item_sword_no;
						child.GetComponent<SpriteRenderer>().sprite= (Sprite) Resources.Load("images/sword/"+ sword_no + "/"+ sword_no, typeof(Sprite));
					}


				}else if(is_item (button.item_type)) {


					child.GetComponent<SpriteRenderer>().sprite= (Sprite) Resources.Load("images/item/"+button.item_type, typeof(Sprite));

					child.renderer.material.color=new Color(0.5f,0.5f,0.5f);

					Destroy (item_box_array[item_slot_no].GetComponent<UI_button_controller>());
				}else {	
					Destroy (item_box_array[item_slot_no].GetComponent<UI_button_controller>());
				}



				if(input_slot==item_slot_no){

					print (input_slot);

					Transform selected=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/selected",  typeof(Transform)) , item_box_array[item_slot_no].position,Quaternion.identity );
					selected.name="item_box_selected";
					selected.parent=temp_gameobject.transform;
				}


			}
		
		}
		
	}




	public Transform show_item_list(){
		
		save_data_controller save_data=GameObject.Find ("controller").GetComponent<save_data_controller> ();
		
		GameObject temp_gameobject= new GameObject();
		temp_gameobject.name = "item_UI";
		Transform temp = temp_gameobject.transform;
		
		temp.parent = GameObject.Find ("UI").transform;
		
		item_box_array= new Transform[16];
		Transform child;
		UI_button_controller button;
		
		for (int i=0; i<=3; i++) {
			for (int j=0; j<=3; j++) {
				
				int item_slot_no= i*4+j;
				
				item_box_array[item_slot_no] = (Transform) Instantiate(item_box, new Vector3(item_box_position[j], item_box_position[3-i], 0), Quaternion.identity);
				item_box_array[item_slot_no].name="item_box";
				item_box_array[item_slot_no].parent= temp;
				
				child=item_box_array[item_slot_no].FindChild("item");
				
				button=item_box_array[item_slot_no].GetComponent<UI_button_controller>();
				
				button.type="item_box";
				button.item_slot_no=item_slot_no;
				button.item_type=save_data.GetString("item_slot_"+item_slot_no+"_type");
				button.item_sword_no=save_data.GetString("item_slot_"+item_slot_no+"_sword_no");
				
				if(save_data.GetInt("sword_equipped")==item_slot_no){
					
					//print ("show"+save_data.GetInt("sword_equipped"));
					
					equipped=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/equipped",  typeof(Transform)) , item_box_array[item_slot_no].position,Quaternion.identity );
					equipped.parent=item_box_array[item_slot_no];
				}
				
				if (button.item_type == "sword") {
					string sword_no=button.item_sword_no;
					child.GetComponent<SpriteRenderer>().sprite= (Sprite) Resources.Load("images/sword/"+ sword_no + "/"+ sword_no, typeof(Sprite));
					
				}else if(is_item (button.item_type)) {
					child.GetComponent<SpriteRenderer>().sprite= (Sprite) Resources.Load("images/item/"+button.item_type, typeof(Sprite));
				}else {	
					//Destroy (item_box_array[item_slot_no].GetComponent<UI_button_controller>());
				}
				
				
				
				//PlayerPrefs.GetInt("max_slot");
				
			}
		}

		return temp_gameobject.transform;
		
	}
	
	public void refresh_equipment(){
		if(GameObject.Find ("item_UI")!=null){
			Destroy (equipped.gameObject);
			
			int slot=save_data.get_sword_equipped_slot();
			
			GameObject[] temps=GameObject.FindGameObjectsWithTag("item_box");
			
			for (int i=0; i<temps.Length; i++) {

				if(temps[i].GetComponent<UI_button_controller>()!=null){
					if(temps[i].GetComponent<UI_button_controller>().item_slot_no==slot){
						//if(i==slot){
						
						equipped=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/equipped",  typeof(Transform)) , temps[i].transform.position,Quaternion.identity );
						equipped.parent=temps[i].transform;
					}
				}
			}
			
			
		}
	}




























	public static string[] get_item_names(){
		return item_names;
	}

	public int sword_attack_point(string input_sword_no){
		return int.Parse (get_data ("sword", input_sword_no, "attack_point"));

	}

	public string about_sword(string input_sword_no){
		return get_data ("sword", input_sword_no, "about");
	}

	public string sword_name(string input_sword_no){
		return get_data ("sword", input_sword_no, "name");
	}

	public string about_item(string input_type){
		return get_data (input_type,"", "about");
	}

	public string item_name(string input_type){
		return get_data (input_type,"", "name");
	}

	public int item_price(string input_type){
		return int.Parse(get_data(input_type,"", "price"));
	}

	public string item_price_text(string input_type){
		string text = "";
		if(input_type=="hatena"){
			text=get_data(input_type,"", "price")+"えん";
		}else{
			text=get_data(input_type,"", "price")+"G";
		}

		return text;
	}

	public string get_data(string type,string input_no,string label){

		string output = "0";

		if(type=="sword"){

			if (input_no=="sword_001_01") {
				if(label=="name"){
					output="ひのきくん";
				}else if(label=="attack_point"){
					output="20";
				}else if(label=="about"){
					output="しゅじんこうのピンチをたすけてくれた\n" +
						"しゃべるひのきのぼう。\n" +
						"おふろにもっていくといいかおり。";
				}

			}else if (input_no=="sword_001_02") {
					if(label=="name"){
						output="よごれちまった ひのきくん";
					}else if(label=="attack_point"){
						output="1";
					}else if(label=="about"){
					output="バッチィてきへのこうげきにつかわれ、\n" +
						"ちからをうしなったひのきのぼう。\n" +
						"よごれちまったかなしみに……。";
					}

			}else if (input_no=="sword_001_03") {
				if(label=="name"){
					output="サイプレス・カタフォーク";
				}else if(label=="attack_point"){
					output="10000";
				}else if(label=="about"){
					output="でんせつのしんけん。じゃあくな\nてきを じゃくめつのひつぎにほうむる\nことができる ゆいいつのけん。";
				}

			}else if (input_no=="sword_001_04") {
				if(label=="name"){
					output="しゃべらないひのきのぼう";
				}else if(label=="attack_point"){
					output="20";
				}else if(label=="about"){
					output="しゃべらない、ふつうのひのきのぼう。\n" +
						"まおうをたおしたときにひろった。\n" +
						"どこかさびしげだ。";
				}
				
			}else if (input_no=="sword_002_01") {
				if(label=="name"){
					output="アイアンソード　レベル1";
				}else if(label=="attack_point"){
					output="40";
				}else if(label=="about"){
					output="てつでできたけん。\n" +
						"のうりょくかいほうで でかくなる。\n" +
						"たいどは ひのきのほうがでかい。";
				}

			}else if (input_no=="sword_002_02") {
				if(label=="name"){
					output="アイアンソード　レベル2";
				}else if(label=="attack_point"){
					output="100";
				}else if(label=="about"){
					output="てつでできたけん。のうりょくかい\n" +
						"ほうでビッグになる。YAZAWA\n" +
							"そうおもうわけ。よろしく！";
				}

			}else if (input_no=="sword_002_03") {
				if(label=="name"){
					output="アイアンソード　レベル3";
				}else if(label=="attack_point"){
					output="200";
				}else if(label=="about"){
					output="てつでできたけん。\n" +
						"のうりょくかいほうでおおきくなる。\n" +
							"すごく…おおきいです…。";
				}

			}else if (input_no=="sword_003_01") {
				if(label=="name"){
					output="ファイアソード　レベル1";
				}else if(label=="attack_point"){
					output="50";
				}else if(label=="about"){
					output="ほのおにつつまれたけん。\n" +
						"のうりょくかいほうで ひをはなつ。\n" +
						"ふるえるぞハート！";
				}

			}else if (input_no=="sword_003_02") {
				if(label=="name"){
					output="ファイアソード　レベル2";
				}else if(label=="attack_point"){
					output="110";
				}else if(label=="about"){
					output="ほのおにつつまれたけん。\n" +
						"のうりょくかいほうで ひをはなつ。\n" +
							"もえつきるほどヒート！！";
				}

			}else if (input_no=="sword_003_03") {
				if(label=="name"){
					output="ファイアソード　レベル3";
				}else if(label=="attack_point"){
					output="210";
				}else if(label=="about"){
					output="ほのおにつつまれたけん。\n" +
						"のうりょくかいほうで ひをはなつ。\n" +
							"きざむぞ けつえきのビート！！！";
				}

			}else if (input_no=="sword_004_01") {
				if(label=="name"){
					output="ウッドソード　レベル1";
				}else if(label=="attack_point"){
					output="60";
				}else if(label=="about"){
					output="もりのかおりがするけん。\n" +
						"のうりょくかいほうでこのはをとばす。\n"+
						"このははじみにトゲトゲでいたい。";
				}

			}else if (input_no=="sword_004_02") {
				if(label=="name"){
					output="ウッドソード　レベル2";
				}else if(label=="attack_point"){
					output="120";
				}else if(label=="about"){
					output="もりのかおりがするけん。\n" +
						"のうりょくかいほうでこのはをとばす。\n"+
							"ささったら、とげがぬけない。";
				}

			}else if (input_no=="sword_004_03") {
				if(label=="name"){
					output="ウッドソード　レベル3";
				}else if(label=="attack_point"){
					output="220";
				}else if(label=="about"){
					output="もりのかおりがするけん。\n" +
						"のうりょくかいほうでこのはをとばす。\n"+
							"うらには　けむしがびっしり。";
				}

			}else if (input_no=="sword_005_01") {
				if(label=="name"){
					output="セイントソード　レベル1";
				}else if(label=="attack_point"){
					output="70";
				}else if(label=="about"){
					output="せいなるけん。のうりょくかいほうで\n" +
						"きずをちりょうする。\n" +
						"こころのきずをのぞいて。 ";
				}

			}else if (input_no=="sword_005_02") {
				if(label=="name"){
					output="セイントソード　レベル2";
				}else if(label=="attack_point"){
					output="130";
				}else if(label=="about"){
					output="せいなるけん。きずをいやす。\n" +
						"でも、いちばんいやしてほしいのは\n" +
							"このむねの いたみなんだよ！！";
				}

			}else if (input_no=="sword_005_03") {
				if(label=="name"){
					output="セイントソード　レベル3";
				}else if(label=="attack_point"){
					output="230";
				}else if(label=="about"){
					output="せいなるけん。きずをなおす。でも、\n" +
						"この こころのきずあとが きれいに\n" +
							"なることなんてない。";

						
				}

			}else if (input_no=="sword_006_01") {
				if(label=="name"){
					output="アイスランス　レベル1";
				}else if(label=="attack_point"){
					output="80";
				}else if(label=="about"){
					output="こおりのやり。のうりょくかいほうじに\n" +
						"こうげきすると、ありのままで\n" +
						"てきがこおる。すこしもさむくないわ。";
				}
			
			}else if (input_no=="sword_006_02") {
				if(label=="name"){
					output="アイスランス　レベル2";
				}else if(label=="attack_point"){
					output="140";
				}else if(label=="about"){
					output="こおりのやり。のうりょくかいほうじに\n" +
						"こうげきするとてきがこおる。\n" +
						"コッチコチやぞ！！";
				}
			
			}else if (input_no=="sword_006_03") {
				if(label=="name"){
					output="アイスランス　レベル3";
				}else if(label=="attack_point"){
					output="240";
				}else if(label=="about"){
					output="こおりのやり。エターナルフォース\n" +
						"ブリザードで　てきがこおる。\n" +
						"あいてはしぬ。（ウソ）";
				}

			}else if (input_no=="sword_007_01") {
				if(label=="name"){
					output="ダークブレード　レベル1";
				}else if(label=="attack_point"){
					output="90";
				}else if(label=="about"){
					output="やみのけん。のうりょくかいほうで\n" +
						"しんでもいちどだけよみがえる。\n" +
							"こうげきりょくも　おおはばアップ。";
				}

			}else if (input_no=="sword_007_02") {
				if(label=="name"){
					output="ダークブレード　レベル2";
				}else if(label=="attack_point"){
					output="150";
				}else if(label=="about"){
					output="やみのけん。のうりょくかいほうで\n" +
						"しんでもいちどだけよみがえる。\n" +
							"めいどうざんげつはは つかえない。";
				}
			}else if (input_no=="sword_007_03") {
				if(label=="name"){
					output="ダークブレード　レベル3";
				}else if(label=="attack_point"){
					output="250";
				}else if(label=="about"){
					output="にんげんが いきものの\nいきしにを じゆうにしようなんて\nおこがましいとおもわんかね……。";
				}

			}else if (input_no=="sword_008_01") {
				if(label=="name"){
					output="マッドアックス　レベル1";
				}else if(label=="attack_point"){
					output="100";
				}else if(label=="about"){
					output="ちのにおいがするおの。\nのうりょくかいほうでグルグルする。\nHere's Johnny!";
				}

			}else if (input_no=="sword_008_02") {
				if(label=="name"){
					output="マッドアックス　レベル2";
				}else if(label=="attack_point"){
					output="200";
				}else if(label=="about"){
					output="きょうきのにおいがするおの。のう\nりょくかいほうでグルグルしまくる。\nメルギブソンのえいがとはむかんけい。";
				}

			}else if (input_no=="sword_008_03") {
				if(label=="name"){
					output="マッドアックス　レベル3";
				}else if(label=="attack_point"){
					output="300";
				}else if(label=="about"){
					output="あくまのにおいがするおの。のうりょく\nかいほうで ちょうグルグルする。\nはらわたをブチまけろ！！";
				}

			}else if (input_no=="sword_009_01") {
				/*
				if(label=="name"){
					output="マッチぼう";
				}else if(label=="attack_point"){
					output="3";
				}else if(label=="about"){
					output="マッチうりのしょうじょをボコって\nてにいれたマッチ。さくしゃの\nぜんさくゲームさんしょう。";
				}*/

			}else if (input_no=="sword_010_01") {
				if(label=="name"){
					output="マッチぼう";
				}else if(label=="attack_point"){
					output="1000";
				}else if(label=="about"){
					output="マッチうりのしょうじょをボコって\nてにいれたマッチ。さくしゃの\nぜんさくゲームさんしょう。";
				}
			}else if (input_no=="sword_011_01") {
				if(label=="name"){
					output="クロベ";
				}else if(label=="attack_point"){
					output="100000";
				}else if(label=="about"){
					output="ゲームのラストまえにラスボスを\nたおしてしまったあかし。\nあんたはえらい！！";
				}
			}else if (input_no=="sword_011_02") {
				if(label=="name"){
					output="イブキ";
				}else if(label=="attack_point"){
					output="10000";
				}else if(label=="about"){
					output="つぎつぎに　おそいかかる\nラスボスたちを　たおしたあかし。\nあんたはすごくえらい！！";
				}
			}
	
		}else{
			if(type=="hatena"){
				if(label=="name"){
					output="バランスほうかいのくじ";
				}else if(label=="price"){
					output="300";
				}else if(label=="about"){
					output="かうとランダムでちょうつよいけんが\nてにはいる。さくしゃのごはんだいに\nなるのでかってください！！";
				}

			}else if(type=="secret"){
				if(label=="name"){
					output="ごくひじょうほう";
				}else if(label=="price"){
					output="2000";
				}else if(label=="about"){
					output="もんのすごい　ヒミツの\nじょうほうをおしえるゼニ！\nゲームクリアしてからゼニ！";
				}

			}else if(type=="power_up_1"){
				if(label=="name"){
					output="ちからのルビー";
				}else if(label=="price"){
					output="100";
				}else if(label=="about"){
					output="けんにつかうと、こうげきりょくが\n5~60あがる。よごれたものを\nきれいにするちからもある。";
				}

			}else if(type=="power_up_10"){

				if(label=="name"){
					output="ごうりきのサファイア";
				}else if(label=="price"){
					output="300";
				}else if(label=="about"){
					output="けんにつかうと、こうげきりょくが\n15~180あがる。むかしとある\nアイドルがもっていたらしい。";
				}

			}else if(type=="sword_up_1"){
				if(label=="name"){
					output="パーライトのけっしょう";
				}else if(label=="price"){
					output="500";
				}else if(label=="about"){
					output="レベル1のけんをレベル2にする。\nまたはこうげきりょくを150あげる。\nじつぶつはいろがちがう。";
				}
			
			}else if(type=="sword_up_2"){
				if(label=="name"){
					output="マルテンサイトのけっしょう";
				}else if(label=="price"){
					output="1000";
				}else if(label=="about"){
					output="レベル2のけんをレベル3にする。\nまたはこうげきりょくを300あげる。\nいろはぬられただけ。";
				}
			
			}else if(type=="match"){
				if(label=="name"){
					output="なぞのマッチ";
				}else if(label=="price"){
					output="2000";
				}else if(label=="about"){
					output="かうと、いせかいにとばされ\nだれかにあえるらしい。\n";
				}
			}else if(type=="ticket"){
				if(label=="name"){
					output="しのしょうたいじょう";
				}else if(label=="price"){
					output="2000";
				}else if(label=="about"){
					output="あなたを　しのきょうふに\nごしょうたいいたします。\nゲームクリアしないとかえない。";
				}
				
			
			}
		
		}


		return output;
	}
	
}
