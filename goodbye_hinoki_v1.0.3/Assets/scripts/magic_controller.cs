﻿using UnityEngine;
using System.Collections;

public class magic_controller : MonoBehaviour {

	public string type;
	public string state;

	public float next_attack_time;

	public Vector3 rotation_pivot;
	public Vector3 next_fire_position;

	public int attack_count;
	public int attack_count_limit;
	
	public long attack_point;
	public float remainging_life_time;
	public bool is_remaining_life_time_enabled=true;

	public int limit_x_left;
	public int limit_x_right;
	public int limit_y_up;
	public int limit_y_down;

	public Sprite[] sprite;

	// Use this for initialization
	void Start () {

		gameObject.layer = 10;

		if(is_remaining_life_time_enabled){
			remainging_life_time += Time.time;
		}

		if (type=="fire"){
			next_attack_time=Time.time+ 0.25f;
		}else if (type=="leaf"){
			//rigidbody.velocity=(GameObject.Find ("hero").transform.position-transform.position).normalized*16.0f;
		}else if (type=="leaf_creater"){
			attack_count=0;
			//attack_count_limit=3;
		}



	}


	
	// Update is called once per frame
	void Update () {
		check_life_time ();
		update_magic ();
	}


	public void enable_life_time(float input_life_time){


		is_remaining_life_time_enabled = true;
		if(input_life_time>=0){
			remainging_life_time = Time.time+input_life_time;
		}else{
			remainging_life_time+=Time.time;
		}
	}

	public void check_life_time(){
		if (Time.time > remainging_life_time&&is_remaining_life_time_enabled) {

			Destroy(gameObject);
		}
	}

	public void update_magic(){

		if (type == "fire") {
			char[] separators = ",".ToCharArray();
			string[] temp=state.Split(separators);
			int temp_2=int.Parse(temp[1]);
			int temp_3=int.Parse(temp[2]);
			
			if(temp_3>10){
				Destroy (gameObject);
			}
			if(Time.time>next_attack_time){
				
				state=temp[0]+","+(temp_2+1)+","+temp_3;
				if(temp_2==1){
					next_attack_time=Time.time+1.0f;
					GetComponent<SpriteRenderer>().sprite =sprite[0];
					

					Vector3 temp_next_position=transform.position+next_fire_position;
					
					Transform fire =(Transform) Instantiate(( Transform) Resources.Load ("prefab/sword/sword_003_01/fire",typeof(Transform)) ,temp_next_position,Quaternion.identity );
					fire.GetComponent<magic_controller>().attack_point=attack_point;
					fire.GetComponent<magic_controller>().state=temp[0]+",1,"+(temp_3+1);
					fire.GetComponent<magic_controller>().next_fire_position=next_fire_position;
				}else if(temp_2==2){
					next_attack_time=Time.time+ 0.25f;
					GetComponent<SpriteRenderer>().sprite =sprite[1];
				}else if(temp_2==3){
					Destroy (gameObject);
					//GetComponent<SpriteRenderer>().sprite =sprite[1];
				}
			}
		}else if (type=="leaf_creater"){

			if(Time.time>next_attack_time){
			
				if(attack_count>attack_count_limit){
					Destroy (gameObject);
				}else{
			
					next_attack_time=Time.time+0.2f;

					Vector3 temp_position= GameObject.Find("hero").transform.position+new Vector3(16,0,0);
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/sword/sword_004_01/leaf",typeof(Transform)) ,temp_position,Quaternion.identity );
					temp.GetComponent<magic_controller>().attack_point=attack_point;

					GameObject[] temps= GameObject.FindGameObjectsWithTag("enemy");

					Vector3 velocity=new Vector3(64.0f,0,0);

					if(temps.Length==0){
						velocity=new Vector3(64,0,0);
					}else{
						if(temps.Length>attack_count){
							velocity=(temps[attack_count].transform.position-temp_position).normalized*64.0f;
						}else{
							velocity=(temps[0].transform.position-temp_position).normalized*64.0f;
						}
					}

					temp.rigidbody.velocity=velocity;

				}
				//print("aaa"+attack_count_limit);
				//print("aaa"+attack_count);
				attack_count++;
			}


		}
	
	}

	
}
