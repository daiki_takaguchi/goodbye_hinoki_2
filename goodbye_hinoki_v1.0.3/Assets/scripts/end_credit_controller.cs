﻿using UnityEngine;
using System.Collections;

public class end_credit_controller : MonoBehaviour {

	// Use this for initialization

	public TextMesh textmesh;
	public bool is_normal=true;

	void Start () {
		StartCoroutine(wait (2.0f,0));
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void next_text(int next){

		string text = "";

		if (next == 0) {
			
			text ="げんさく\n\nさくしゃ";
		}else if(next ==1){
			text ="きゃくしょく\n\nさくしゃ";
		}else if(next ==2){
			text ="かんとく\n\nさくしゃ";
		}else if(next ==3){
			text ="えんしゅつ\n\nさくしゃ";
		}else if(next ==4){
			text ="おんがく\n\nむりょうそざい";
		}else if(next ==5){
			text ="THE END";
		}
		textmesh.text = text;

		if(next<6){
			StartCoroutine (wait(2.0f,next+1));
		}else{
			//GameObject.Find("hero").GetComponent<hero_controller>().disable_is_event();
			//GameObject.Find("hero").GetComponent<hero_controller>().is_initialized=false;
			if(is_normal){
				Destroy(GameObject.Find ("hero").transform.FindChild("sword").gameObject);
				Destroy(GameObject.Find ("hero").transform.FindChild("aura").gameObject);
				Transform temp= (Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/etc/boss_009_sword",  typeof(Transform)), new Vector3(GameObject.Find ("hero").transform.position.x,200,0),Quaternion.identity  );
				temp.rigidbody.velocity=new Vector3(0,-16,0);
			}else{

				GameObject.Find ("controller").GetComponent<battle_controller>().return_to_map_pressed(1.0f);

			}
			Destroy(gameObject);
			
		};
	}

	public IEnumerator wait(float wait_time,int next){
		yield return new WaitForSeconds(wait_time);

		next_text (next);
	}
}
