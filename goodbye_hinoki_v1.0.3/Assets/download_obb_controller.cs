﻿using UnityEngine;
using System.Collections;

public class download_obb_controller : MonoBehaviour {

	// Use this for initialization

	public bool load_waiting=false;
	public bool is_sd_set=false;
	public float next_check_time;
	public float next_check_time_interval=1.0f;

	public string mainPath = null;
	public string expPath = null;

	public TextMesh textmesh;

	public int count=0;
	
	#if UNITY_ANDROID
	void Start () {

		check_sd();
	
	}

	void check_sd(){
		if (GooglePlayDownloader.RunningOnAndroid()){
			expPath = GooglePlayDownloader.GetExpansionFilePath();
			if (expPath == null){
				textmesh.text="SDカードを\nそうにゅうしてください。";
					
			}else{
				is_sd_set=true;
				mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
				//string patchPath = GooglePlayDownloader.GetPatchOBBPath(expPath);
				//if (mainPath == null || patchPath == null){
				if (mainPath == null ){
					GooglePlayDownloader.FetchOBB();
					load_waiting=true;
					
				}else{
					loadlevel();
				}
				
				
			}
		}
	}
	/*
	void OnGUI() {

		GUI.Label(new Rect(10, 10, Screen.width-10, 20), "Main = ..."  + ( mainPath == null ? "null" :  mainPath.Substring(expPath.Length)));

	}
	*/


	public void check_loaded(){

		expPath = GooglePlayDownloader.GetExpansionFilePath();
		if (expPath == null){
			textmesh.text="SDカードを\nそうにゅうしてください。";
			is_sd_set=false;
		}else{
			string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
			//string patchPath = GooglePlayDownloader.GetPatchOBBPath(expPath);
			if (mainPath == null ){
			//if (mainPath == null || patchPath == null){
				loading_text();
			}else{
				//Application.LoadLevel("title");
				loadlevel();
			}
		}
		
	}
	
	
	// Update is called once per frame
	void Update () {


		if(Time.time>next_check_time){
			next_check_time=Time.time+next_check_time_interval;

			if(!is_sd_set){
				check_sd();
			}else if(load_waiting){
				check_loaded ();
			}
		}

	
	}

	

	public void loadlevel(){
		Application.LoadLevel("title");
	}
	#endif
	/*
	void Update () {
			
			
		if(Time.time>next_check_time){
			next_check_time=Time.time+next_check_time_interval;

				loading_text ();
			
		}
	}
	*/

	public void loading_text(){
		string output = "よみこみちゅう";
			

			for (int i=0;i<count;i++){
				output+="…";
			}
			count++;
			if(count>2){
				count=0;
			}

		textmesh.text=output;
	}
}
