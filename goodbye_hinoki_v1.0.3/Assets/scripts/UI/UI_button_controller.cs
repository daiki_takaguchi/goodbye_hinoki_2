﻿using UnityEngine;
using System.Collections;

public class UI_button_controller : MonoBehaviour {

	public string type;

	public string item_type;
	public string item_sword_no;
	public int item_slot_no;

	public int stage_no;

	public int button_width;
	public int button_height;

	public bool is_layer_no_enablaed;
	public int layer_no;
	public string layer_name="";
	
	public bool detect_desplay_pressed = false;

	public bool display_pressed;

	public string display_pressed_to;

	private float layer_offset = -0.05f;

	private float wait_for_click=0;
	// Use this for initialization
	void Start () {

			
			/*
			transform.position=new Vector3(transform.position.x,transform.position.y,-layer_no * 0.02f+layer_offset);
				BoxCollider temp = transform.GetComponent<BoxCollider> ();
			//	temp.center = new Vector3 (temp.center.x, temp.center.y, -layer_no * 0.01f+layer_offset);
				temp.size = new Vector3 (temp.size.x, temp.size.y, 0.01f);
			*/

			if (is_layer_no_enablaed) {
				//set_layer_no (change_layer_name_to_layer_no(layer_name));
				set_layer_no (layer_no);
			}

			if(tag=="stage_select_button"){
				gameObject.layer=14;
			}


	}

	public int change_layer_name_to_layer_no(string input_layer_name){
		int output=0;
		return output;
	}

	public void set_layer_no(int input_layer_no){
		layer_no = input_layer_no;
		transform.position=new Vector3(transform.position.x,transform.position.y,-layer_no * 0.02f+layer_offset);

		if(GetComponent<BoxCollider>()!=null){
			BoxCollider temp = transform.GetComponent<BoxCollider> ();
			temp.size = new Vector3 (temp.size.x, temp.size.y, 0.01f);
		}

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)&&detect_desplay_pressed) {
			if(display_pressed_to=="OnMouseUpAsButton"){
				wait_for_click=Time.time+0.2f;
				//GameObject.Find ("UI").GetComponent<UI_controller> ().UI_OnMouseUpAsButton(this,transform);
			}
		}

		if(wait_for_click!=0){
			if(Time.time>wait_for_click){
				//print (wait_for_click);
				display_pressed=true;
				GameObject.Find ("UI").GetComponent<UI_controller> ().UI_OnMouseUpAsButton(this,transform);
				display_pressed=false;
				wait_for_click=0;


			}
		}
	}

	public void enable_detect_desplay_pressed(){
		detect_desplay_pressed = true;

	}

	public void disable_detect_desplay_pressed(){
		detect_desplay_pressed = false;
		wait_for_click = 0;
	}

	void OnMouseUpAsButton(){
		//print (stage_no);

		//print ("click");

		GameObject.Find ("UI").GetComponent<UI_controller> ().UI_OnMouseUpAsButton(this,transform);
		if(wait_for_click!=0){
			wait_for_click=0;
		}

	}

	void OnMouseDrag() {
		GameObject.Find ("UI").GetComponent<UI_controller> ().UI_OnMouseDrag (this,transform);
	}

	void OnMouseDown(){
		GameObject.Find ("UI").GetComponent<UI_controller> ().UI_OnMouseDown(this,transform);
	}

	void OnMouseUp(){
		GameObject.Find ("UI").GetComponent<UI_controller> ().UI_OnMouseUp (this,transform);
	}


}
