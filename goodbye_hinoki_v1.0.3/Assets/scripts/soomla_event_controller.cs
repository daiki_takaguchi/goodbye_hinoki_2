﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Soomla.Store {

		
	public class soomla_event_controller{
		
		public soomla_event_controller () {
			
			StoreEvents.OnItemPurchased += onItemPurchased;
			
		}
		
		public void onItemPurchased(PurchasableVirtualItem pvi, string payload) {
			
			
			GameObject.Find ("soomla_controller").GetComponent<soomla_controller> ().purchase_sword ();
		}
	}


}