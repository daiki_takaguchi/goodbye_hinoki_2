﻿using UnityEngine;
using System.Collections;

public class BGM_controller : MonoBehaviour {


	public AudioClip BGM;
	//public int playing_BGM = 0;
	public bool mute=false;
	//public bool is_BGM_set;

	
	public string state="stop";
	// Use this for initialization
	void Start () {
		if(PlayerPrefs.HasKey("is_BGM_mute")){
			mute=(PlayerPrefs.GetInt ("is_BGM_mute") == 1) ? true : false;
		}else{
			mute=false;
		}
	
	}
	
	// Update is called once per frame
	void Update () {
		/*
		if(state!="stop"){
			if (!audio.isPlaying) {
				//switch_BGM();
				//audio.clip = BGM [playing_BGM];
				//
				play_audio();
			}
		}else{
			stop_BGM();
		}
		*/
		//}
		/*
		if (mute) {
			stop_BGM();
			PlayerPrefs.SetInt ("is_BGM_mute",mute ? 1 : 0);
		
		}
		*/
	}

	public void set_play_BGM(string input_BGM){
		if (state == "stop") {
			set_BGM(input_BGM);
			play_audio();
		}else if (state=="play"){
			stop_BGM();
			set_BGM(input_BGM);
			play_audio();
		}
	}

	public void play_BGM(){
		if (state == "stop") {
			state = "play";
			play_audio();

		}
		//}
	}

	public void play_audio(){

		if(!mute){
			if(audio.clip!=null){
				state="play";
				audio.Play ();
			}
		}
	}

	public void stop_BGM(){
		state = "stop";
		audio.Stop();
		//if (bgm_state == "stop") {
			
			
		//}
	}

	/*
	public void switch_BGM(){
		playing_BGM++;


		if (playing_BGM == BGM.Length) {
			playing_BGM=0;
		}

	}
	*/

	public void switch_mute(){
		mute = !mute;

		if(mute){
			stop_BGM();
		}else{
			play_BGM();

		}



		PlayerPrefs.SetInt ("is_BGM_mute",mute ? 1 : 0);

	}

	public void set_BGM(string input_BGM){

		audio.clip =(AudioClip) Resources.Load ("audio/BGM/"+input_BGM,typeof(AudioClip));

		//audio.clip=
	}


}
