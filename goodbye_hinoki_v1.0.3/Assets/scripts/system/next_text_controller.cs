﻿using UnityEngine;
using System.Collections;

public class next_text_controller : MonoBehaviour {

	public string next_text_label;
	public float next_text_wait_time=0.0f;
	public bool is_shown=false;

	//public bool is_animation_enabled;
	//public bool is_all_text_shown=false;

	public bool detect_display_pressed;
	
	public window_controller window;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (window.is_animation_enabled) {

			if(detect_display_pressed){

				if(window.text_mesh.is_all_text_shown){
					bool next_text_flag = false;
					
					next_text_flag = Input.GetMouseButtonDown (0);
					
					if (next_text_flag&&is_shown&&Time.time>next_text_wait_time){
						//StartCoroutine(wait_for_seconds(next_text_wait_time));
						
						next_text(next_text_label);
						
					}else if(next_text_flag&&is_shown&&Time.timeScale==0){
						
						next_text(next_text_label);
					}

				}else{

					bool next_text_flag = false;

					next_text_flag = Input.GetMouseButtonDown (0);

					if (next_text_flag&&is_shown){
						window.text_mesh.show_all_text();
					}

				}
			}

		}else{

			if(detect_display_pressed){
				bool next_text_flag = false;
				
				next_text_flag = Input.GetMouseButtonDown (0);
				
				if (next_text_flag&&is_shown&&Time.time>next_text_wait_time){
					//StartCoroutine(wait_for_seconds(next_text_wait_time));

					next_text(next_text_label);
				
				}else if(next_text_flag&&is_shown&&Time.timeScale==0){

					next_text(next_text_label);
				}
			}
		}

	}

	public IEnumerator wait_for_seconds(float wait_time){

		yield return new WaitForSeconds(wait_time);
		next_text(next_text_label);
	}

	public void set_next_text_label(string input_text){
		next_text_wait_time = Time.time + 1.0f;
		next_text_label = input_text;
	}

	public void next_text(string input_text){
		next_text_wait_time = Time.time + 0.5f;
		GameObject temp_game = new GameObject ();
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type=input_text;
		GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);

	}
	
}
