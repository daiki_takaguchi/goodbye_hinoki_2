﻿using UnityEngine;
using System.Collections;

public class window_controller : MonoBehaviour {

	public Transform[] window ;
	public Transform window_UI;

	public Transform text_transform;
	public Transform next_text_button;

	public text_mesh_controller text_mesh;

	public bool is_shown = false;
	public bool is_animation_enabled=false;

	public int width;

	public int height;




	// Use this for initialization
	void Start () {
		//show_window (2,2,-8,-8,GameObject.Find ("UI").transform);
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void change_layer(string layer_name){
		Transform[] temps= transform.GetComponentsInChildren<Transform> ();
		foreach(Transform temp in temps){
			if(temp.tag=="window"){
				temp.GetComponent<SpriteRenderer>().sortingLayerName="layer_name";
			}
		}
	}



	public void show_window( int input_width,int input_height, Vector3 input_position ,string layer,float delay){
		width = input_width;
		height = input_height;
		transform.position = input_position;

		StartCoroutine (make_window_wait (input_width, input_height, input_position,layer,delay));
	}

	public IEnumerator make_window_wait(int input_width,int input_height,Vector3 input_position ,string layer,float delay){
		
		yield return new WaitForSeconds(delay);
		make_window (input_width, input_height, input_position,layer);

	}

	public void make_window( int input_width,int input_height,Vector3 input_position,string layer){

		//width = input_width;
		//height = input_height;
		//transform.position = new Vector3 (x,y,0);


		//Transform parent_transform = (Transform) Instantiate(window_UI, new Vector3(0,0,0), Quaternion.identity);
		//parent_transform.parent=UI_transform;

		for (int i=0; i<width; i++) {
			for (int j=0; j<height; j++) {

				Transform temp;

				Vector3 position= new Vector3(i*16 -(width*16/2)+8,j*16-(height*16/2)+8, 0)+input_position;

				if(i==0&&j==0){
					temp= (Transform) Instantiate(window[6],position , Quaternion.identity);
				
				}else if(i==0&&j==height-1){
					temp= (Transform) Instantiate(window[0],position , Quaternion.identity);

				}else if(i==width-1&&j==0){
					temp= (Transform) Instantiate(window[8],position , Quaternion.identity);

				}else if(i==width-1&&j==height-1){
					temp= (Transform) Instantiate(window[2],position , Quaternion.identity);
				
				}else if(i==0){
					temp= (Transform) Instantiate(window[3],position , Quaternion.identity);
					
				}else if(i==width-1){
					temp= (Transform) Instantiate(window[5],position , Quaternion.identity);
					
				}else if(j==0){
					temp= (Transform) Instantiate(window[7],position , Quaternion.identity);
					
				}else if(j==height-1){
					temp= (Transform) Instantiate(window[1],position , Quaternion.identity);

				
				}else{
					temp= (Transform) Instantiate(window[4],position, Quaternion.identity);
				}
				temp.GetComponent<SpriteRenderer>().sortingLayerName=layer;
				temp.gameObject.layer=gameObject.layer;
				temp.parent=transform;
				temp.name="one_window";
				temp.tag="window";

			}
		}

		is_shown = true;

		if(next_text_button!=null){
			next_text_button.GetComponent<next_text_controller> ().is_shown = true;
			next_text_button.renderer.enabled = true;
		}
		if(text_transform!=null){
			if(text_mesh!=null){
				//if(!text_transform.renderer.enabled){
				//if(text_mesh.is){
				text_mesh.enable_animation();
				//}
				//}
			}
			text_transform.renderer.enabled = true;
		}

	}

	public void set_text_auto_position(string text_label,string input_layer_name ,bool input_animation_enabled){

		is_animation_enabled = input_animation_enabled;

		Vector3 position = new Vector3 (transform.position.x-(width*16/2) +4,transform.position.y+(height*16/2),0);
		
		text_transform=(Transform) Instantiate(GameObject.Find ("controller").transform.FindChild("text_mesh"), position,Quaternion.identity );

		text_mesh = text_transform.GetComponent<text_mesh_controller> ();

		text_transform.GetComponent<text_mesh_controller> ().set_layer (input_layer_name);
		text_transform.parent = transform;

			if(is_animation_enabled){

				string out_1;
				string out_2;
				GameObject.Find ("controller").GetComponent<text_data_controller> ().get_data_animation(text_label,out out_1,out out_2);
				text_transform.GetComponent<text_mesh_controller>().set_text_animation(out_1,out_2);

			}else{
				text_transform.GetComponent<text_mesh_controller>().set_text(GameObject.Find ("controller").GetComponent<text_data_controller> ().get_data (text_label));
				//print(GameObject.Find ("controller").GetComponent<text_data_controller> ().get_data (text_label));
			}

		text_transform.name="text_mesh";
		text_transform.renderer.enabled = is_shown;
	
	}

	public void set_hinoki_text_auto_position(int map_no,int stage_cleared,string input_layer_name ){
		Vector3 position = new Vector3 (transform.position.x-(width*16/2) +4,transform.position.y+(height*16/2),0);
		
		text_transform=(Transform) Instantiate(GameObject.Find ("controller").transform.FindChild("text_mesh"), position,Quaternion.identity );
		text_transform.gameObject.layer = gameObject.layer;
		text_transform.GetComponent<text_mesh_controller> ().set_layer (input_layer_name);
		text_transform.parent = transform;
		text_transform.GetComponent<text_mesh_controller>().set_text(GameObject.Find ("controller").GetComponent<text_data_controller> ().get_hinoki_comment(map_no,stage_cleared));
		text_transform.name="text_mesh";
		text_transform.renderer.enabled = is_shown;
		
	}

	public void set_shop_secret_text_auto_position(string input_layer_name ){
		Vector3 position = new Vector3 (transform.position.x-(width*16/2) +4,transform.position.y+(height*16/2),0);
		
		text_transform=(Transform) Instantiate(GameObject.Find ("controller").transform.FindChild("text_mesh"), position,Quaternion.identity );
		text_transform.GetComponent<text_mesh_controller> ().set_layer (input_layer_name);
		text_transform.parent = transform;
		text_transform.GetComponent<text_mesh_controller>().set_text(GameObject.Find ("controller").GetComponent<text_data_controller> ().get_data("shop_secret"));
		text_transform.name="text_mesh";
		text_transform.renderer.enabled = is_shown;
		
	}

	public void change_hinoki_text_auto_position(int map_no,int stage_cleared){
		text_transform.GetComponent<text_mesh_controller>().set_text(GameObject.Find ("controller").GetComponent<text_data_controller> ().get_hinoki_comment(map_no,stage_cleared));
		text_transform.renderer.enabled = is_shown;

	}

	public void change_text_auto_position(string text_label){
		if (is_animation_enabled) {

			string out_1;
			string out_2;
			GameObject.Find ("controller").GetComponent<text_data_controller> ().get_data_animation(text_label,out out_1,out out_2);
			text_transform.GetComponent<text_mesh_controller>().set_text_animation(out_1,out_2);
			text_transform.GetComponent<text_mesh_controller>().enable_animation();
			text_transform.renderer.enabled = is_shown;

		}else{
			text_transform.GetComponent<text_mesh_controller>().set_text(GameObject.Find ("controller").GetComponent<text_data_controller> ().get_data (text_label));
			text_transform.renderer.enabled = is_shown;
		}
		
	}



	public void set_text(string text,float text_x,float text_y){
		/*
		Vector3 position = new Vector3 (transform.position.x-(width*16/2)+8,transform.position.y+(height*16/2)-8,0);
*/		
		Vector3 position = new Vector3 (transform.position.x-(width*16/2) +4,transform.position.y+(height*16/2),0);

		text_transform=(Transform) Instantiate(GameObject.Find ("controller").transform.FindChild("text_mesh"), position,Quaternion.identity );
		text_transform.parent = transform;
		text_transform.GetComponent<text_mesh_controller>().set_text(GameObject.Find ("UI").GetComponent<text_data_controller> ().get_data (text));

		text_mesh = text_transform.GetComponent<text_mesh_controller> ();

		text_transform.renderer.enabled = is_shown;
	}

	public void add_next_text_button(string next_text_label ,Vector3 local_position){

		next_text_button=(Transform) Instantiate(Resources.Load ("prefab/UI/text/next_text",  typeof(Transform)),transform.position,Quaternion.identity );
		//next_text_button.gameObject.layer = gameObject.layer;
		next_text_button.name="next_text";
		next_text_button.parent = transform;
		next_text_button.GetComponent<next_text_controller> ().window = this;
		next_text_button.localPosition = local_position;
		next_text_button.GetComponent<next_text_controller> ().set_next_text_label(next_text_label);
		next_text_button.renderer.enabled = is_shown;

	}

	public void add_hinoki_next_text_button(Vector3 local_position){

		next_text_button=(Transform) Instantiate(Resources.Load ("prefab/UI/text/next_text_hinoki",  typeof(Transform)),transform.position,Quaternion.identity );
		next_text_button.gameObject.layer = gameObject.layer;
		next_text_button.name="next_text";
		next_text_button.parent = transform;
		next_text_button.GetComponent<next_text_controller> ().window = this;
		next_text_button.localPosition = local_position;
		//next_text_button.GetComponent<next_text_controller> ().set_next_text_label(next_text_label);
		next_text_button.renderer.enabled = is_shown;
	}

	public void change_next_text_button(string next_text_label){


		next_text_button.GetComponent<next_text_controller> ().set_next_text_label(next_text_label);
		next_text_button.renderer.enabled = is_shown;

		
	}

	public void destroy_next_text_button(){
		Destroy (next_text_button.GetComponent<next_text_controller>());
	}
}
