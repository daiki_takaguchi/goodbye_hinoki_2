﻿using UnityEngine;
using System.Collections;

public class enemy_magic_controller : MonoBehaviour {

	public string type;
	public string state;

	public float next_attack_time;

	public Vector3 rotation_pivot;
	public Vector3 next_fire_position;

	public int attack_point;
	public float remaining_life_time;

	public bool is_remaining_life_time_enabled=true;

	public bool is_screen_limit_set;
	public float charactor_width;
	public float charactor_height;

	private float limit_x_left=-80;
	private float limit_x_right=80;
	private float limit_y_top=48;
	private float limit_y_bottom=-64;

	public Sprite[] sprite;

	// Use this for initialization
	void Start () {
			
		if(type == "boss_008_beam"||
		   type =="boss_009_beam"||
		   type =="boss_009_ball"||
		   type =="enemy_003_tongue"||
		   type =="enemy_007_rock"||
		   type =="enemy_008_fire"
		   ){

			gameObject.layer = 13;

		}



		if (type == "enemy_004_bullet") {
			rigidbody.velocity=new Vector3(-60,0,0);
		}else if(type=="boss_001_sword"){
			state="follow";
			next_attack_time=Time.time+1.0f;
		}else if (type=="boss_002_fire"){
			next_attack_time=Time.time+ 0.25f;
		}else if (type=="boss_003_leaf"){
			//rigidbody.velocity=(GameObject.Find ("hero").transform.position-transform.position).normalized*16.0f;

		}else if (type=="boss_008_beam"){
			//renderer.material.color=new Color(1.0f,1.0f,1.0f,0.5f);
			next_attack_time=Time.time+remaining_life_time-0.5f;
		}else if (type=="enemy_011_spore"){
			set_screen_limit(0,80,48,-64,16,16);

		}else if (type=="enemy_014_sumi"){
			rigidbody.velocity=new Vector3(-60,0,0);
		}else if (type=="enemy_021_bomb"){
			//rigidbody.velocity=new Vector3(-60,0,0);
			GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("bomb");
		}


		if(is_remaining_life_time_enabled){
			//remaining_life_time_private=remaining_life_time;
			remaining_life_time += Time.time;
		}

	}


	
	// Update is called once per frame
	void Update () {
		check_life_time ();
		update_magic ();
	}


	public void enable_life_time(float input_life_time){


		is_remaining_life_time_enabled = true;
		if(input_life_time>=0){
			remaining_life_time = Time.time+input_life_time;
		}else{
			remaining_life_time+=Time.time;
		}
	}

	public void check_life_time(){
		if (Time.time > remaining_life_time&&is_remaining_life_time_enabled) {
			if(type=="enemy_021_seed"){
				Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_021/enemy_021_bomb",  typeof(Transform)) ,transform.position,Quaternion.identity );

			}

			if(type=="boss_008_beam"){
				Destroy(transform.parent.gameObject);
			}else if(type=="enemy_011_spore"){
				enemy_011_spore_destroy();
			}else{
				Destroy(gameObject);
			}
		}
	}


	public void enemy_011_spore_destroy(){

		Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_011/enemy_011",  typeof(Transform)) ,
			            transform.position,Quaternion.identity );


		Destroy (gameObject);
	}


	public void set_screen_limit(float input_limit_x_left,float input_limit_x_right, float input_limit_y_top, float input_limit_y_bottom,float input_charactor_width,float input_charactor_height){
		
		is_screen_limit_set = true;
		
		limit_x_left = input_limit_x_left;
		limit_x_right = input_limit_x_right;
		limit_y_top= input_limit_y_top;
		limit_y_bottom=input_limit_y_bottom;
		charactor_width = input_charactor_width;
		charactor_height = input_charactor_height;
	}

	public void move_limit(){
		
		//GameObject target = GameObject.Find("hero");
		//print ();
		if(is_screen_limit_set){
			Vector3 velocity = rigidbody.velocity;
			Vector3 position = transform.position;
			
			if ((transform.position.x<=limit_x_left + (charactor_width/2))&&(velocity.x<0)) {
				
				velocity.x=0;
				position.x=limit_x_left+(charactor_width/2);
				
			}else if((transform.position.x>=limit_x_right-(charactor_width/2))&&(velocity.x>0)){
				velocity.x=0;
				position.x=limit_x_right-(charactor_width/2);
			}
			
			if ((transform.position.y<=limit_y_bottom+(charactor_height/2))&&(velocity.y<0)) {
				velocity.y=0;
				position.y=limit_y_bottom+(charactor_height/2);

			}else if((transform.position.y>=limit_y_top-(charactor_height/2))&&(velocity.y>0)){
				velocity.y=0;
				position.y=limit_y_top-(charactor_height/2);
			}
			
			transform.position = position;
			rigidbody.velocity = velocity;
		}
		
	}
	public void update_magic(){
		if (type == "boss_001_sword") {
			if(state=="follow"){
	
				transform.position=new Vector3(transform.position.x, GameObject.Find ("hero").transform.position.y+16,transform.position.z);
				if(Time.time>next_attack_time){

					state="growing";
					next_attack_time=Time.time+1.0f;
					GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("sword_grow");
				}
			
			}else if (state=="growing"){
				//float temp=1.0f*Time.deltaTime;

				Vector3 temp= new Vector3(1,1,1)*1.0f*Time.deltaTime;

				//transform.localScale=new Vector3(transform.localScale.x+temp,transform.localScale.y+temp,transform.localScale.z+temp);


				transform.localScale+=temp;

				if(Time.time>next_attack_time){
					state="rotate";
					next_attack_time=Time.time+ 1.0f;
					rotation_pivot=transform.position+new Vector3(0,-16,0);
				}
			}else if (state=="rotate"){

				//tra

				//Vector3 temp =transform.position
				transform.Rotate(new Vector3(0,0,90.0f*Time.deltaTime));
				transform.position=RotatePointAroundPivot(transform.position,rotation_pivot, Quaternion.Euler(new Vector3(0,0,90.0f*Time.deltaTime)));

				if(Time.time>next_attack_time){
					state="bullet";
					transform.rotation=Quaternion.Euler(new Vector3(0,0,90));
				}
			}else if(state=="bullet"){
				rigidbody.velocity=new Vector3(-100,0,0);
			}
			
		}else if (type == "boss_002_fire") {
			char[] separators = ",".ToCharArray();
			string[] temp=state.Split(separators);
			int temp_2=int.Parse(temp[1]);
			int temp_3=int.Parse(temp[2]);

			if(temp_3>10){
				Destroy (gameObject);
			}
			if(Time.time>next_attack_time){

				state=temp[0]+","+(temp_2+1)+","+temp_3;
				if(temp_2==1){
					next_attack_time=Time.time+1.0f;
					GetComponent<SpriteRenderer>().sprite =sprite[0];

					/*
					Vector3 temp_next_position= new Vector3( 0,0,0);
					if(temp[0]=="up_left"){
						temp_next_position=new Vector3(-16,8,0);	
					}else if(temp[0]=="left"){
						temp_next_position=new Vector3(-16,0,0);	
					}else if(temp[0]=="down_left"){
						temp_next_position=new Vector3(-16,-8,0);	
					}
					*/
					Vector3 temp_next_position=transform.position+next_fire_position;

					Transform fire=(Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_002/boss_002_fire",typeof(Transform)) ,temp_next_position,Quaternion.identity );
					fire.GetComponent<enemy_magic_controller>().state=temp[0]+",1,"+(temp_3+1);
					fire.GetComponent<enemy_magic_controller>().next_fire_position=next_fire_position;
				}else if(temp_2==2){
					next_attack_time=Time.time+ 0.25f;
					GetComponent<SpriteRenderer>().sprite =sprite[1];
				}else if(temp_2==3){
					Destroy (gameObject);
					//GetComponent<SpriteRenderer>().sprite =sprite[1];
				}
			}
		}else if (type == "enemy_011_spore") {
			move_limit();
		}else if (type=="boss_008_beam"){
			if(Time.time<next_attack_time){
				//renderer.material.color=new Color(1.0f,1.0f,1.0f,1.0f-(next_attack_time-Time.time)/remaining_life_time_private);
				//print ((next_attack_time-Time.time)/remaining_life_time_private);
			}else{
				renderer.material.color=new Color(1.0f,1.0f,1.0f,0.5f+(remaining_life_time-Time.time));
			}

		}
	}



	public void move_end(){
		if(type=="boss_009_ball"){
			Transform temp= (Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_009/boss_009_beam_1",typeof(Transform)) ,transform.position,Quaternion.identity );
			temp.FindChild("beam").GetComponent<enemy_magic_controller>().attack_point=attack_point;
			Destroy (gameObject);
		}else if(type=="boss_011_ball"){
			Transform temp= (Transform)  Instantiate(( Transform) Resources.Load ("prefab/enemy/boss_011/boss_011_beam_1",typeof(Transform)) ,transform.position,Quaternion.identity );
			temp.FindChild("beam").GetComponent<enemy_magic_controller>().attack_point=attack_point;
			Destroy (gameObject);
		}
	}

	public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion angle) {
		return angle * ( point - pivot) + pivot;
	}

	void OnTriggerEnter (Collider c){

		if (c.tag == "magic") {
			if(c.GetComponent<magic_controller>().type=="fire"){
				if(type!="boss_001_sword"&&type!="boss_002_fire"&&type!="boss_009_ball"&&type!="boss_008_beam"&&type!="enemy_007_rock"){

					Destroy (gameObject);
				}
			}

		}
	}

	public void destroy_this(){
		Destroy (gameObject);
	}


	
}
