﻿using UnityEngine;
using System.Collections;

public class SE_controller : MonoBehaviour {

	public AudioClip select;
	public AudioClip start;
	public AudioClip count;
	public AudioClip fight;
	public AudioClip enemy_hit;
	public AudioClip hero_hit;
	public AudioClip coin;
	public AudioClip game_clear;
	public AudioClip revive;
	public AudioClip using_sword;
	public AudioClip fire;
	public AudioClip wind;
	public AudioClip heal;
	public AudioClip shop;
	public AudioClip sword_grow;
	public AudioClip error;
	public AudioClip buy;
	public AudioClip power_up;
	public AudioClip clear;
	public AudioClip aura;
	public AudioClip one_up;
	public AudioClip bomb;
	public AudioClip boss_005;
	public AudioClip barrier;
	public AudioClip text;

	public string bgm_state="stop";

	public bool mute=false;



	// Use this for initialization
	void Start () {
		if(PlayerPrefs.HasKey("SE_mute")){
			mute=(PlayerPrefs.GetInt ("SE_mute") == 1) ? true : false;
		}else{
			mute=false;
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void play_audio(string selected_audio_clip){

		if(!mute){

			AudioClip temp = null;
			if (selected_audio_clip == "select") {
				temp = select;
			}else if (selected_audio_clip == "start") {
				temp=start;
			}else if (selected_audio_clip == "count") {
				temp = count;
			}else if (selected_audio_clip == "fight") {
				temp = fight;
			}else if (selected_audio_clip == "enemy_hit") {
				temp = enemy_hit;
			}else if (selected_audio_clip == "hero_hit") {
				temp = hero_hit;
			}else if (selected_audio_clip == "coin") {
				temp = coin;
			}else if (selected_audio_clip == "game_clear") {
				temp =  game_clear;
			}else if (selected_audio_clip == "revive") {
				temp =  revive;
			}else if (selected_audio_clip == "using_sword") {
				temp =  using_sword;
			}else if (selected_audio_clip == "fire") {
				temp =  fire;
			}else if (selected_audio_clip == "wind") {
				temp =  wind;
			}else if (selected_audio_clip == "heal") {
				temp =  heal;
			}else if (selected_audio_clip == "shop") {
				temp =  shop;
			}else if (selected_audio_clip == "sword_grow") {
				temp =  sword_grow;
			}else if (selected_audio_clip == "error") {
				temp =  error;
			}else if (selected_audio_clip == "buy") {
				temp =  buy;
			}else if (selected_audio_clip == "power_up") {
				temp =  power_up;
			}else if (selected_audio_clip == "clear") {
				temp =  clear;
			}else if (selected_audio_clip == "aura") {
				temp =  aura;
			}else if (selected_audio_clip == "one_up") {
				temp =  one_up;
			}else if (selected_audio_clip == "bomb") {
				temp =  bomb;
			}else if (selected_audio_clip == "boss_005") {
				temp =  boss_005;
			}else if (selected_audio_clip == "barrier") {
				temp =  barrier;
			}else if (selected_audio_clip == "text") {
				temp =  text;
			}
		audio.PlayOneShot (temp);

		}
		
	}

	public void switch_mute(){
		
		mute = !mute;
		PlayerPrefs.SetInt ("SE_mute",mute ? 1 : 0);
		
	}




}
