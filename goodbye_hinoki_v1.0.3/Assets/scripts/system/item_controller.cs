﻿using UnityEngine;
using System.Collections;

public class item_controller : MonoBehaviour {

	//public Transform item_UI;

	public string type;
	public string sword_no;
	public int money;
	public bool got_item=false;
	public float created_time;
	public bool is_remaining=false;

	void Start () {
		created_time = Time.time;
	}
	
	// Update is called once per frame
	void Update () {

		if(!is_remaining){

			if (Time.time > created_time + 3.0f) {
				got_item=true;
				Destroy(this.gameObject);

			}else if (Time.time > created_time + 2.0f) {

				renderer.material.color=new Color(1.0f,1.0f,1.0f,1.0f-(Time.time-created_time-2.0f));
			}
		}
	}


	public void dead(){

	}
	
	
	public void OnDestroy(){
		if(got_item){
			if(GameObject.Find ("controller").GetComponent<battle_controller> ()!=null){
				GameObject.Find ("controller").GetComponent<battle_controller> ().got_item();
			}
		}
		
	}

}
