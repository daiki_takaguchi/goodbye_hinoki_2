﻿using UnityEngine;
using System.Collections;

public class battle_controller : MonoBehaviour {

	//int remaining_enemy;
	public int stage_no;
	public bool is_unchi_stage=false;
	public bool is_stage_test = false;
	public bool is_paused;
	public bool is_stage_end =false;
	public string battle_end_type="normal";
	public int remainging_enemy_count;
	public Sprite[] bacground_inages;

	public long total_enemy_exp=0;

	public save_data_controller save_data;

	private int boss_kill_count=0;

	// Use this for initialization
	void Start () {

		if(!is_stage_test){
			stage_no=PlayerPrefs.GetInt("stage_no");
		
			/*
			if(save_data.get_stage_cleared()==-1){
				stage_no=0;
				save_data.set_stage_cleared(0);
			}*/

			if(stage_no==5&&save_data.get_stage_cleared()<5){
				stage_no=-1;
			}
			/*
			if(stage_no==8&&save_data.get_stage_cleared()==7&&!save_data.is_met_boss_002()){
				//PlayerPrefs.SetInt ("met_boss_002",0);

				//PlayerPrefs.SetInt("stage_no",8);
				//stage_no=-2;
			}*/

			//PlayerPrefs.DeleteKey ("met_last_boss_at_stage_17");

			if(stage_no==17&&!PlayerPrefs.HasKey ("met_last_boss_at_stage_17")){
				PlayerPrefs.SetInt ("met_last_boss_at_stage_17",0);
				stage_no=-3;
			}

			if(stage_no==11&&save_data.get_stage_cleared()<11){
				stage_no=-5;

			}

			if (save_data.get_stage_cleared()<4||stage_no>0&&stage_no<5&&save_data.get_stage_cleared()<5) {
				Destroy (GameObject.Find ("button_left"));
			}

			if(stage_no==-4&&save_data.is_boss_009_cleared()){
				stage_no=-6;
			}

			if(stage_no==-4||stage_no==-6||stage_no==-7||stage_no==-8||stage_no==-9){
				PlayerPrefs.SetInt("stage_no",save_data.get_stage_cleared());

			}

			if(!save_data.HasKey("met_hinoki")){
				stage_no=0;
			}
			/*
			if(){
				stage_no=-1;
			}
			*/


		}else{

		}


		/*
		GameObject.Find ("controller").GetComponent<save_data_controller> ().initialize ();
		GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword ("sword_003_01",0);
		GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword ("sword_004_01",0);
		GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword ("sword_005_01",0);
		GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword ("sword_006_01",0);
		GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword ("sword_007_01",0);
		GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword ("sword_008_01",0);
		GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword ("sword_010_01",0);
		*/

		//GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword ("sword_010_01",0);

		GameObject.Find ("controller").GetComponent<save_data_controller> ().refresh_public_variables ();


		if (stage_no != 0) {
			GameObject.Find ("hero").GetComponent<hero_controller> ().initialize_hero ();
		}

		set_background ();


		Transform temp;

		if (stage_no == 1) {

						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_001/enemy_001",  typeof(Transform)) ,new Vector3(50,30,0),Quaternion.identity );
		
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_001/enemy_001",  typeof(Transform)) ,new Vector3(40,0,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_001/enemy_001",  typeof(Transform)) ,new Vector3(50,-30,0),Quaternion.identity );
			*/

						instantiate_enemy ("enemy_001", new Vector3 (50, 30, 0));
						instantiate_enemy ("enemy_001", new Vector3 (40, 0, 0));
						instantiate_enemy ("enemy_001", new Vector3 (50, -30, 0));

						//surainu

				} else if (stage_no == 2) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_001/enemy_001",  typeof(Transform)) ,new Vector3(50,30,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_002/enemy_002",  typeof(Transform)) ,new Vector3(40,0,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_002/enemy_002",  typeof(Transform)) ,new Vector3(50,-30,0),Quaternion.identity );
*/
						instantiate_enemy ("enemy_001", new Vector3 (50, 30, 0));
						instantiate_enemy ("enemy_002", new Vector3 (40, 0, 0));
						instantiate_enemy ("enemy_002", new Vector3 (50, -30, 0));





						//surainu+usagi

				} else if (stage_no == 3) {
			
						//Transform temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_002/enemy_002",  typeof(Transform)) ,new Vector3(50,30,0),Quaternion.identity );
			
						//temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_002/enemy_002",  typeof(Transform)) ,new Vector3(40,0,0),Quaternion.identity );
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_004/enemy_004",  typeof(Transform)) ,new Vector3(56,-(128-32)/2,0),Quaternion.identity );
			*/

						temp = instantiate_enemy ("enemy_004", new  Vector3 (56, 32, 0));
						temp.GetComponent<enemy_controller> ().next_attack_state = "move_down";

						//PlayerPrefs.DeleteKey("map_001_talk");

						if (!save_data.is_map_001_talk_end ()) {
								event_start ("map_001_talk_1", 1.0f);
								battle_end_type = "map_001_talk_1";
						}

						//bara

				} else if (stage_no == 4) {

						instantiate_enemy ("boss_001", new Vector3 (56, 0, 0));
						//boss
			
				} else if (stage_no == 5) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_006/enemy_006",  typeof(Transform)) ,new Vector3(32,32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_006/enemy_006",  typeof(Transform)) ,new Vector3(32,0,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_006/enemy_006",  typeof(Transform)) ,new Vector3(32,-32,0),Quaternion.identity );
			*/

						instantiate_enemy ("enemy_006", new Vector3 (32, 32, 0));
						instantiate_enemy ("enemy_006", new Vector3 (32, 0, 0));
						instantiate_enemy ("enemy_006", new Vector3 (32, -32, 0));

						//kuro
				} else if (stage_no == 6) {
						/*
			temp = (Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_004/enemy_004",  typeof(Transform)) ,new Vector3(32,-(128-32)/2,0),Quaternion.identity );
			temp.GetComponent<enemy_controller>().next_attack_state="move_up";

			temp = (Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_004/enemy_004",  typeof(Transform)) ,new Vector3(8,(128-32)/2,0),Quaternion.identity );
			temp.GetComponent<enemy_controller>().next_attack_state="move_down";

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_005/enemy_005",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );
			*/

						temp = instantiate_enemy ("enemy_004", new  Vector3 (32, -48, 0));
						temp.GetComponent<enemy_controller> ().next_attack_state = "move_up";

						temp = instantiate_enemy ("enemy_004", new  Vector3 (8, 32, 0));
						temp.GetComponent<enemy_controller> ().next_attack_state = "move_down";

						temp = instantiate_enemy ("enemy_005", new  Vector3 (56, 0, 0));

						// bara+hituji
				} else if (stage_no == 7) {

						/*
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_005/enemy_005",  typeof(Transform)) ,new Vector3(16,0,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_017/enemy_017",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );
			
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_017/enemy_017",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_017/enemy_017",  typeof(Transform)) ,new Vector3(32,48,0),Quaternion.identity );
			
			*/


						instantiate_enemy ("enemy_005", new  Vector3 (16, 0, 0));

						instantiate_enemy ("enemy_017", new  Vector3 (56, 0, 0));
						instantiate_enemy ("enemy_017", new  Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_017", new  Vector3 (32, 48, 0));


				} else if (stage_no == 8) {
			
						instantiate_enemy ("boss_002", new Vector3 (56, 0, 0));//boss


						//PlayerPrefs.DeleteKey ("met_boss_002");
						if (!save_data.is_met_boss_002 ()) {
								event_start ("tutorial_sword_1", 1.0f);
								battle_end_type = "tutorial_sword_1";
						}
			
		
				} else if (stage_no == 9) {

						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_003/enemy_003",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_003/enemy_003",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_003/enemy_003",  typeof(Transform)) ,new Vector3(32,48,0),Quaternion.identity );

			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_009/enemy_009",  typeof(Transform)) ,new Vector3(0,0,0),Quaternion.identity );

*/

			
						instantiate_enemy ("enemy_003", new Vector3 (56, 0, 0));
						instantiate_enemy ("enemy_003", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_003", new Vector3 (32, 48, 0));

						instantiate_enemy ("enemy_009", new  Vector3 (0, 0, 0));

						// kaeru+hamu

				} else if (stage_no == 10) {
						//kinoko(HP ooi)+hamu
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_011/enemy_011",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_011/enemy_011",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_011/enemy_011",  typeof(Transform)) ,new Vector3(32,48,0),Quaternion.identity );
			
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_009/enemy_009",  typeof(Transform)) ,new Vector3(0,0,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_009/enemy_009",  typeof(Transform)) ,new Vector3(0,0,0),Quaternion.identity );

			*/

						instantiate_enemy ("enemy_011", new Vector3 (56, 0, 0));
						instantiate_enemy ("enemy_011", new Vector3 (8, 16, 0));
						instantiate_enemy ("enemy_011", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_011", new Vector3 (32, 32, 0));

						instantiate_enemy ("enemy_009", new  Vector3 (0, -16, 0));
						instantiate_enemy ("enemy_009", new  Vector3 (0, 16, 0));


				} else if (stage_no == 11) {

						//namekuji


						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_020/enemy_020",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_020/enemy_020",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_020/enemy_020",  typeof(Transform)) ,new Vector3(32,48,0),Quaternion.identity );
			*/

						instantiate_enemy ("enemy_020", new Vector3 (56, 0, 0));
						instantiate_enemy ("enemy_020", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_020", new Vector3 (32, 48, 0));


				} else if (stage_no == 12) {
						instantiate_enemy ("boss_003", new Vector3 (56, 0, 0));//boss

				} else if (stage_no == 13) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_014/enemy_014",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_014/enemy_014",  typeof(Transform)) ,new Vector3(32,48,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_021/enemy_021",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );
			*/

						instantiate_enemy ("enemy_014", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_014", new Vector3 (32, 48, 0));
						instantiate_enemy ("enemy_021", new Vector3 (56, 0, 0));



						//yasi
						//tako
				} else if (stage_no == 14) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_015/enemy_015",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_015/enemy_015",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_015/enemy_015",  typeof(Transform)) ,new Vector3(32,48,0),Quaternion.identity );

			//temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_014/enemy_014_2",  typeof(Transform)) ,new Vector3( 0,0,0),Quaternion.identity );


			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_014/enemy_014_2",  typeof(Transform)) ,new Vector3( 24,32,0),Quaternion.identity );
			*/

						instantiate_enemy ("enemy_015", new Vector3 (56, 0, 0));
						instantiate_enemy ("enemy_015", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_015", new Vector3 (32, 48, 0));

						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/enemy_014/enemy_014_2", typeof(Transform)), new Vector3 (24, 32, 0), Quaternion.identity);

			
						//same
				} else if (stage_no == 15) {

						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_013/enemy_013",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			temp.rigidbody.velocity= new Vector3(16.0f,0,0);

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_021/enemy_021",  typeof(Transform)) ,new Vector3(32,48,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_021/enemy_021",  typeof(Transform)) ,new Vector3(56,16,0),Quaternion.identity );
			*/

						temp = instantiate_enemy ("enemy_013", new Vector3 (16, 0, 0));
						temp.GetComponent<SpriteRenderer> ().sortingOrder = 1;

						temp.rigidbody.velocity = new Vector3 (16.0f, 0, 0);

						temp = instantiate_enemy ("enemy_021", new Vector3 (32, -16, 0));
						temp.GetComponent<SpriteRenderer> ().sortingOrder = 2;

						instantiate_enemy ("enemy_021", new Vector3 (56, 24, 0));


						if (!save_data.is_map_005_talk_end ()) {
								event_start ("map_005_talk_1", 1.0f);
								battle_end_type = "map_005_talk_1";
						}

						//kani
						//yasi
				} else if (stage_no == 16) {
						//same+boss
						instantiate_enemy ("boss_004", new Vector3 (48, 0, 0));//boss



				} else if (stage_no == 17) {
						//pengin azarashi;
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_016/enemy_016",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_016/enemy_016",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_016/enemy_016",  typeof(Transform)) ,new Vector3(32,48,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_022/enemy_022",  typeof(Transform)) ,new Vector3(24,16,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_022/enemy_022",  typeof(Transform)) ,new Vector3(40,-24,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_022/enemy_022",  typeof(Transform)) ,new Vector3(56,32,0),Quaternion.identity );
			*/



						instantiate_enemy ("enemy_016", new Vector3 (56, 0, 0));
						instantiate_enemy ("enemy_016", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_016", new Vector3 (32, 48, 0));
						instantiate_enemy ("enemy_022", new Vector3 (24, 16, 0));
						instantiate_enemy ("enemy_022", new Vector3 (40, -24, 0));
						instantiate_enemy ("enemy_022", new Vector3 (56, 32, 0));
				} else if (stage_no == 18) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_023/enemy_023",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_023/enemy_023",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_023/enemy_023",  typeof(Transform)) ,new Vector3(32,32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_022/enemy_022",  typeof(Transform)) ,new Vector3(24,16,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_022/enemy_022",  typeof(Transform)) ,new Vector3(40,-24,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_022/enemy_022",  typeof(Transform)) ,new Vector3(56,24,0),Quaternion.identity );
			*/

						instantiate_enemy ("enemy_023", new Vector3 (56, 0, 0));
						instantiate_enemy ("enemy_023", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_023", new Vector3 (32, 32, 0));
						instantiate_enemy ("enemy_022", new Vector3 (24, 16, 0));
						instantiate_enemy ("enemy_022", new Vector3 (40, -24, 0));
						instantiate_enemy ("enemy_022", new Vector3 (56, 24, 0));
						//ookami azarashi
				} else if (stage_no == 19) {

						//temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_019/enemy_019",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );


						instantiate_enemy ("enemy_019", new Vector3 (56, 0, 0));
						//kuma
				} else if (stage_no == 20) {
						instantiate_enemy ("boss_005", new Vector3 (48, 0, 0));//boss

				} else if (stage_no == 21) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_018/enemy_018",  typeof(Transform)) ,new Vector3(32,0,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_018/enemy_018",  typeof(Transform)) ,new Vector3(16,32,0),Quaternion.identity );
			//temp.rigidbody.velocity= new Vector3(16.0f,0,0);

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_013/enemy_013",  typeof(Transform)) ,new Vector3(16,-16,0),Quaternion.identity );
			temp.rigidbody.velocity= new Vector3(16.0f,0,0);

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_013/enemy_013",  typeof(Transform)) ,new Vector3(-16,-48,0),Quaternion.identity );
			temp.rigidbody.velocity= new Vector3(-16.0f,0,0);
			*/

						instantiate_enemy ("enemy_018", new Vector3 (56, 0, 0));
						instantiate_enemy ("enemy_018", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_018", new Vector3 (32, 32, 0));

						temp = instantiate_enemy ("enemy_013", new Vector3 (24, 16, 0));
						temp.rigidbody.velocity = new Vector3 (16.0f, 0, 0);

						temp = instantiate_enemy ("enemy_013", new Vector3 (40, -24, 0));
						temp.rigidbody.velocity = new Vector3 (16.0f, 0, 0);

						temp = instantiate_enemy ("enemy_013", new Vector3 (56, 24, 0));
						temp.rigidbody.velocity = new Vector3 (-16.0f, 0, 0);


						if (!save_data.is_map_006_talk_end ()) {
								event_start ("map_006_talk_1", 1.0f);
								battle_end_type = "map_006_talk_1";
						}

						//kani
						//buta
				} else if (stage_no == 22) {

						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_018/enemy_018",  typeof(Transform)) ,new Vector3(32,-32,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_018/enemy_018",  typeof(Transform)) ,new Vector3(48,16,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_018/enemy_018",  typeof(Transform)) ,new Vector3(32,0,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_018/enemy_018",  typeof(Transform)) ,new Vector3(16,32,0),Quaternion.identity );
			//temp.rigidbody.velocity= new Vector3(16.0f,0,0);
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_012/enemy_012",  typeof(Transform)) ,new Vector3(16,-16,0),Quaternion.identity );
			//temp.rigidbody.velocity= new Vector3(16.0f,0,0);
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_012/enemy_012",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			//temp.rigidbody.velocity= new Vector3(-16.0f,0,0);
			*/

						instantiate_enemy ("enemy_018", new Vector3 (32, -32, 0));
						instantiate_enemy ("enemy_018", new Vector3 (48, 16, 0));
						instantiate_enemy ("enemy_018", new Vector3 (32, 0, 0));
						instantiate_enemy ("enemy_018", new Vector3 (16, 32, 0));
						instantiate_enemy ("enemy_012", new Vector3 (16, -16, 0));
						instantiate_enemy ("enemy_012", new Vector3 (16, -32, 0));


						//buta
						//tori
				} else if (stage_no == 23) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3(56,0,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3(32,32,0),Quaternion.identity );


			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_012/enemy_012",  typeof(Transform)) ,new Vector3(16,16,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_012/enemy_012",  typeof(Transform)) ,new Vector3(56,-16,0),Quaternion.identity );
			//temp.rigidbody.velocity= new Vector3(16.0f,0,0);
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_012/enemy_012",  typeof(Transform)) ,new Vector3(56,-32,0),Quaternion.identity );

			*/

						instantiate_enemy ("enemy_007", new Vector3 (56, 0, 0));
						instantiate_enemy ("enemy_007", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_007", new Vector3 (32, 32, 0));
						instantiate_enemy ("enemy_012", new Vector3 (16, 16, 0));
						instantiate_enemy ("enemy_012", new Vector3 (56, -16, 0));
						instantiate_enemy ("enemy_012", new Vector3 (56, -32, 0));


						//tori tairyou
						//iwa
				} else if (stage_no == 24) {
						instantiate_enemy ("boss_006", new Vector3 (48, 0, 0));//boss
						//miira
				} else if (stage_no == 25) {


						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3( 64, 4,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3(16,-24,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3(32, 40,0),Quaternion.identity );
			
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3(16,16,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3(56,32,0),Quaternion.identity );
			//temp.rigidbody.velocity= new Vector3(16.0f,0,0);
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3( 40,0,0),Quaternion.identity );


			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3( 60,-24,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_007/enemy_007",  typeof(Transform)) ,new Vector3( 40,-40,0),Quaternion.identity );
			*/
						//iwa

						instantiate_enemy ("enemy_007", new Vector3 (64, 4, 0));
						instantiate_enemy ("enemy_007", new Vector3 (16, -40, 0));
						instantiate_enemy ("enemy_007", new Vector3 (48, 40, 0));
						instantiate_enemy ("enemy_007", new Vector3 (8, 32, 0));
						instantiate_enemy ("enemy_026", new Vector3 (8, 0, 0));
						instantiate_enemy ("enemy_026", new Vector3 (24, -32, 0));
						instantiate_enemy ("enemy_026", new Vector3 (32, 32, 0));

						if (!save_data.is_map_007_talk_end ()) {
								event_start ("map_007_talk_1", 1.0f);
								battle_end_type = "map_007_talk_1";
						}


				} else if (stage_no == 26) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_024/enemy_024",  typeof(Transform)) ,new Vector3( 64, 4,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_024/enemy_024",  typeof(Transform)) ,new Vector3(16,-24,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_024/enemy_024",  typeof(Transform)) ,new Vector3(32, 40,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_026/enemy_026",  typeof(Transform)) ,new Vector3( 8, 0,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_026/enemy_026",  typeof(Transform)) ,new Vector3(24,-32,0),Quaternion.identity );
			*/

						//temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_026/enemy_024",  typeof(Transform)) ,new Vector3(32, 40,0),Quaternion.identity );

						instantiate_enemy ("enemy_026", new Vector3 (64, 4, 0));
						instantiate_enemy ("enemy_026", new Vector3 (16, -24, 0));
						instantiate_enemy ("enemy_026", new Vector3 (32, 40, 0));
						instantiate_enemy ("enemy_026", new Vector3 (16, 16, 0));
						instantiate_enemy ("enemy_026", new Vector3 (56, 32, 0));
						instantiate_enemy ("enemy_026", new Vector3 (40, 0, 0));
						instantiate_enemy ("enemy_026", new Vector3 (60, -24, 0));
						instantiate_enemy ("enemy_026", new Vector3 (40, -40, 0));





						//hone
						//saramanda
						//

				} else if (stage_no == 27) {
						//hone
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_008/enemy_008",  typeof(Transform)) ,new Vector3( 64, 4,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_008/enemy_008",  typeof(Transform)) ,new Vector3(16,-24,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_008/enemy_008",  typeof(Transform)) ,new Vector3(32, 40,0),Quaternion.identity );

			*/
						instantiate_enemy ("enemy_008", new Vector3 (64, 4, 0));
						instantiate_enemy ("enemy_008", new Vector3 (16, -24, 0));
						instantiate_enemy ("enemy_008", new Vector3 (32, 40, 0));


				} else if (stage_no == 28) {
						instantiate_enemy ("boss_007", new Vector3 (48, 0, 0));//boss
						//metal
				} else if (stage_no == 29) {
						//hone
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_024/enemy_024",  typeof(Transform)) ,new Vector3( 64, 4,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_024/enemy_024",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_024/enemy_024",  typeof(Transform)) ,new Vector3(32, 40,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_024/enemy_024",  typeof(Transform)) ,new Vector3( 8, 0,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_024/enemy_024",  typeof(Transform)) ,new Vector3(48,-32,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_010/enemy_010",  typeof(Transform)) ,new Vector3(32, 0,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_010/enemy_010",  typeof(Transform)) ,new Vector3( 8, -16,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_010/enemy_010",  typeof(Transform)) ,new Vector3(48, 32,0),Quaternion.identity );
			*/


		
						instantiate_enemy ("enemy_027", new Vector3 (40, -32, 0));
						instantiate_enemy ("enemy_027", new Vector3 (-4, 8, 0));


						instantiate_enemy ("enemy_010", new Vector3 (16, 0, 0));
						instantiate_enemy ("enemy_010", new Vector3 (16, 0, 0));
						instantiate_enemy ("enemy_010", new Vector3 (16, 0, 0));

						instantiate_enemy ("enemy_010", new Vector3 (32, 32, 0));
						instantiate_enemy ("enemy_010", new Vector3 (32, 32, 0));
						instantiate_enemy ("enemy_010", new Vector3 (32, 32, 0));
			
						instantiate_enemy ("enemy_010", new Vector3 (64, -52, 0));
						instantiate_enemy ("enemy_010", new Vector3 (64, -52, 0));
						instantiate_enemy ("enemy_010", new Vector3 (64, -52, 0));


						if (!save_data.is_map_008_talk_end () && !save_data.is_boss_009_cleared ()) {
								event_start ("map_008_talk_1", 1.0f);
								battle_end_type = "map_008_talk_1";
						}

				} else if (stage_no == 30) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_010/enemy_010",  typeof(Transform)) ,new Vector3( 64, 4,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_010/enemy_010",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_010/enemy_010",  typeof(Transform)) ,new Vector3(32, 40,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_010/enemy_010",  typeof(Transform)) ,new Vector3( 8, 0,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_027/enemy_027",  typeof(Transform)) ,new Vector3( 32, -8,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_027/enemy_027",  typeof(Transform)) ,new Vector3( 56, -32,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_027/enemy_027",  typeof(Transform)) ,new Vector3( 60, 32,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_027/enemy_027",  typeof(Transform)) ,new Vector3( 32, -8,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_027/enemy_027",  typeof(Transform)) ,new Vector3( 8, 40,0),Quaternion.identity );
			*/


						instantiate_enemy ("enemy_024", new Vector3 (64, 4, 0));
						instantiate_enemy ("enemy_024", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_024", new Vector3 (32, 40, 0));
						instantiate_enemy ("enemy_024", new Vector3 (8, 0, 0));
						instantiate_enemy ("enemy_024", new Vector3 (48, -32, 0));
			
			
						instantiate_enemy ("enemy_027", new Vector3 (32, 0, 0));
						instantiate_enemy ("enemy_027", new Vector3 (8, -16, 0));
						instantiate_enemy ("enemy_027", new Vector3 (48, 32, 0));
						//koumori dragon


		


				} else if (stage_no == 31) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_025/enemy_025",  typeof(Transform)) ,new Vector3( 64, 8,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_025/enemy_025",  typeof(Transform)) ,new Vector3(16,-32,0),Quaternion.identity );
			
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_025/enemy_025",  typeof(Transform)) ,new Vector3(32, 32,0),Quaternion.identity );
			*/
						instantiate_enemy ("enemy_025", new Vector3 (64, 8, 0));
						instantiate_enemy ("enemy_025", new Vector3 (16, -32, 0));
						instantiate_enemy ("enemy_025", new Vector3 (32, 32, 0));

				} else if (stage_no == 32) {
						instantiate_enemy ("boss_008", new Vector3 (40, 0, 0));//boss
						if (save_data.get_stage_cleared () < 32 && !save_data.is_boss_009_cleared ()) {
								battle_end_type = "boss_008";
						}

				} else if (stage_no == 0) {
				

						GameObject.Find ("hero").GetComponent <hero_controller> ().enable_is_event ();
						GameObject.Find ("UI").GetComponent<UI_controller> ().state = "event";
						battle_end_type = "tutorial";
						StartCoroutine (tutorial_start (1.0f));
	
				} else if (stage_no == -1) {

						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/enemy_028/enemy_028", typeof(Transform)), new Vector3 (60, 0, 0), Quaternion.identity);
			
						GameObject.Find ("hero").GetComponent <hero_controller> ().enable_is_event ();
						GameObject.Find ("UI").GetComponent<UI_controller> ().state = "event";

						battle_end_type = "unchi";
						StartCoroutine (unchi_start (2.0f));



				} else if (stage_no == -2) {
			
						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/boss_002/boss_002", typeof(Transform)), new Vector3 (56, 0, 0), Quaternion.identity);


						/*
			temp.GetComponent<enemy_controller>().is_stop=true;
			GameObject.Find ("hero").GetComponent <hero_controller>().enable_is_event ();
			GameObject.Find ("UI").GetComponent<UI_controller>().state="event";



			battle_end_type="boss_002";
			StartCoroutine(tutorial_sword_start( 1.0f));

			*/

				} else if (stage_no == -3) {
			
						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/boss_009/boss_009", typeof(Transform)), new Vector3 (40, 0, 0), Quaternion.identity);
						temp.GetComponent<enemy_controller> ().enable_stop ();
						GameObject.Find ("hero").GetComponent <hero_controller> ().enable_is_event ();
						GameObject.Find ("UI").GetComponent<UI_controller> ().state = "event";

						StartCoroutine (last_boss_at_stage_17_start (1.5f));
						battle_end_type = "boss_009_at_stage_17";

				} else if (stage_no == -4) {

						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/boss_009/boss_009", typeof(Transform)), new Vector3 (40, 0, 0), Quaternion.identity);

						temp.GetComponent<enemy_controller> ().enable_stop ();
						GameObject.Find ("hero").GetComponent <hero_controller> ().enable_is_event ();
						GameObject.Find ("UI").GetComponent<UI_controller> ().state = "event";


						StartCoroutine (boss_009_start (1.0f));
						battle_end_type = "boss_009";


				} else if (stage_no == -5) {
				
						//temp.GetComponent<enemy_controller>().is_stop=true;

						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/enemy_020/enemy_020", typeof(Transform)), new Vector3 (56, 0, 0), Quaternion.identity);
			
						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/enemy_020/enemy_020", typeof(Transform)), new Vector3 (16, -32, 0), Quaternion.identity);
			
						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/enemy_020/enemy_020", typeof(Transform)), new Vector3 (32, 48, 0), Quaternion.identity);

						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/etc/shopper_body", typeof(Transform)), new Vector3 (60, -32, 0), Quaternion.identity);
						temp.name = "shopper_body";
			
						GameObject.Find ("hero").GetComponent <hero_controller> ().enable_is_event ();
						GameObject.Find ("UI").GetComponent<UI_controller> ().state = "event";

			
						battle_end_type = "shop_help";


						StartCoroutine (shop_help_start (1.5f));


				} else if (stage_no == -6) {

						instantiate_enemy ("boss_009", new Vector3 (40, 0, 0));

				} else if (stage_no == -7) {
						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/boss_010/boss_010", typeof(Transform)), new Vector3 (40, 0, 0), Quaternion.identity);
		
				} else if (stage_no == -8) {
						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/boss_011/boss_011", typeof(Transform)), new Vector3 (48, 0, 0), Quaternion.identity);
		
				} else if (stage_no == -9) {

						string input_enemy_no = "boss_001";

						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/" + input_enemy_no + "/" + input_enemy_no, typeof(Transform)), new Vector3 (40, 32, 0), Quaternion.identity);
						temp.GetComponent<enemy_controller> ().is_red = true;

						input_enemy_no = "boss_002";
						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/" + input_enemy_no + "/" + input_enemy_no, typeof(Transform)), new Vector3 (40, -32, 0), Quaternion.identity);
						temp.GetComponent<enemy_controller> ().is_red = true;

						//battle_end_type="all_boss_battle";
	
				} else if (stage_no == -20) {
						/*
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_001/enemy_001",  typeof(Transform)) ,new Vector3(50,30,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_002/enemy_002",  typeof(Transform)) ,new Vector3(40,0,0),Quaternion.identity );

			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_002/enemy_002",  typeof(Transform)) ,new Vector3(50,-30,0),Quaternion.identity );
*/
						string input_enemy_no = "enemy_001";	
						Vector3 input_vector=new Vector3 (50, 30, 0);

						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/" + input_enemy_no + "/" + input_enemy_no, typeof(Transform)), input_vector, Quaternion.identity);


						input_enemy_no = "enemy_002";	
						input_vector=new Vector3 (40, 0, 0);
						
						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/" + input_enemy_no + "/" + input_enemy_no, typeof(Transform)), input_vector, Quaternion.identity);

						input_enemy_no = "enemy_002";	
						input_vector=new Vector3 (50, -30, 0);
						
						temp = (Transform)Instantiate ((Transform)Resources.Load ("prefab/enemy/" + input_enemy_no + "/" + input_enemy_no, typeof(Transform)), input_vector, Quaternion.identity);

						/*
						instantiate_enemy ("enemy_001", new Vector3 (50, 30, 0));
						instantiate_enemy ("enemy_002", new Vector3 (40, 0, 0));
						instantiate_enemy ("enemy_002", new Vector3 (50, -30, 0));
						*/
				}



		if(stage_no==PlayerPrefs.GetInt("unchi_stage")){
			temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_028/enemy_028",  typeof(Transform)) ,new Vector3(60,0,0),Quaternion.identity );
			is_unchi_stage=true;

		}

		if(stage_no==14||stage_no==16){
			
		}else{
			Destroy(GameObject.Find ("hero").transform.FindChild("boat").gameObject);
		}





		if (stage_no != 0) {
			set_play_BGM ();
		} else {
			GameObject.Find ("BGM").GetComponent<BGM_controller>().set_BGM("battle_map_1");
		}

		
	}

	public void event_start(string event_type,float wait_time){
		//temp.GetComponent<enemy_controller>().is_stop=true;

		GameObject[] gs=GameObject.FindGameObjectsWithTag("enemy");

		//print (gs.Length);

		foreach (GameObject g in gs){
			if(g.GetComponent<enemy_controller>()!=null){
				g.GetComponent<enemy_controller>().enable_stop();
			}
		}

		GameObject.Find ("hero").GetComponent <hero_controller>().enable_is_event ();
		GameObject.Find ("UI").GetComponent<UI_controller>().state="event";

		StartCoroutine(event_window_start(event_type,wait_time));
	}

	public void set_play_BGM(){

		if (is_unchi_stage) {
			GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("unchi");
		}else{

			if(battle_end_type=="unchi_normal"||battle_end_type=="unchi"){
				GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("unchi");
			}else{

				if (stage_no>=0&&stage_no<=3 ) {
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("battle_map_1");
				}else if(stage_no>=5&&stage_no<=7 ){
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("battle_map_2");
				}else if(stage_no>=9&&stage_no<=11 ){
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("battle_map_3");
				}else if(stage_no>=13&&stage_no<=15 ){
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("battle_map_4");
				}else if(stage_no>=17&&stage_no<=19){
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("battle_map_5");
				}else if(stage_no>=21&&stage_no<=23){
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("battle_map_6");
				}else if(stage_no>=25&&stage_no<=27){
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("battle_map_7");
				}else if(stage_no>=29&&stage_no<=31){
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("battle_map_8");
				
				}else if(stage_no==4||
				         stage_no==8||
				         stage_no==12||
				         stage_no==16||
				         stage_no==20||
				         stage_no==24||
				         stage_no==28
				         ){
				
				
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("boss");
				}else if(stage_no==32){

					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("boss");
				
				}else if(stage_no==-1 ||stage_no==-5){
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("unchi");
				}else if(stage_no==-2 ){
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("boss");
				}else if(stage_no==-3||stage_no==-4||stage_no==-6){
					
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("last_boss");

				}else if(stage_no==-7){
					
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("match_girl");
				}else if(stage_no==-8){
					
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("unchi");

				}else if(stage_no==-9){
					
					GameObject.Find ("BGM").GetComponent<BGM_controller>().set_play_BGM ("match_girl");
				}
			}
		}

	}

	public void set_background(){

		Sprite temp = null ;

		if (stage_no>=0&&stage_no<=4 ) {
			//temp=bacground_inages[0];
			temp=(Sprite) Resources.Load ("images/background/stage/stage_1_1",typeof(Sprite));
		}else if(stage_no>=5&&stage_no<=8 ){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_2_1",typeof(Sprite));
		}else if(stage_no>=9&&stage_no<=12 ){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_3_1",typeof(Sprite));
		}else if(stage_no ==13||stage_no==15 ){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_4_1",typeof(Sprite));
		}else if(stage_no ==14||stage_no==16 ){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_4_2",typeof(Sprite));
		}else if(stage_no>=17&&stage_no<=18){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_5_1",typeof(Sprite));
		}else if(stage_no>=19&&stage_no<=20){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_5_2",typeof(Sprite));
		}else if(stage_no>=21&&stage_no<=24){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_6_1",typeof(Sprite));
		}else if(stage_no>=25&&stage_no<=28){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_7_1",typeof(Sprite));
		}else if(stage_no>=29&&stage_no<=32){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_8_1",typeof(Sprite));
		}else if(stage_no==-1 ){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_2_1",typeof(Sprite));
		}else if(stage_no==-2 ){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_2_1",typeof(Sprite));
		}else if(stage_no>=-3){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_5_1",typeof(Sprite));
		}else if(stage_no==-4 ){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_9_1",typeof(Sprite));
		}else if(stage_no==-5 ){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_3_1",typeof(Sprite));
		}else if(stage_no==-6 ){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_9_1",typeof(Sprite));
		}else if(stage_no==-7){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_9_1",typeof(Sprite));
		}else if(stage_no==-8){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_2_1",typeof(Sprite));
		}else if(stage_no==-9){
			temp=(Sprite) Resources.Load ("images/background/stage/stage_10_2",typeof(Sprite));
		}

		if(temp!=null){
			GameObject.Find ("background").GetComponent<SpriteRenderer> ().sprite = temp;
		}
	}
	/*
	public void event_start(){

		GameObject.Find ("hero").GetComponent <hero_controller>().enable_is_event();
		
		GameObject[] gc=GameObject.FindGameObjectsWithTag("enemy");
		
		foreach (GameObject g in gc){
			g.GetComponent<enemy_controller>().is_stop=true;
		}
		
		GameObject.Find ("UI").GetComponent<UI_controller>().state="event";
*/
	/*
	public void event_end(){
		GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();
		
		GameObject[] gc=GameObject.FindGameObjectsWithTag("enemy");
		
		foreach (GameObject g in gc){
			g.GetComponent<enemy_controller>().disable_stop();
		}
		
		GameObject.Find ("UI").GetComponent<UI_controller>().state="normal";
		
*/

	public IEnumerator event_window_start(string event_type, float wait_time){
		
		yield return new WaitForSeconds(wait_time);
		
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type=event_type;
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
	}



	public IEnumerator tutorial_start(float wait_time){
		
		yield return new WaitForSeconds(wait_time);

		GameObject temp_game = new GameObject ();

		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type="tutorial_1";
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);

	}

	public IEnumerator unchi_start(float wait_time){
		
		yield return new WaitForSeconds(wait_time);

		GameObject temp_game = new GameObject ();

		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type="unchi_1";

		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
		
	}
	/*
	public IEnumerator tutorial_sword_start(float wait_time){
		
		yield return new WaitForSeconds(wait_time);
		
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type="tutorial_sword_1";
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
		
	}
	*/

	public IEnumerator shop_help_start(float wait_time){
		
		yield return new WaitForSeconds(wait_time);
		
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type="shop_help_1";
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
		
	}

	public IEnumerator last_boss_at_stage_17_start(float wait_time){
		
		yield return new WaitForSeconds(wait_time);
		
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type="last_boss_at_stage_17_1";
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
		
	}

	public IEnumerator boss_009_start(float wait_time){
		
		yield return new WaitForSeconds(wait_time);
		
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type="boss_009_start_1";
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
		
	}

	public IEnumerator hinoki_used(float wait_time){
		
		yield return new WaitForSeconds(wait_time);
		
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type="hinoki_used_1";
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
		
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void enemy_dead(enemy_controller input_enemy,long input_exp){

		//print (save_data.AddExp (input_exp.ToString ()));

		if (save_data.AddExp (input_exp.ToString ())=="level_up") {
			GameObject.Find ("hero").GetComponent<hero_controller>().level_up();
			//level_up.parent=GameObject.Find ("hero").transform;
			//level_up.localPosition=new Vector3(0,32,0);
		};

		if (stage_no==-9) {
			boss_kill_count++;
			if(boss_kill_count<8){

				if(boss_kill_count==4) {
					GameObject[] gs = GameObject.FindGameObjectsWithTag("enemy");
					if(gs[0].GetComponent<enemy_controller>().enemy_no=="boss_005"&&gs[0].GetComponent<enemy_controller>().next_attack_state=="frozen"){
						gs[0].GetComponent<enemy_controller>().next_attack_state="break_ice";
					}
				}

				Transform boss;
				string input_enemy_no= "boss_00"+ (boss_kill_count+2);
				/*string input_enemy_no= "boss_00"+ (boss_kill_count+2);
				int rnd=Random.Range (0,3);


				if(rnd==0){

					boss= (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/"+input_enemy_no +"/"+input_enemy_no,  typeof(Transform)) , new Vector3(40,-32,0),Quaternion.identity );


				}else if(rnd==1){
					boss= (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/"+input_enemy_no +"/"+input_enemy_no,  typeof(Transform)) , new Vector3(40,32,0),Quaternion.identity );
				
				}else{
					boss= (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/"+input_enemy_no +"/"+input_enemy_no,  typeof(Transform)) , new Vector3(40,0,0),Quaternion.identity );

				}*/

				//print (enemy.transform.position.y);

				Vector3[] vectors= new Vector3[3]{new Vector3(40,32,0),new Vector3(40,0,0),new Vector3(40,-32,0)};

				Vector3 vector=vectors[0];
				Vector3 enemy_position=vectors[0]; 

				GameObject[] gss = GameObject.FindGameObjectsWithTag("enemy");

				if(gss.Length>0){
					print ("ok");
					enemy_position=gss[0].transform.position;
				}

				float prev_d =0.0f;

				foreach(Vector3 v in vectors){

					float d=  1/((enemy_position-v).sqrMagnitude+1)+1/((input_enemy.transform.position-v).sqrMagnitude+1);

					//print (d);

					if(prev_d==0.0f||prev_d>d){
						vector=v;
						prev_d=d;
						//print ("updated"+d);
					}

				}

			
			
					
				boss= (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/"+input_enemy_no +"/"+input_enemy_no,  typeof(Transform)) , vector,Quaternion.identity );
					
					

				if(boss_kill_count<3){
					boss.GetComponent<enemy_controller> ().is_red = true;
				//}else{
				}else if(boss_kill_count<5){
					boss.GetComponent<enemy_controller> ().is_yellow = true;
				//}else if(boss_kill_count==7){
					//boss.GetComponent<enemy_controller> ().is_yellow = true;
				}
				


			}
		}

		//print (input_exp);
		//total_enemy_exp += input_exp;

		GameObject[] temp = GameObject.FindGameObjectsWithTag("enemy");
		remainging_enemy_count =temp.Length;

		GameObject[] items = GameObject.FindGameObjectsWithTag("item");
		//remainging_enemy_count =temp.Length;

		if (remainging_enemy_count==0){
			GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("clear");
		}

		if (remainging_enemy_count==0&&items.Length==0){
			battle_end();
		}
	}


	public void got_item(){
		GameObject[] temp = GameObject.FindGameObjectsWithTag("enemy");
		remainging_enemy_count =temp.Length;
		
		GameObject[] items = GameObject.FindGameObjectsWithTag("item");
		//remainging_enemy_count =temp.Length;

		if (remainging_enemy_count==0&&items.Length==0){
			battle_end();
		}
	}





	public void battle_end(){

		is_stage_end = true;

		GameObject.Find ("hero").GetComponent <hero_controller> ().disable_using_sword ();
		GameObject.Find ("hero").GetComponent <hero_controller>().enable_is_event ();


		if(stage_no==-3){
			save_data.set_stage_cleared (17);
		}

		if (battle_end_type == "normal") {

			stage_clear ();


		}else if(battle_end_type=="tutorial"){


			GameObject.Find ("UI").GetComponent<UI_controller>().state="event";
			StartCoroutine(tutorial_clear(0));
			save_data.set_stage_cleared (1);
			save_data.SetInt("met_hinoki",1);

		}else if(battle_end_type=="unchi"){
			//GameObject.Find ("hero").GetComponent <hero_controller>().enable_is_event ();
			GameObject.Find ("UI").GetComponent<UI_controller>().state="event";
			StartCoroutine(unchi_end( 1.0f));
			save_data.set_stage_cleared (5);
		}else if(battle_end_type=="boss_002"){
			stage_clear ();
			save_data.set_stage_cleared (8);

		}else if(battle_end_type=="shop_help"){

			GameObject.Find ("UI").GetComponent<UI_controller>().state="event";

			//save_data.set_shop_open();
			save_data.set_stage_cleared (11);
		
			StartCoroutine(shop_help_end(0));

		}else if(battle_end_type=="boss_008"){
			//GameObject.Find ("hero").GetComponent <hero_controller>().enable_is_event ();
			GameObject.Find ("UI").GetComponent<UI_controller>().state="event";
			StartCoroutine(boss_008_end( 1.0f));
			save_data.set_stage_cleared (32);

		}else if(battle_end_type=="boss_009"){
			//GameObject.Find ("hero").GetComponent <hero_controller>().enable_is_event ();

			GameObject.Find ("UI").GetComponent<UI_controller>().state="event";
			StartCoroutine(boss_009_end( 1.0f));

		}else if(battle_end_type=="boss_009_at_stage_17"){
			save_data.boss_009_clear();

			GameObject.Find ("UI").GetComponent<UI_controller>().state="event";

			Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/end_credit_UI",  typeof(Transform)) );
			temp.GetComponent<end_credit_controller>().is_normal=false;

		}else{
			stage_clear ();
		}


	}

	public IEnumerator shop_help_end(float wait_time){
		
		yield return new WaitForSeconds(wait_time);

		//print ("ok");
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();

		temp.type="shop_help_end_1";
		
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
	}

	public IEnumerator tutorial_clear(float wait_time){
		
		yield return new WaitForSeconds(wait_time);

		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type="tutorial_clear_1";

		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
	}

	public IEnumerator unchi_end(float wait_time){
		
		yield return new WaitForSeconds(wait_time);
		
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();

		if(save_data.GetString ("map_hinoki")=="sword_001_02"){
			temp.type="unchi_end_dirty_1";
		}else{
			temp.type="unchi_end_clean_1";
		}
		
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
		
	}

	public IEnumerator boss_008_end(float wait_time){
		
		yield return new WaitForSeconds(wait_time);
		
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();

		temp.type="boss_008_end_1";

		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
		
	}

	public IEnumerator boss_009_end(float wait_time){
		
		yield return new WaitForSeconds(wait_time);
		
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();


		temp.type="boss_009_end_1";
		
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);
		
	}


	public void stage_clear(){

		save_data.set_stage_cleared (stage_no);

		return_to_map_pressed(1.0f);
	}


	public void game_over(){
		if (battle_end_type == "tutorial") {
			GameObject.Find ("hero").GetComponent <hero_controller>().is_event=true;
			GameObject.Find ("UI").GetComponent<UI_controller>().state="event";
			save_data.SetInt("met_hinoki",1);

			StartCoroutine(tutorial_dead(0));
		}else{

			save_data.SetInt("game_over",0);

			is_stage_end = true;

			return_to_map_pressed(1.0f);
			//StartCoroutine(return_to_map());
		}
	}


	public IEnumerator tutorial_dead(float wait_time){
		
		yield return new WaitForSeconds(wait_time);
		
		GameObject temp_game = new GameObject ();
		
		UI_button_controller temp = temp_game.AddComponent <UI_button_controller>();
		temp.type="tutorial_dead_1";
		
		GameObject.Find ("UI").GetComponent<UI_controller>().UI_OnMouseUpAsButton(temp,transform);

	}

	public Transform instantiate_enemy(string input_enemy_no,Vector3 input_position){
		Transform temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/"+input_enemy_no +"/"+input_enemy_no,  typeof(Transform)) ,input_position,Quaternion.identity );

		//temp.renderer.material.color = new Color (1.0f, 1.0f, 0.5f);
		//temp.GetComponent<enemy_controller> ().is_yellow = true;
		/*
		print (temp.GetComponent<enemy_controller> ().max_HP);
		temp.GetComponent<enemy_controller> ().max_HP = temp.GetComponent<enemy_controller> ().max_HP*8;
		temp.GetComponent<enemy_controller> ().remaining_HP *= 8;

		temp.GetComponent<enemy_controller> ().enemy_exp *= 16;
		temp.GetComponent<enemy_controller> ().enemy_money *= 16;
		*/


		if (stage_no <= save_data.GetInt ("stage_cleared")) {

			int rnd=Random.Range (1,160-GameObject.Find ("controller").GetComponent<level_data_controller> ().get_level ());
			if(rnd<=save_data.GetInt ("stage_cleared")){
				temp.GetComponent<enemy_controller> ().is_red = true;
			}else if(rnd<=save_data.GetInt ("stage_cleared")*2){
				temp.GetComponent<enemy_controller> ().is_yellow = true;
			}

		}

		return temp;
	}

	public void return_to_map_pressed(float wait_time){

		if(battle_end_type=="tutorial_sword_1"){
			save_data.met_boss_002();

		}else if(battle_end_type=="map_001_talk_1"){
			save_data.map_001_talk_end();

		}else if(battle_end_type=="map_005_talk_1"){
			save_data.map_005_talk_end();

		}else if(battle_end_type=="map_006_talk_1"){
			save_data.map_006_talk_end();

		}else if(battle_end_type=="map_007_talk_1"){
			save_data.map_007_talk_end();

		}else if(battle_end_type=="map_008_talk_1"){
			save_data.map_008_talk_end();
		}

		GameObject.Find("UI").GetComponent<UI_controller>().state="event";
		StartCoroutine(return_to_map(wait_time));
	}

	public IEnumerator return_to_map(float wait_time){



		yield return new WaitForSeconds(wait_time);
		
		FadeManager.Instance.LoadLevel("map", 0.5f);
	}
	
}
