﻿using UnityEngine;
using System.Collections;

public class hero_controller : MonoBehaviour {

	public bool is_attack=false;
	public bool is_attack_prev=false;
	public bool is_stop = false;
	public bool is_event=false;
	public bool is_dead=false;
	public bool is_invincible=false;

	public bool is_initialized = false; 

	//public bool force_sword_initialize=false;

	public int sword_slot;
	public string sword_no;
	public bool is_using_sword=false;
	public float using_sword_end_time;
	//public bool change_sword_scale=false;
	public float sword_scale=1.0f;

	public bool is_fire_enabled=false;
	public int fire_level;

	public bool is_leaf_enabled = false;
	public int leaf_level;

	public bool is_heal_enabled=false;
	public int heal_point;

	public bool is_revive=false;
	public bool is_rotating=false;
	public float rotation_speed=-360;
	public bool is_invincible_by_sword=false;

	//public bool is_stage_cleared=false;

	public Vector3 rotation_by_sword;

	public bool is_match_enabled = false;
	public float next_match_throw_time=0;

	public float attack_multiplier = 1.0f;



	public float invincible_end_time;
	public float invincible_time_interval=0.5f;

	public Transform sword=null;
	public Transform hero_body=null;
	public int remaining_HP;
	public int max_HP;
	public long attack_point;
	public long sword_attack_point;

	public long total_exp;
	//public int exp_in_this_stage = 0;

	public HP_bar_controller HP_bar;
	public HP_count_controller HP_count;
	

	private bool is_screen_limit_set=true;

	//private int screen_width=160;
	//private int screen_height=128;

	private float limit_x_left =-80;
	private float limit_x_right=80;
	private float limit_y_up=64;
	private float limit_y_down=-64;

	private int charactor_width=16;
	private int charactor_height=32;

	private Color hero_color = new Color (1.0f,1.0f,1.0f);
	
	public bool is_sword_test;
	public string sword_no_for_test;

	public bool is_boss_009_barrier_enablaed = false;

	private bool is_freeze=false;
	private float freeze_end_time;

	private float last_fire_time;

	private bool is_paralyze;
	private float paralyze_end_time;


	public float enemy_freeze_time=0;

	public save_data_controller save_data;

	// Use this for initialization
	void Start () {
//		/save_data = GameObject.Find ("controller").GetComponent<save_data_controller> ();
		transform.FindChild("hero_arm_1").transform.renderer.enabled=true;
		transform.FindChild("hero_hand_1").transform.renderer.enabled=true;
		transform.FindChild("hero_arm_2").transform.renderer.enabled=false;
		transform.FindChild("hero_hand_2").transform.renderer.enabled=false;

	}

	void Update () {

		if(!is_event){
			if(is_initialized){

				change_sword_state (false);

				damage_controller ("update");
			}

	
		if(is_match_enabled&&Time.time>next_match_throw_time){
			next_match_throw_time=Time.time+0.5f;

			Vector3 temp_position= transform.position+new Vector3(16,0,0);
			Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/sword/sword_010_01/match",typeof(Transform)) ,temp_position,Quaternion.identity );
			temp.GetComponent<magic_controller>().attack_point=total_attack_point();
			
			GameObject[] temps= GameObject.FindGameObjectsWithTag("enemy");
			
			Vector3 velocity=new Vector3(128.0f,0,0);
			
				if(temps.Length==0){
					velocity=new Vector3(128.0f,0,0);
				}else{
					velocity=(temps[0].transform.position-temp_position).normalized*128.0f;

				}
			temp.rigidbody.velocity=velocity;

		}

		check_using_sword ();
		check_rotation ();
		

		}
		if(hero_body!=null){
			//hero_body.position=transform.position;
		}

		//check_leaf ();
		
	}


	public void enable_is_event(){
		rigidbody.velocity = new Vector3 (0,0,0);
		is_event = true;
	}

	public void disable_is_event(){
		rigidbody.velocity = new Vector3 (0,0,0);
		is_event = false;
	}

	public void initialize_hero(){
		initialize_sword ();

		//total_exp=
		max_HP=save_data.max_HP;
		remaining_HP = max_HP;

		attack_point = GameObject.Find ("controller").GetComponent<level_data_controller> ().get_attack_point ();
		
		add_HP_bar ();
		add_HP_count ();

		is_initialized = true;

	}
	

	public void add_HP_bar(){
		Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/HP_bar/HP_bar",  typeof(Transform)) ,new Vector3( -72,80,0),Quaternion.identity );
		
		temp.parent = GameObject.Find ("UI").transform;
		
		HP_bar = temp.GetComponent<HP_bar_controller> ();
		HP_bar.set_scale(4.0f,0.5f);
		HP_bar.set_layer ("text");

		temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
		temp.parent=GameObject.Find ("UI").transform;
		window_controller window= temp.GetComponent<window_controller> ();
		window.show_window(10,2,new Vector3(0, 80,0),"arrows",  0.0f);


		//temp=
	
		
	}

	public void add_HP_count(){
		Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/HP_bar/HP_count",  typeof(Transform)) ,new Vector3(72, 78,0),Quaternion.identity );
		
		temp.parent = GameObject.Find ("UI").transform;

		HP_count= temp.GetComponent<HP_count_controller> ();
		HP_count.set_max_HP (max_HP);
	}

	
	// Update is called once per frame


	public void damage_controller(string input){

		if (input == "update") {
			if(is_invincible&&Time.time>invincible_end_time){
				is_invincible=false;
				change_color(new Color(hero_color.r,hero_color.g,hero_color.b,1.0f));
			}

			if(is_freeze){
				if(Time.time >freeze_end_time){
					is_freeze=false;
					Destroy(transform.FindChild("ice").gameObject);
					
					damage_controller ("invincible_start");
				}
			}

			if(is_paralyze){
				if(Time.time >paralyze_end_time){
					is_paralyze=false;

					damage_controller ("invincible_start");

					change_color_to_normal ();
				}
			}
		
		}else if(input=="invincible_start"&&!is_invincible){
			invincible_end_time=Time.time+invincible_time_interval;
			is_invincible=true;
			change_color(new Color(hero_color.r,hero_color.g,hero_color.b,0.5f));
		
		}else if(input=="start_freeze"&&!is_invincible_by_sword&&!is_invincible&&!is_freeze&&!is_paralyze){
			is_freeze = true;
			freeze_end_time = Time.time + 2.0f;
			rigidbody.velocity= new Vector3(0,0,0);

			if(transform.FindChild ("ice")==null){
				Transform temp = (Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/etc/ice",  typeof(Transform)) , transform.position,Quaternion.identity );
				temp.name="ice";
				temp.parent=transform;
				temp.GetComponent<ice_controller>().set_ice(1,2,transform);
			}
		}else if(input=="start_paralyze"&&!is_using_sword&&!is_invincible&&!is_freeze&&!is_paralyze){
			is_paralyze = true;
			paralyze_end_time = Time.time + 2.0f;
			rigidbody.velocity= new Vector3(0,0,0);
			change_color (new Color(1.0f,1.0f,0.5f));
		}

	}


	public void change_sword_state(bool force_change){
		//print(force_change);
		if(GameObject.Find ("UI").GetComponent<UI_controller>().state=="normal"||force_change){

			/*
			if(change_sword_scale){
				sword.localScale= new Vector3(sword_scale,sword_scale,1.0f);
				change_sword_scale=false;
			}*/

			if((is_attack!=is_attack_prev)||force_change){

				if (is_attack) {

					transform.FindChild("hero_arm_1").transform.renderer.enabled=false;
					transform.FindChild("hero_hand_1").transform.renderer.enabled=false;
					transform.FindChild("hero_arm_2").transform.renderer.enabled=true;
					transform.FindChild("hero_hand_2").transform.renderer.enabled=true;

					if(sword_scale==1.0f){
						sword.localPosition= new Vector3(18,-4,0);
						sword.localEulerAngles =new Vector3(0,0,270);
					}else if(sword_scale==2.0f){
						sword.localPosition= new Vector3(36,-4,0);
						sword.localEulerAngles =new Vector3(0,0,270);
					}else if(sword_scale==3.0f){
						sword.localPosition= new Vector3(54,-4,0);
						sword.localEulerAngles =new Vector3(0,0,270);
					}else if(sword_scale==4.0f){
						sword.localPosition= new Vector3(72,-4,0);
						sword.localEulerAngles =new Vector3(0,0,270);
					}
					
				}else{
					transform.FindChild("hero_arm_1").transform.renderer.enabled=true;
					transform.FindChild("hero_hand_1").transform.renderer.enabled=true;
					transform.FindChild("hero_arm_2").transform.renderer.enabled=false;
					transform.FindChild("hero_hand_2").transform.renderer.enabled=false;
					if(sword_scale==1.0f){
						sword.localPosition= new Vector3(-1,11,0);
						sword.localEulerAngles =new Vector3(0,0,0);
					}else if(sword_scale==2.0f){
						sword.localPosition= new Vector3(-1,27,0);
						sword.localEulerAngles =new Vector3(0,0,0);
					}else if(sword_scale==3.0f){
						sword.localPosition= new Vector3(-1,43,0);
						sword.localEulerAngles =new Vector3(0,0,0);
					}else if(sword_scale==4.0f){
						sword.localPosition= new Vector3(-1,59,0);
						sword.localEulerAngles =new Vector3(0,0,0);
					}
				}
				is_attack_prev=is_attack;
			}
		}
	
	}


	public void initialize_sword(){
		if(!is_using_sword){

			if (GameObject.Find ("sword")!=null) {
				Destroy (GameObject.Find("sword").gameObject);
			}


			sword_slot = save_data.get_sword_equipped_slot ();


			string temp_sword_no = save_data.get_sword_no (sword_slot);

			if(is_sword_test){
				temp_sword_no=sword_no_for_test;
			}

			if(temp_sword_no=="null"){
				temp_sword_no="sword_001_01";
			}

			sword_no = temp_sword_no;
			sword= (Transform) Instantiate(Resources.Load ("prefab/sword/"+temp_sword_no+"/"+temp_sword_no,typeof(Transform)),transform.position, Quaternion.identity);
			sword.GetComponent<sword_controller>().sword_no=temp_sword_no;
			if (sword.GetComponent<item_controller> () != null) {
				Destroy (sword.GetComponent<item_controller> ());
			}

			//sword.GetComponent<sword_controller> ().load_sword_data ();
			//sword.GetComponent<sword_controller> ().power_up = save_data.get_power_up (sword_slot);
			sword.name="sword";

			sword_attack_point = GameObject.Find ("controller").GetComponent<item_data_controller> ().sword_attack_point (sword_no)+save_data.get_power_up (sword_slot);

			sword.parent = transform;

			change_sword_state(true);

			//sword.localPosition= new Vector3(-1,11,0);
			//sword.localEulerAngles =new Vector3(0,0,0);

		}

	}


	public void attack(){

		if(!is_freeze&&!is_paralyze){
			is_attack = true;

			if (is_fire_enabled) {
				create_fire (fire_level);
				GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("fire");
			}

			if (is_leaf_enabled) {
				create_leaf(leaf_level);
				GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("wind");
			}

			if (is_heal_enabled) {
				heal (heal_point);
				GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("heal");
			}
		}

	}


	public void stop_attack(){
		is_attack = false;
	}

	public  void start_move(){

		if(!is_dead&&!is_stop&&!is_event&&!is_freeze&&!is_paralyze){
		Vector3 mouse_point=GameObject.Find("Main Camera").camera.ScreenToWorldPoint(Input.mousePosition);
			
			//GameObject target = GameObject.Find("hero");
			
			float speed = 6.0f;
			
			Vector3 speed_vector=new Vector3(mouse_point.x-GameObject.Find("arrows").transform.position.x,mouse_point.y-GameObject.Find("arrows").transform.position.y, 0);
			
			rigidbody.velocity = speed_vector*speed;

			move_limit ();
		}


	}

	public void stop_move() {
		
		//GameObject target = GameObject.Find("hero");
		
		//float speed =  0.0f;
		
		//Vector3 speed_vector=new Vector3(0, 0, 0);
		
		rigidbody.velocity = new Vector3(0, 0, 0);
		//GameObject.Find ("hero_body").rigidbody.velocity= new Vector3(0, 0, 0);
	}

	public void move_limit(){
		
		//GameObject target = GameObject.Find("hero");
		
		if(is_screen_limit_set){
			Vector3 velocity = rigidbody.velocity;
			Vector3 position = transform.position;
			
			if ((transform.position.x<=limit_x_left + (charactor_width/2))&&(velocity.x<0)) {
				
				velocity.x=0;
				position.x=limit_x_left+(charactor_width/2);
				
			}else if((transform.position.x>=limit_x_right-(charactor_width/2))&&(velocity.x>0)){
				velocity.x=0;
				position.x=limit_x_right-(charactor_width/2);
			}
			
			if ((transform.position.y<=limit_y_down+(charactor_height/2))&&(velocity.y<0)) {
				velocity.y=0;
				position.y=limit_y_down+(charactor_height/2);
			}else if((transform.position.y>=limit_y_up-(charactor_height/2))&&(velocity.y>0)){
				velocity.y=0;
				position.y=limit_y_up-(charactor_height/2);
			}
			
			transform.position = position;
			rigidbody.velocity = velocity;
		}
		
	}

	public void level_up(){

		Instantiate(( Transform) Resources.Load ("prefab/UI/etc/level_up",  typeof(Transform)) ,new Vector3( 0, 0,0),Quaternion.identity );

		max_HP=save_data.max_HP;
		//remaining_HP = max_HP;
		
		attack_point = GameObject.Find ("controller").GetComponent<level_data_controller> ().get_attack_point ();
		HP_bar.show (remaining_HP,max_HP );
		HP_count.show(remaining_HP,max_HP);

		GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("power_up");

	}

	public void hit(int damage){

		if(is_initialized&&!is_invincible&&!is_dead&&!is_invincible_by_sword&&!is_event){
			remaining_HP -= damage;

			damage_controller ("invincible_start");


			GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("hero_hit");


			//Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/text/hero_damage_count",  typeof(Transform)) , transform.position,Quaternion.identity );
			
			//temp.GetComponent<damage_count_controller> ().set_text(damage.ToString (),transform.position);
			
			if (remaining_HP <= 0) {
				if(!is_revive){
					remaining_HP=0;
					is_dead=true;
					rigidbody.velocity=new Vector3(0,0,0);
					//GameObject.Find ("hero_body").rigidbody.velocity= new Vector3(0, 0, 0);
					GameObject.Find("controller").GetComponent<battle_controller>().game_over();
				}else{
					remaining_HP=max_HP;
					disable_revive();
				}
			}

			HP_bar.show (remaining_HP,max_HP );
			HP_count.show(remaining_HP,max_HP);


		}

	}

	public void heal(int input_heal_point){

		remaining_HP += input_heal_point;

		if (remaining_HP >= max_HP) {
			remaining_HP=max_HP;
		}

		HP_bar.show (remaining_HP,max_HP );
		HP_count.show(remaining_HP,max_HP);

		Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/text/heal_count",  typeof(Transform)) , transform.position,Quaternion.identity );
		
		temp.GetComponent<damage_count_controller> ().set_text(input_heal_point.ToString (),transform.position);

	}

	public long total_attack_point(){

		return Mathf.RoundToInt(attack_multiplier*(attack_point+sword_attack_point));
	}

	public void start_using_sword(){

		if(!is_using_sword){
			is_using_sword = true;
			bool aura_use=true;


			if(sword_no=="sword_002_01"){
				change_sword_scale(2.0f);
				change_sword_state(true);
				//sword.localPosition= new Vector3(-1,27,0);
				//sword.localScale= new Vector3(sword_scale,sword_scale,1.0f);
				//change_sword_scale=true;

				//sword.localScale= new Vector3(sword_scale,sword_scale,1.0f);
				using_sword_end_time = Time.time + 6.0f;
				attack_multiplier=3.0f;

			}else if(sword_no=="sword_002_02"){
				change_sword_scale(3.0f);
				change_sword_state(true);
				//sword.localPosition= new Vector3(-1,43,0);
				//sword.localScale= new Vector3(sword_scale,sword_scale,1.0f);
				//change_sword_scale=true;
				//sword.localScale= new Vector3(sword_scale,sword_scale,1.0f);
				using_sword_end_time = Time.time + 8.0f;
				attack_multiplier=6.0f;

			}else if(sword_no=="sword_002_03"){
				change_sword_scale(4.0f);
				change_sword_state(true);
				//sword.localPosition= new Vector3(-1,59,0);
				//sword.localScale= new Vector3(sword_scale,sword_scale,1.0f);
				//change_sword_scale=true;
				//sword.localScale= new Vector3(sword_scale,sword_scale,1.0f);
				using_sword_end_time = Time.time + 10.0f;
				attack_multiplier=12.0f;
				
			}else if(sword_no=="sword_003_01"){
				is_fire_enabled=true;
				fire_level=1;
				using_sword_end_time = Time.time + 6.0f;
				attack_multiplier=3.0f;

			}else if(sword_no=="sword_003_02"){
				is_fire_enabled=true;
				fire_level=2;
				using_sword_end_time = Time.time + 8.0f;
				attack_multiplier=6.0f;

			}else if(sword_no=="sword_003_03"){

				is_fire_enabled=true;
				fire_level=3;
				using_sword_end_time = Time.time + 10.0f;
				attack_multiplier=12.0f;

			}else if(sword_no=="sword_004_01"){
			
				is_leaf_enabled=true;
				leaf_level=1;
				using_sword_end_time = Time.time + 6.0f;
				attack_multiplier=3.0f;

			}else if(sword_no=="sword_004_02"){
				
				is_leaf_enabled=true;
				leaf_level=2;
				using_sword_end_time = Time.time + 8.0f;
				attack_multiplier=6.0f;

			}else if(sword_no=="sword_004_03"){
				
				is_leaf_enabled=true;
				leaf_level=3;
				using_sword_end_time = Time.time + 10.0f;
				attack_multiplier=12.0f;

			}else if(sword_no=="sword_005_01"){

				is_heal_enabled=true;
				heal_point=max_HP/16;
				using_sword_end_time = Time.time + 6.0f;

				attack_multiplier=4.0f;

			}else if(sword_no=="sword_005_02"){
				
				is_heal_enabled=true;
				heal_point=max_HP/8;
				using_sword_end_time = Time.time + 8.0f;
				
				attack_multiplier=8.0f;
			}else if(sword_no=="sword_005_03"){
				
				is_heal_enabled=true;
				heal_point=max_HP/4;
				using_sword_end_time = Time.time + 10.0f;
				
				attack_multiplier=16.0f;

			}else if(sword_no=="sword_006_01"){
				enemy_freeze_time=2.0f;
				using_sword_end_time = Time.time + 6.0f;
				attack_multiplier=4.0f;

			}else if(sword_no=="sword_006_02"){
				enemy_freeze_time=4.0f;
				using_sword_end_time = Time.time + 8.0f;
				attack_multiplier=8.0f;

			}else if(sword_no=="sword_006_03"){
				enemy_freeze_time=6.0f;
				using_sword_end_time = Time.time + 10.0f;
				attack_multiplier=16.0f;

			}else if(sword_no=="sword_007_01"){
				enable_rivive();
				using_sword_end_time = Time.time + 6.0f;
				attack_multiplier=16.0f;

			}else if(sword_no=="sword_007_02"){

				enable_rivive();
				using_sword_end_time = Time.time + 8.0f;
				attack_multiplier=32.0f;

			}else if(sword_no=="sword_007_03"){

				enable_rivive();
				using_sword_end_time = Time.time + 10.0f;
				attack_multiplier=64.0f;

			}else if(sword_no=="sword_008_01"){
				aura_use=false;

				is_rotating=true;
				rotation_speed=360;
				is_invincible_by_sword=true;
				using_sword_end_time = Time.time + 10.0f;
				attack_multiplier=8.0f;

			}else if(sword_no=="sword_008_02"){
				aura_use=false;

				is_rotating=true;
				rotation_speed=360*2;
				is_invincible_by_sword=true;
				using_sword_end_time = Time.time + 12.0f;
				attack_multiplier=16.0f;

			}else if(sword_no=="sword_008_03"){
				aura_use=false;

				is_rotating=true;
				rotation_speed=360*4;
				is_invincible_by_sword=true;
				using_sword_end_time = Time.time + 14.0f;
				attack_multiplier=32.0f;
			
			}else if(sword_no=="sword_010_01"){

				is_match_enabled=true;

				using_sword_end_time = Time.time + 10.0f;
				attack_multiplier=64.0f;
			}else if(sword_no=="sword_011_02"){
				
				//is_match_enabled=true;
				is_invincible_by_sword=true;
				using_sword_end_time = Time.time + 10.0f;
				attack_multiplier=16.0f;

			}else if((sword_no=="sword_001_01"||sword_no=="sword_001_02")
			         &&is_boss_009_barrier_enablaed){

				is_invincible_by_sword=true;
				using_sword_end_time = Time.time + 10.0f;
				save_data.change_hinoki();

				GameObject.Find ("controller").GetComponent<battle_controller>().event_start("hinoki_used_1",1.0f);
				//StartCoroutine(GameObject.Find ("controller").GetComponent<battle_controller>().hinoki_used(1.0f));

			}else{
				is_using_sword=false;
			}


			if(is_using_sword){

				if(GameObject.Find ("tap_count")!=null){
					GameObject.Find ("tap_count").GetComponent<tap_count>().enable_count();
				}
				if(aura_use){
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/hero/aura",typeof(Transform)) ,transform.position+new Vector3(0,8,0),Quaternion.identity );
					temp.name="aura";
					temp.localScale=new Vector3(0.75f,1.0f,1.0f);
					temp.parent=transform;
				}

				GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("using_sword");


				save_data.enable_is_using_sword();
			}
		}
	}

	public void disable_using_sword(){

		if (is_using_sword) {
			if(GameObject.Find ("tap_count")!=null){
				GameObject.Find ("tap_count").GetComponent<tap_count>().disable_count();
			}
			if(sword_no=="sword_001_03"){
				using_sword_end_time=Time.time+10.0f;

			}else{
				attack_multiplier = 1.0f;
				change_sword_scale(1.0f);
				//change_sword_scale=true;
				//sword.localScale = new Vector3 (1.0f, 1.0f, 1.0f);

				//sword.localScale= new Vector3(sword_scale,sword_scale,1.0f);

				is_fire_enabled = false;
				fire_level = 0;

				is_leaf_enabled = false;
				leaf_level = 0;

				is_heal_enabled = false;
				heal_point = 0;

				enemy_freeze_time = 0;

				is_rotating = false;
				is_invincible_by_sword = false;
				transform.rotation = Quaternion.Euler (new Vector3 (0, 0, 0));

				is_match_enabled = false;



				is_using_sword = false;

				if (transform.FindChild ("aura") != null) {
						Destroy (transform.FindChild ("aura").gameObject);
				}

				change_sword_state(true);
				save_data.sword_use_end ();

				initialize_sword();
			}



		}
	}

	public void check_using_sword(){
		if (is_using_sword) {
			if(Time.time>using_sword_end_time){
				disable_using_sword();

			}
		}
	}

	public void create_fire(int input_level){

		if(Time.time-last_fire_time>0.5f){
			long input_attack_point = 0; 

			//print (input_level);
			
			if(input_level==1){
				input_attack_point = total_attack_point(); 
			}else if(input_level==2){
				input_attack_point = total_attack_point(); 
			}else if(input_level==3){
				input_attack_point = total_attack_point(); 
			}

			//print (input_level);
			Vector3 temp_position= transform.position+new Vector3(16,0,0);
			Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/sword/sword_003_01/fire",typeof(Transform)) ,temp_position,Quaternion.identity );
			temp.GetComponent<magic_controller> ().attack_point = input_attack_point;
			temp.GetComponent<magic_controller>().state="up_left,1,1";
			temp.GetComponent<magic_controller> ().next_fire_position = new Vector3 (16, 0, 0);

			last_fire_time=Time.time;
		}
	}

	public void create_leaf(int input_level){

		long input_attack_point = 0;
		int input_attack_count_limit = 2; 
		
		if(input_level==1){
			input_attack_point = total_attack_point(); 
			input_attack_count_limit = 1; 
		}else if(input_level==2){
			input_attack_point = total_attack_point(); 
			input_attack_count_limit = 2; 
		}else if(input_level==3){
			input_attack_point = total_attack_point(); 
			input_attack_count_limit = 3; 
		}
		

		Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/sword/sword_004_01/leaf_creater",typeof(Transform)) ,new Vector3(0,0,0),Quaternion.identity );
		temp.GetComponent<magic_controller> ().attack_point = input_attack_point;
		temp.GetComponent<magic_controller> ().attack_count_limit = input_attack_count_limit;
	}



	public void check_rotation(){
		if (is_rotating) {
			rotation_by_sword=new Vector3(0,0,-rotation_speed*Time.deltaTime);
			transform.Rotate(rotation_by_sword);
		}
	}


	public void change_color(Color input_color){

		hero_color = input_color;
		transform.FindChild ("hero_body").GetComponent<SpriteRenderer>().material.color=input_color;
		transform.FindChild ("hero_arm_1").GetComponent<SpriteRenderer>().material.color=input_color;
		transform.FindChild ("hero_arm_2").GetComponent<SpriteRenderer>().material.color=input_color;
		transform.FindChild ("hero_hand_1").GetComponent<SpriteRenderer>().material.color=input_color;
		transform.FindChild ("hero_hand_2").GetComponent<SpriteRenderer>().material.color=input_color;


	}

	public void change_color_to_normal(){
		if(is_revive){
			change_color (new Color(1.0f,0.5f,1.0f));
		}else{
			change_color (new Color(1.0f,1.0f,1.0f));
		}
	}

	public void enable_rivive(){
		is_revive = true;
		change_color (new Color(1.0f,0.5f,1.0f));
	}

	public void disable_revive(){
		is_revive = false;
		change_color (new Color(1.0f,1.0f,1.0f));
	}

	public void unchi_hit(){
		if(save_data.get_sword_no(save_data.get_sword_equipped_slot())=="sword_001_01"){
			save_data.unchi_hit();
			initialize_sword();
		}
	}

	public void change_sword_scale(float input_scale){
		sword_scale = input_scale;
		if(sword!=null){
			sword.localScale= new Vector3(input_scale,input_scale,1.0f);
		}
	}

	/*
	public void start_freeze(float freeze_time){
		if(!is_freeze&&!is_paralyze&&!is_event&&!is_invincible&!is_invincible_by_sword){
		is_freeze = true;
		freeze_end_time = Time.time + freeze_time;
		rigidbody.velocity= new Vector3(0,0,0);
			if(transform.FindChild ("ice")==null){
				Transform temp = (Transform) Instantiate(( Transform) Resources.Load ("prefab/enemy/etc/ice",  typeof(Transform)) , transform.position,Quaternion.identity );
				temp.name="ice";
				temp.parent=transform;
				temp.GetComponent<ice_controller>().set_ice(1,2,transform);
			}
		}
	}
	*/
	/*
	private void  check_freeze(){
		if(is_freeze){
			if(Time.time >freeze_end_time){
				is_freeze=false;
				Destroy(transform.FindChild("ice").gameObject);

				damage_controller ("invincible_start");
			}
		}
	}
	*/
	/*
	public void start_paralyze(float paralyze_time){
		if(!is_freeze&&!is_paralyze&&!is_event&&!is_invincible&!is_invincible_by_sword){
			is_paralyze = true;
			paralyze_end_time = Time.time + paralyze_time;
			rigidbody.velocity= new Vector3(0,0,0);

			change_color (new Color(1.0f,1.0f,0.5f));
		}
	}
	
	private void  check_paralyze(){
		if(is_paralyze){
			if(Time.time >paralyze_end_time){
				is_paralyze=false;
				damage_controller ("invincible_start");
			}
		}
	}
	*/

	/*
	public void add_exp(int input_exp){
		exp_in_this_stage +=input_exp;
	}
*/

}
