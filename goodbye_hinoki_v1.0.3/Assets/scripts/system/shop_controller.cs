﻿using UnityEngine;
using System.Collections;

public class shop_controller : MonoBehaviour {

	public Transform item_box;
	public Transform[] item_box_array;


	//int[] item_box_position = { -51, -17, 17,51};
	// Use this for initialization
	void Start () {
		/*
		GameObject.Find ("controller").GetComponent<item_controller>().show_shop_item_list();

		//GameObject.Find ("text_mesh(Clone)").GetComponent<TextMesh> ().text = "\n\nはい　　　　　いいえ　　　　せつめい";
		 


		Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );

		temp.name = "window";
		temp.parent = GameObject.Find ("shop_UI").transform;

		window_controller window= temp.GetComponent<window_controller> ();
		
		window.show_window(8,3,0, 32 ,"shop_window",0);

		window.set_text_auto_position("shop_1","shop_text");

		//show_shop_window ();

		//GameObject.Find ("controller").GetComponent<item_controller>().show_item_list();
		*/
		//GameObject.Find ("controller").GetComponent<save_data_controller>().initialize();
		//GameObject.Find ("controller").GetComponent<save_data_controller>().AddMoney(20);
		show_shop_item_list ();
		GameObject.Find ("BGM").GetComponent<BGM_controller> ().set_play_BGM("shop");
		if (Application.platform == RuntimePlatform.Android) {
			Soomla.Store.SoomlaStore.StartIabServiceInBg();
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void show_shop_item_list(){

		GameObject.Find ("controller").GetComponent<item_data_controller>().show_shop_item_list();

		Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
		
		temp.name = "window";
		temp.parent = GameObject.Find ("shop_UI").transform;

		
		window_controller window= temp.GetComponent<window_controller> ();
		
		window.show_window(8,3, new Vector3(0, 32 ,0),"shop_window",0);
		
		window.set_text_auto_position("shop_1","shop_text",false);

		temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/money_UI",  typeof(Transform)) , new Vector3(40,80,0),Quaternion.identity );
		temp.name="money_UI";
		temp.parent = GameObject.Find ("shop_UI").transform;

	}
	
	public void show_shop_item_list_again(){
		
		GameObject.Find ("controller").GetComponent<item_data_controller>().show_shop_item_list();
		
		Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
		
		temp.name = "window";
		temp.parent = GameObject.Find ("shop_UI").transform;
		
		window_controller window= temp.GetComponent<window_controller> ();
		
		window.show_window(8,3,new Vector3(0, 32,0) ,"shop_window",0);
		
		window.set_text_auto_position("shop_2","shop_text",false);

		temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/money_UI",  typeof(Transform)) , new Vector3(40,80,0),Quaternion.identity );
		temp.name="money_UI";
		temp.parent = GameObject.Find ("shop_UI").transform;
	}

	public void show_shop_window(){
		
		GameObject temp_gameobject= new GameObject();
		temp_gameobject.name = "item_UI";
		
		Transform temp = (Transform) Instantiate(temp_gameobject.transform, new Vector3(0, 0, 0), Quaternion.identity);
		temp.parent = GameObject.Find ("UI").transform;
		
		item_box_array= new Transform[16];
		//item_box_controller child;



		
		for (int i=0; i<=3; i++) {
			for (int j=0; j<=1; j++) {
				/*
				int item_slot_no= i*4+j;
				
				item_box_array[item_slot_no] = (Transform) Instantiate(item_box, new Vector3(item_box_position[i], item_box_position[j], 0), Quaternion.identity);
				
				item_box_array[item_slot_no].parent= temp;
				
				child=item_box_array[item_slot_no].FindChild("item").GetComponent<item_box_controller>();
				
				child.item_slot_no=item_slot_no;
				
				child.type=PlayerPrefs.GetString("item_slot_"+item_slot_no+"_type");
				
				child.sword_type=PlayerPrefs.GetString("item_slot_"+item_slot_no+"_sword_type");
				
				
				child.render_item();
				
				*/
				
				//PlayerPrefs.GetInt("max_slot");
				
			}
		}
		
	}

	void OnDestroy() {
		if (Application.platform == RuntimePlatform.Android) {
			Soomla.Store.SoomlaStore.StartIabServiceInBg();
		}
	}


}
