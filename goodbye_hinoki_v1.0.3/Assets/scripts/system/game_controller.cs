﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class game_controller : MonoBehaviour {

	private Vector3 screen_point;
	//private Vector3 offset;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

			Dictionary<string,bool> controller_state = new Dictionary<string,bool>()
			{
				{"ButtonDown", false},
				{"dragged", false},
				{"ButtonUp", false}
			};

			Vector3 mouse_point=new Vector3(0,0,0);


			if (Input.GetMouseButtonDown (0)) {
				controller_state["ButtonDown"]=true;
				//offset=GameObject.Find("Main Camera").camera.ScreenToWorldPoint(Input.mousePosition);
			}

			if (Input.GetMouseButton(0)) {
				
				controller_state["dragged"]=true;
				mouse_point = GameObject.Find("Main Camera").camera.ScreenToWorldPoint(Input.mousePosition);
			}

			if (Input.GetMouseButtonUp(0)) {
				controller_state["ButtonUp"]=true;
				
			}



			for (int i = 0; i < Input.touchCount; i++) {
			
			// タッチ情報を取得する
				Touch touch = Input.GetTouch (i);

			//print("aa");
			//print (touch.phase);
			
			// ゲーム中ではなく、タッチ直後であればtrueを返す。
				if (touch.phase == TouchPhase.Began) {
				//this.screen_point = Camera.main.WorldToScreenPoint(transform.position);
				//print("touch"); 

				}else if (touch.phase == TouchPhase.Moved){
				//GameObject target = GameObject.FindWithTag("player");
				
				//float speed = 3;

				//target.rigidbody2D.velocity = new Vector3(Input.mousePosition.x-screen_point.x, 0, screen_point.z)*speed;


				}
			}



			if(controller_state["ButtonDown"]){
			//print ("ButtonDown");
			//this.screen_point = Camera.main.WorldToScreenPoint(transform.position);
			}

			if(controller_state["dragged"]){

			print ("aaaa");

				GameObject target = GameObject.Find("hero");

				float speed = 4.0f;


				//Vector3 speed_vector=new Vector3(mouse_point.x-offset.x,mouse_point.y-offset.y, 0);

			Vector3 speed_vector=new Vector3(mouse_point.x-target.transform.position.x,mouse_point.y-target.transform.position.y, 0);

				target.rigidbody2D.velocity = speed_vector*speed;
			
			}

			if(controller_state["ButtonUp"]){
			
				GameObject target = GameObject.Find("hero");

			//float speed =  0.0f;
			
				Vector3 speed_vector=new Vector3(0, 0, 0);

				target.rigidbody2D.velocity = speed_vector;

			}

	}
}
