﻿using UnityEngine;
using System.Collections;

public class about_item_UI_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//set_text ("",1);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void set_text(string input_type){

		transform.FindChild ("item").GetComponent<SpriteRenderer>().sprite= 
			(Sprite) Resources.Load("images/item/"+input_type, typeof(Sprite));

		transform.FindChild ("price").GetComponent<TextMesh> ().text =
			GameObject.Find ("controller").GetComponent<item_data_controller> ().item_price_text(input_type)+ "\n　　　　　　　　　";;


		transform.FindChild ("text_2").GetComponent<TextMesh> ().text =
			GameObject.Find ("controller").GetComponent<item_data_controller> ().item_name(input_type)
				+"\n\n"+
			GameObject.Find ("controller").GetComponent<item_data_controller> ().about_item(input_type);
			


	}
}
