﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class soomla_controller : MonoBehaviour {



	// Use this for initialization


		


	public static soomla_controller instance;

	public bool is_initailized = false;

	void Awake(){

		if(instance == null){ 	// making sure we only initialize one instance.
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			//initialize();
		} else {				// Destroying unused instances.
			GameObject.Destroy(gameObject);
		}
	}

	void initialize(){

	}

	void Start () {
		if (is_initailized) {
			Destroy(gameObject);
		}else{
			//sword_event= Instantiate(sword_event);
			Soomla.Store.SoomlaStore.Initialize( new Soomla.Store.soomla_sword_assets());
			Soomla.Store.StoreEvents.OnItemPurchased += onItemPurchased;
			is_initailized=true;

			//purchase_sword();
		}
	}
	
	// Update is called once per frame
	void Update () {

	

		//Soomla.Store.StoreInventory.BuyItem()
	
	}

	public void try_purchase_sword(){
		Soomla.Store.StoreInventory.BuyItem ("sword");

	}

	public void purchase_sword(){

		int rnd = Random.Range (2, 9);

		string sword_no="sword_00"+rnd+"_03";

		GameObject.Find ("controller").GetComponent<save_data_controller> ().add_sword (sword_no,10000);
	}

	public void onItemPurchased(Soomla.Store.PurchasableVirtualItem pvi, string payload) {

		purchase_sword ();

		//print ("ok");
	}

}

namespace Soomla.Store{

	public class soomla_sword_assets: IStoreAssets{
	

	public int GetVersion() {
		return 0;
	}
	
	/// <summary>
	/// see parent.
	/// </summary>
	public VirtualCurrency[] GetCurrencies() {
		return new VirtualCurrency[]{};
	}
	
	/// <summary>
	/// see parent.
	/// </summary>
	public VirtualGood[] GetGoods() {
		return new VirtualGood[] {soomla_sword};
	}
	
	/// <summary>
	/// see parent.
	/// </summary>
	public VirtualCurrencyPack[] GetCurrencyPacks() {
		return new VirtualCurrencyPack[] {};
	}
	
	/// <summary>
	/// see parent.
	/// </summary>
	public VirtualCategory[] GetCategories() {
		return new VirtualCategory[]{};
	}

	public const string sword_product_id = "sword";

	public static Soomla.Store.VirtualGood soomla_sword = new SingleUseVG(
		"sword",                              // name
		"Customers buy a double portion!",         // description
		"sword", 				 //id
		new PurchaseWithMarket(new MarketItem(
		sword_product_id,                     // product ID
		MarketItem.Consumable.NONCONSUMABLE,    // product type
		300))
	);   
	
	
	}
}






