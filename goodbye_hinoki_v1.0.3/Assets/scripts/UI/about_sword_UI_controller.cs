﻿using UnityEngine;
using System.Collections;

public class about_sword_UI_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//set_text ("",1);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void set_text(string input_sword_no,int power_up){
		transform.FindChild ("sword").GetComponent<SpriteRenderer>().sprite= (Sprite) Resources.Load("images/sword/"+ input_sword_no + "/"+ input_sword_no, typeof(Sprite));

		transform.FindChild ("attack_point").GetComponent<TextMesh> ().text =
			GameObject.Find ("controller").GetComponent<item_data_controller>().sword_attack_point(input_sword_no)+ "\n　　　　　　　　　";
		transform.FindChild ("power_up").GetComponent<TextMesh> ().text =power_up + "\n　　　　　　　　　";

		transform.FindChild ("text_2").GetComponent<TextMesh> ().text =
			GameObject.Find ("controller").GetComponent<item_data_controller> ().sword_name(input_sword_no)+"\n\n"+
			GameObject.Find ("controller").GetComponent<item_data_controller> ().about_sword (input_sword_no);


	}
}
