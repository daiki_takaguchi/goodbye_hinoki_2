﻿using UnityEngine;
using System.Collections;

public class SE_text_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
		refresh_text ();
	}

	public void refresh_text(){
		if (GameObject.Find ("SE").GetComponent<SE_controller> ().mute) {
			GetComponent<TextMesh>().text="SE OFF";
		} else {
			GetComponent<TextMesh>().text="SE ON";
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
