﻿using UnityEngine;
using System.Collections;

public class HP_count_controller : MonoBehaviour {

	// Use this for initialization
	public int HP;
	public int max_HP;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void hit(int input_HP){

	}

	public void set_max_HP(int input_max_HP){
		HP = input_max_HP;
		max_HP = input_max_HP;
		//transform.renderer.enabled = true;
		transform.GetComponent<text_mesh_controller> ().set_text (make_text());
	}

	public string make_text(){
		return "HP　" +HP+"/" +max_HP;
	}

	public void show(int input_HP,int input_max_HP){



		HP = input_HP;

		if(HP<0){
			HP=0;
		}

		max_HP = input_max_HP;
		//transform.renderer.enabled = true;
		transform.GetComponent<text_mesh_controller> ().set_text (make_text());
	}

}
