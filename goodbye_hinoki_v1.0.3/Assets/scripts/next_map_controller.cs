﻿using UnityEngine;
using System.Collections;

public class next_map_controller : MonoBehaviour {
	

	//public Sprite[] move_left_sprite;
	public Sprite[] move_right_sprite;
	//public Sprite[] move_right_sprite;

	public string move_left_state;
	public string move_right_state;
	public string move_up_state;



	

	//private Renderer move_left_renderer;
	//private Renderer move_right_renderer;

	private SpriteRenderer move_left_sprite_renderer;
	private SpriteRenderer move_right_sprite_renderer;
	private SpriteRenderer move_up_sprite_renderer;


	public float last_pressed_time;

	// Use this for initialization
	void Start () {
		//move_left_renderer = transform.FindChild ("move_left").GetComponent<Renderer> ();
		//move_right_renderer = transform.FindChild ("move_right").GetComponent<Renderer> ();


		move_right_sprite_renderer=transform.FindChild ("move_right").GetComponent<SpriteRenderer> ();
		move_left_sprite_renderer=transform.FindChild ("move_left").GetComponent<SpriteRenderer> ();
		move_up_sprite_renderer=transform.FindChild ("move_up").GetComponent<SpriteRenderer> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > last_pressed_time+0.2f) {
			move_left_sprite_renderer.sprite=null;
			move_right_sprite_renderer.sprite=null;
			move_up_sprite_renderer.sprite=null;

		}

	}

	public void  change_state(string target, string input_state){
		last_pressed_time = Time.time;
		SpriteRenderer temp_sprite_renderer =new SpriteRenderer();
		Sprite[] temp_sprite =new Sprite[0];
		string temp_state ="" ;

		if(target=="left"){
			temp_sprite_renderer = move_left_sprite_renderer;
			temp_sprite = move_right_sprite;
			temp_state =  move_left_state;
		}else if(target=="right"){
			temp_sprite_renderer = move_right_sprite_renderer;
			temp_sprite =  move_right_sprite;
			temp_state =  move_right_state;
		}else if(target=="up"){
			temp_sprite_renderer = move_up_sprite_renderer;
			temp_sprite =  move_right_sprite;
			temp_state =  move_up_state;
		}

		
		if (input_state != temp_state) {
			if(input_state=="pressed"){
				temp_sprite_renderer.sprite=temp_sprite[0];
			}else if(input_state=="selected"){
				temp_sprite_renderer.sprite=temp_sprite[1];
			}else if (input_state=="unpressed"){
				temp_sprite_renderer.sprite=null;
			}


		}
	}




}
