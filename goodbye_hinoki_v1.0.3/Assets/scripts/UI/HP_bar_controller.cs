﻿using UnityEngine;
using System.Collections;

public class HP_bar_controller : MonoBehaviour {

	// Use this for initialization



	void Start () {


	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void set_scale(float x, float y){
		transform.localScale = new Vector3 (x, y, transform.localScale.z);

	}

	public void show(float HP, float max_HP){
		Transform temp = transform.FindChild ("green");
		if (max_HP>0){
			if(HP<0){
				HP=0;
			}
			temp.localScale =new Vector3 (HP/max_HP,  temp.localScale.y, temp.localScale.z);
		}
	}

	public void set_layer(string layer){
		transform.FindChild ("red").renderer.sortingLayerName = layer;
		transform.FindChild ("green").renderer.sortingLayerName = layer;
	}
}
