﻿using UnityEngine;
using System.Collections;

public class UI_controller : MonoBehaviour {
	
	// Use this for initialization
	public string type="map";
	public string state="normal";
	public Vector3 button_pressed_position;
	public int next_map_length=100;
	public int map_shown=1;
	
	public int max_map_no=8;
	
	public float battle_equipment_button_press_start_time;
	public bool is_battle_equipment_button_long_pressed;

	public map_cube_controller map_cube;

	private int previous_slot;
	private string previous_item_type;
	private string previous_sword_no;


	private save_data_controller save_data;
	private item_data_controller item_data;


	
	void Start () {
		save_data = GameObject.Find ("controller").GetComponent<save_data_controller> ();
		item_data = GameObject.Find ("controller").GetComponent<item_data_controller> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void close_window(string input_state,Transform input_transform){
		
		
	}
	
	public void UI_OnMouseUpAsButton(UI_button_controller input, Transform input_transform){
		
		if(type=="battle"){
			
			
			if(state=="normal"){
				
				
				if(input.type=="stage_clear"){

					//StartCoroutine (GameObject.Find ("controller").GetComponent<battle_controller>().return_to_map());
					GameObject.Find ("controller").GetComponent<battle_controller>().return_to_map_pressed(1.0f);
				}else if(input.type=="battle_menu"){
						
						Destroy(GameObject.Find ("item_UI"));
						
						Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/battle_menu_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
						temp.name="battle_menu_UI";
						temp.parent=transform;
						state="battle_menu";
						
						Time.timeScale=0;
						
						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}
				
				
			}else if(state=="event"){

					
				if (input.type=="tutorial_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new  Vector3(0,-48,0), "window_2", 1.0f);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("tutorial_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
					
					
					
					
				}else if (input.type=="tutorial_2"){

					Destroy (GameObject.Find("tutorial_window"));

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();


					window.show_window(8,3,new  Vector3(0,-48,0),"window_2", 2.0f);

					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("tutorial_3",new Vector3(56,-16,0));


					temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_001/enemy_001_tutorial",  typeof(Transform)) ,new Vector3(100,30,0),Quaternion.identity );
					temp.name="enemy_001";
					temp.GetComponent<object_controller>().move_to_position(new Vector3(50,30,0),1.0f,0);
					
					
					
					temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_001/enemy_001_tutorial",  typeof(Transform)) ,new Vector3(90,0,0),Quaternion.identity );
					temp.name="enemy_002";
					temp.GetComponent<object_controller>().move_to_position(new Vector3(40,0,0),1.0f,0);
				
					
					temp = (Transform)Instantiate(( Transform) Resources.Load ("prefab/enemy/enemy_001/enemy_001_tutorial",  typeof(Transform)) ,new Vector3(100,-30,0),Quaternion.identity );
					temp.name="enemy_003";
					temp.GetComponent<object_controller>().move_to_position(new Vector3(50,-30,0),1.0f,0);

					Destroy (input.gameObject);

				}else if (input.type=="tutorial_3"){
				

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_4");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="tutorial_4"){

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_5");
					
					Destroy (input.gameObject);

				}else if (input.type=="tutorial_5"){


					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_6");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="tutorial_6"){

					Destroy (GameObject.Find("tutorial_window"));
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					
					window.show_window(8,3,new Vector3(0,-48,0),"window_2", 5.0f);
					
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("tutorial_7",new Vector3(56,-16,0));

					
					temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/etc/tutorial_sword",  typeof(Transform)) , new Vector3(0,200,0),Quaternion.identity );
					temp.rigidbody.velocity=new Vector3(0,-50,0);
					temp.name="tutorial_sword";
					
					Destroy (input.gameObject);

					GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("aura");

				}else if (input.type=="tutorial_7"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_8");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="tutorial_8"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_9");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="tutorial_9"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_10");
					
					Destroy (input.gameObject);

					
				}else if (input.type=="tutorial_10"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_11");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="tutorial_11"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_12");
					
					Destroy (input.gameObject);

				}else if (input.type=="tutorial_12"){

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_13");

					Destroy (input.gameObject);


				}else if (input.type=="tutorial_13"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_14");
					
					Destroy (input.gameObject);

				}else if (input.type=="tutorial_14"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_15");
					
					Destroy (input.gameObject);

				}else if (input.type=="tutorial_15"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_16");
					
					Destroy (input.gameObject);

				}else if (input.type=="tutorial_16"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_17");
					
					Destroy (input.gameObject);
				
				}else if (input.type=="tutorial_17"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_18");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_18"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_19");
					
					Destroy (input.gameObject);

				}else if (input.type=="tutorial_19"){

					Destroy (GameObject.Find("tutorial_window"));

					GameObject.Find ("hero").GetComponent<hero_controller>().disable_is_event();
					Destroy (input.gameObject);

					state="normal";

					GameObject.Find ("arrows").AddComponent<arrows_tutorial_controller>();
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/etc/tutorial_arrow",  typeof(Transform)) , 
					            GameObject.Find ("arrows").transform.position+new Vector3(0,32,0),Quaternion.identity );

					temp.name="tutorial_arrow";
					temp.parent=	GameObject.Find ("arrows").transform;
				}else if (input.type=="tutorial_clear_1"){


					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2",  1.0f);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("tutorial_clear_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
					
				}else if (input.type=="tutorial_dead_1"){

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2", 1.0f);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("tutorial_clear_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
					
				}else if (input.type=="tutorial_run_2"){

					state="normal";
					Destroy (GameObject.Find("tutorial_window"));
					Destroy (input.gameObject);


				}else if (input.type=="tutorial_clear_2"){

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_3");
					
					Destroy (input.gameObject);


					
				}else if (input.type=="tutorial_clear_3"){

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_4");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="tutorial_clear_4"){

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_5");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="tutorial_clear_5"){

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_6");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_clear_6"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_7");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_clear_7"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_8");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_clear_8"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_9");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_clear_9"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_10");
					
					Destroy (input.gameObject);

				}else if (input.type=="tutorial_clear_10"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_11");


					Destroy (input.gameObject);

				}else if (input.type=="tutorial_clear_11"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_12");

					Destroy (input.gameObject);
				
				}else if (input.type=="tutorial_clear_12"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_13");
					
					Destroy (input.gameObject);

				}else if (input.type=="tutorial_clear_13"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_clear_14");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_clear_14"){

				

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.destroy_next_text_button();

					GameObject.Find ("controller").GetComponent<battle_controller>().return_to_map_pressed(1.0f);
				
					Destroy (input.gameObject);




				}else if (input.type=="map_001_talk_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,-48,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("map_001_talk_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);

				}else if (input.type=="map_001_talk_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_001_talk_3");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_001_talk_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_001_talk_4");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_001_talk_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_001_talk_5");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_001_talk_5"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_001_talk_6");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_001_talk_6"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_001_talk_7");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_001_talk_7"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_001_talk_8");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="map_001_talk_8"){
					
					
					Destroy (GameObject.Find("tutorial_window"));
					
					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();

					disable_stop_enemies();

					state="normal";
					Destroy (input.gameObject);
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				}else if (input.type=="unchi_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();

					window.show_window(8,3,new Vector3(0,-48,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("unchi_2",new Vector3(56,-16,0));

					Destroy (input.gameObject);
				
				
				}else if (input.type=="unchi_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("unchi_3");

					Destroy (input.gameObject);
				}else if (input.type=="unchi_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("unchi_4");

					Destroy (input.gameObject);

				}else if (input.type=="unchi_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("unchi_5");

					Destroy (input.gameObject);
				}else if (input.type=="unchi_5"){

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/etc/tutorial_left_button",  typeof(Transform)) ,GameObject.Find ("button_left").transform.position+ new Vector3(0,24,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_left_button";

					Destroy (GameObject.Find ("tutorial_window"));
					Destroy (input.gameObject);

					state="wait_left_button";




				}else if (input.type=="unchi_end_dirty_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("unchi_end_dirty_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);

				}else if (input.type=="unchi_end_dirty_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("unchi_end_dirty_3");
					
					Destroy (input.gameObject);

				}else if (input.type=="unchi_end_dirty_3"){

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.destroy_next_text_button();
					GameObject.Find ("controller").GetComponent<battle_controller>().return_to_map_pressed(1.0f);

					Destroy (input.gameObject);

				}else if (input.type=="unchi_end_clean_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("unchi_end_clean_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
					
				}else if (input.type=="unchi_end_clean_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("unchi_end_clean_3");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="unchi_end_clean_3"){


					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.destroy_next_text_button();
					GameObject.Find ("controller").GetComponent<battle_controller>().return_to_map_pressed(1.0f);

					Destroy (input.gameObject);
				
				}else if (input.type=="tutorial_sword_1"){

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,-48,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("tutorial_sword_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_sword_2"){

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_sword_3");
					
					Destroy (input.gameObject);

				}else if (input.type=="tutorial_sword_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_sword_4");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_sword_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_sword_5");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_sword_5"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_sword_6");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_sword_6"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_sword_7");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_sword_7"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_sword_8");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_sword_8"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_sword_9");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_sword_9"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_sword_10");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_sword_10"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_sword_11");
					
					Destroy (input.gameObject);
				}else if (input.type=="tutorial_sword_11"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("tutorial_sword_12");
					
					Destroy (input.gameObject);

				}else if (input.type=="tutorial_sword_12"){


					Destroy (GameObject.Find("tutorial_window"));

					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();

					disable_stop_enemies();

					state="normal";
					Destroy (input.gameObject);
				
				}else if (input.type=="shop_help_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("shop_help_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
					
				}else if (input.type=="shop_help_2"){
					
					Destroy (GameObject.Find("tutorial_window"));
					
					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();

					state="normal";
					Destroy (input.gameObject);

				}else if (input.type=="shop_help_end_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("shop_help_end_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);

				}else if (input.type=="shop_help_end_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("shop_help_end_3");
					
					Destroy (input.gameObject);

				}else if (input.type=="shop_help_end_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("shop_help_end_4");
					
					Destroy (input.gameObject);

				}else if (input.type=="shop_help_end_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("shop_help_end_5");
					
					Destroy (input.gameObject);

				}else if (input.type=="shop_help_end_5"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("shop_help_end_6");
					
					Destroy (input.gameObject);


				}else if (input.type=="shop_help_end_6"){
	
					Destroy (GameObject.Find("tutorial_window"));

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2", 2.5f);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("shop_help_end_7",new Vector3(56,-16,0));

					GameObject.Find ("shopper_body").rigidbody.velocity=new Vector3(16,0,0);

					Destroy (input.gameObject);


				
				}else if (input.type=="shop_help_end_7"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("shop_help_end_8");
					
					Destroy (input.gameObject);

					Destroy(GameObject.Find ("shopper_body"));

				}else if (input.type=="shop_help_end_8"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("shop_help_end_9");
					Destroy (input.gameObject);

				}else if (input.type=="shop_help_end_9"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("shop_help_end_10");

					Destroy (input.gameObject);

				}else if (input.type=="shop_help_end_10"){

					//Destroy (GameObject.Find("tutorial_window"));

					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.destroy_next_text_button();

					GameObject.Find ("controller").GetComponent<battle_controller>().return_to_map_pressed(1.0f);

					Destroy (input.gameObject);





			

				}else if (input.type=="last_boss_at_stage_17_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,-48,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("last_boss_at_stage_17_2",new Vector3(56,-16,0));


					
					Destroy (input.gameObject);
				}else if (input.type=="last_boss_at_stage_17_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("last_boss_at_stage_17_3");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="last_boss_at_stage_17_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("last_boss_at_stage_17_4");
					
					Destroy (input.gameObject);
				}else if (input.type=="last_boss_at_stage_17_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("last_boss_at_stage_17_5");
					
					Destroy (input.gameObject);

				}else if (input.type=="last_boss_at_stage_17_5"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("last_boss_at_stage_17_6");
					
					Destroy (input.gameObject);

					
				}else if (input.type=="last_boss_at_stage_17_6"){
					
					
					Destroy (GameObject.Find("tutorial_window"));
					
					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();
					
					disable_stop_enemies();
					
					state="normal";
					Destroy (input.gameObject);
				




				}else if (input.type=="map_005_talk_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,-48,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("map_005_talk_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
					
				}else if (input.type=="map_005_talk_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_005_talk_3");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="map_005_talk_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_005_talk_4");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="map_005_talk_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_005_talk_5");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_005_talk_5"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_005_talk_6");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_005_talk_6"){
					
					
					Destroy (GameObject.Find("tutorial_window"));
					
					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();
					
					disable_stop_enemies();

					state="normal";
					Destroy (input.gameObject);







				}else if (input.type=="map_006_talk_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,-48,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("map_006_talk_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
			
				
				}else if (input.type=="map_006_talk_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_3");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_006_talk_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_4");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_006_talk_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_5");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_006_talk_5"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_6");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_006_talk_6"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_7");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_006_talk_7"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_8");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_006_talk_8"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_9");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_006_talk_9"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_10");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_006_talk_10"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_11");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_006_talk_11"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_12");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_006_talk_12"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_006_talk_13");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_006_talk_13"){
					
					
					Destroy (GameObject.Find("tutorial_window"));
					
					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();
					
					disable_stop_enemies();
					
					state="normal";
					Destroy (input.gameObject);









					
				}else if (input.type=="map_007_talk_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,-48,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("map_007_talk_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
					
					
				}else if (input.type=="map_007_talk_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_007_talk_3");
					
					Destroy (input.gameObject);


				}else if (input.type=="map_007_talk_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_007_talk_4");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_007_talk_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_007_talk_5");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_007_talk_5"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_007_talk_6");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_007_talk_6"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_007_talk_7");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_007_talk_7"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_007_talk_8");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_007_talk_8"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_007_talk_9");
					
					Destroy (input.gameObject);
					
				}else if (input.type=="map_007_talk_9"){
					
					
					Destroy (GameObject.Find("tutorial_window"));
					
					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();
					
					disable_stop_enemies();
					
					state="normal";
					Destroy (input.gameObject);




				}else if (input.type=="map_008_talk_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,-48,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("map_008_talk_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
					
					
				}else if (input.type=="map_008_talk_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_3");
					
					Destroy (input.gameObject);


				}else if (input.type=="map_008_talk_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_4");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_008_talk_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_5");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_008_talk_5"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_6");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_008_talk_6"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_7");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_008_talk_7"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_8");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_008_talk_8"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_9");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_008_talk_9"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_10");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_008_talk_10"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_11");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_008_talk_11"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_12");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_008_talk_12"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_13");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_008_talk_13"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_14");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_008_talk_14"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_15");
					
					Destroy (input.gameObject);
				}else if (input.type=="map_008_talk_15"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("map_008_talk_16");
					
					Destroy (input.gameObject);

				}else if (input.type=="map_008_talk_16"){
					
					
					Destroy (GameObject.Find("tutorial_window"));
					
					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();
					
					disable_stop_enemies();
					
					state="normal";
					Destroy (input.gameObject);

				}else if (input.type=="boss_008_end_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("boss_008_end_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);

				}else if (input.type=="boss_008_end_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("boss_008_end_3");
					
					Destroy (input.gameObject);
				}else if (input.type=="boss_008_end_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("boss_008_end_4");
					
					Destroy (input.gameObject);

				}else if (input.type=="boss_008_end_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("boss_008_end_5");
					
					Destroy (input.gameObject);

				}else if (input.type=="boss_008_end_5"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("boss_008_end_6");
					
					Destroy (input.gameObject);
				}else if (input.type=="boss_008_end_6"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.destroy_next_text_button();
					
					GameObject.Find ("controller").GetComponent<battle_controller>().return_to_map_pressed(1.0f);

					Destroy (input.gameObject);







				}else if (input.type=="boss_009_start_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,-48,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("boss_009_start_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);

				}else if (input.type=="boss_009_start_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("boss_009_start_3");
					
					Destroy (input.gameObject);
				}else if (input.type=="boss_009_start_3"){
					
					Destroy (GameObject.Find("tutorial_window"));
					
					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();

					disable_stop_enemies();

					state="normal";
					Destroy (input.gameObject);
					
				}else if (input.type=="boss_009_run_2"){
					
					state="normal";
					Destroy (GameObject.Find("tutorial_window"));
					Destroy (input.gameObject);


				}else if (input.type=="hinoki_used_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("hinoki_used_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);
					
				}else if (input.type=="hinoki_used_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("hinoki_used_3");

					Destroy (input.gameObject);
				}else if (input.type=="hinoki_used_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("hinoki_used_4");
					Destroy (input.gameObject);
				}else if (input.type=="hinoki_used_4"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("hinoki_used_5");
					Destroy (input.gameObject);
				}else if (input.type=="hinoki_used_5"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("hinoki_used_6");
					Destroy (input.gameObject);
				}else if (input.type=="hinoki_used_6"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("hinoki_used_7");
					Destroy (input.gameObject);
				}else if (input.type=="hinoki_used_7"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("hinoki_used_8");
					Destroy (input.gameObject);
				}else if (input.type=="hinoki_used_8"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("hinoki_used_9");
					Destroy (input.gameObject);
				}else if (input.type=="hinoki_used_9"){
					

					Destroy (GameObject.Find("tutorial_window"));
					
					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();
					
					disable_stop_enemies();

					GameObject.Find ("hero").GetComponent <hero_controller>().is_using_sword=false;
					GameObject.Find ("hero").GetComponent <hero_controller>().initialize_sword();
					GameObject.Find ("hero").GetComponent <hero_controller>().is_using_sword=true;
					
					state="normal";
					Destroy (input.gameObject);

				}else if (input.type=="boss_009_end_1"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("boss_009_end_2",new Vector3(56,-16,0));
					
					Destroy (input.gameObject);

				}else if (input.type=="boss_009_end_2"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("boss_009_end_3");
					Destroy (input.gameObject);
				}else if (input.type=="boss_009_end_3"){
					
					window_controller window= GameObject.Find ("tutorial_window").GetComponent<window_controller> ();
					window.change_text_auto_position(input.type);
					window.change_next_text_button("boss_009_end_4");
					Destroy (input.gameObject);
				}else if (input.type=="boss_009_end_4"){
					Destroy(GameObject.Find ("tutorial_window"));
					Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/end_credit_UI",  typeof(Transform)) );
					Destroy (input.gameObject);

				}else if (input.type=="boss_009_end_5"){
				
				}else if (input.type=="boss_009_end_6"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("UI").transform;
					temp.name="tutorial_window";
					window_controller window= temp.GetComponent<window_controller> ();
					
					window.show_window(8,3,new Vector3(0,0,0), "window_2", 0);
					window.set_text_auto_position(input.type,"text_2",true);
					window.add_next_text_button("boss_009_end_7",new Vector3(56,-16,0));

				}else if (input.type=="boss_009_end_7"){

					Destroy (GameObject.Find("tutorial_window"));
					
					GameObject.Find ("controller").GetComponent<battle_controller>().return_to_map_pressed(1.0f);
					
					state="boss_009_end_8";
					Destroy (input.gameObject);
				}









				
			}else if(state=="wait_left_button"){
				if(input.type=="show_item_list"){
					Destroy (GameObject.Find ("tutorial_left_button"));

					GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();

					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					Time.timeScale=0;
					state="show_item_list";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");



				}
		
				




				
				
				
				
				
				
				
				
				
				
			}else if(state=="show_item_list"){

				if (input.type=="item_box"){

					item_selected(input);

				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI").gameObject);
					Time.timeScale=1.0f;
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="battle_menu"){
					
					Destroy(GameObject.Find ("item_UI").gameObject);
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/battle_menu_UI",  typeof(Transform)) , new Vector3(0,0,-0.6f),Quaternion.identity );
					temp.name="battle_menu_UI";
					temp.parent=transform;
					state="battle_menu";
					
					Time.timeScale=0;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}

				
			}else if(state=="item_selected"){
				
				if(input.type=="equipment_yes"){

					save_data.SetInt("sword_equipped", previous_slot);

					Destroy(input_transform.parent.parent.gameObject);
					
					GameObject.Find ("hero").GetComponent<hero_controller>().initialize_sword();
					GameObject.Find ("controller").GetComponent<item_data_controller>().refresh_equipment();
					Destroy (GameObject.Find ("item_box_selected"));
					
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="use_item"){
					
					Destroy(GameObject.Find ("item_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list_for_use(previous_item_type,previous_slot);
					state="use_item";

				}else if(input.type=="about"){
					
					if(previous_item_type=="sword"){
						
						Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/about_sword_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
						temp.name="about_UI";
						temp.parent=GameObject.Find ("item_UI").transform;
						temp.GetComponent<about_sword_UI_controller>().set_text(
							previous_sword_no,save_data.get_power_up(previous_slot));

						state="about";

						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

					}else if(save_data.is_item (previous_item_type)){
						
						Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/about_item_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
						temp.name="about_UI";
						temp.parent=GameObject.Find ("item_UI").transform;
						temp.GetComponent<about_item_UI_controller>().set_text (previous_item_type);
						
						state="about";
						
						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
						
					}
					
				}else if(input.type=="replace"){
					
					Destroy (GameObject.Find ("item_selected_UI"));
					state="replace";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
					
				}else if (input.type=="close_item_selected_UI"){
					
					Destroy (GameObject.Find ("item_selected_UI"));
					Destroy (GameObject.Find ("item_box_selected"));
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI").gameObject);
					Time.timeScale=1.0f;
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="battle_menu"){
					
					Destroy(GameObject.Find ("item_UI").gameObject);
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/battle_menu_UI",  typeof(Transform)) , new Vector3(0,0,-0.6f),Quaternion.identity );
					temp.name="battle_menu_UI";
					temp.parent=transform;
					state="battle_menu";
					
					Time.timeScale=0;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}

			}else if(state=="use_item"){

				if (input.type=="item_box_for_use"){

					use_item (input);


				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					state="normal";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}

			}else if(state=="replace"){
				if (input.type=="item_box"){

					replace_item(input);
					
				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="battle_menu"){
					
					Destroy(GameObject.Find ("item_UI").gameObject);
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/battle_menu_UI",  typeof(Transform)) , new Vector3(0,0,-0.6f),Quaternion.identity );
					temp.name="battle_menu_UI";
					temp.parent=transform;
					state="battle_menu";
					
					Time.timeScale=0;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}
				
			}else if(state=="about"){
				
				if (input.type=="close_about_UI"){
					
					Destroy (GameObject.Find ("about_UI"));
					state="item_selected";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					state="normal";
					Time.timeScale=1.0f;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="battle_menu"){
					
					Destroy(GameObject.Find ("item_UI").gameObject);
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/battle_menu_UI",  typeof(Transform)) , new Vector3(0,0,-0.6f),Quaternion.identity );
					temp.name="battle_menu_UI";
					temp.parent=transform;
					state="battle_menu";
					
					Time.timeScale=0;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}
				
				
				


			}else if(state=="battle_menu"){


				if(input.type=="battle_menu"){
					
					Destroy(GameObject.Find ("battle_menu_UI"));
					state="normal";
					
					Time.timeScale=1.0f;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="show_item_list"){

				
					
				}else if(input.type=="about_hero"){

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/about_hero_UI",  typeof(Transform)) , new Vector3(0,0,-0.61f),Quaternion.identity );
					temp.name="about_hero_UI";
					temp.parent=GameObject.Find ("battle_menu_UI").transform;
					state="about_hero";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="audio"){

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/audio_UI",  typeof(Transform)) , new Vector3(0,0,-0.61f),Quaternion.identity );
					temp.name="audio_UI";
					temp.parent=GameObject.Find ("battle_menu_UI").transform;
					state="audio";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="share"){

					if (Application.platform == RuntimePlatform.Android){
						SocialConnector.Share("#さようなら、ひのきくん", "http://goo.gl/nzxJ6D", null);
					}else if(Application.platform == RuntimePlatform.IPhonePlayer){
						SocialConnector.Share("#さようなら、ひのきくん", "http://goo.gl/flga1Z", null);
					}
				}else if(input.type=="review"){
					
					
					if (Application.platform == RuntimePlatform.Android){
						Application.OpenURL("https://play.google.com/store/apps/details?id=jp.stimuli.goodbyehinoki&hl=ja");
					}else if(Application.platform == RuntimePlatform.IPhonePlayer){
						Application.OpenURL("https://itunes.apple.com/us/app/sayounara-hinokikun/id944515515?l=ja&ls=1&mt=8");
					}
					

				}else if(input.type=="title_back"){

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/title_back_UI",  typeof(Transform)) , new Vector3(0,0,-0.61f),Quaternion.identity );
					temp.name="title_back_UI";
					temp.parent=GameObject.Find ("battle_menu_UI").transform;
					state="title_back";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");


				}else if(input.type=="run"){


					if(GameObject.Find ("controller").GetComponent<battle_controller>().stage_no==0){

						Destroy(GameObject.Find ("battle_menu_UI"));
						Time.timeScale=1.0f;

						Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
						temp.parent=GameObject.Find ("UI").transform;
						temp.name="tutorial_window";
						window_controller window= temp.GetComponent<window_controller> ();
						
						window.show_window(8,3,new Vector3(0,0,0), "window_2", 0.0f);
						window.set_text_auto_position("tutorial_run_1","text_2",false);
						window.add_next_text_button("tutorial_run_2",new Vector3(56,-16,0));

						state="event";

					}else if(GameObject.Find ("controller").GetComponent<battle_controller>().stage_no==-4){
						Destroy(GameObject.Find ("battle_menu_UI"));
						Time.timeScale=1.0f;
						
						Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
						temp.parent=GameObject.Find ("UI").transform;
						temp.name="tutorial_window";
						window_controller window= temp.GetComponent<window_controller> ();
						
						window.show_window(8,3,new Vector3(0,0,0), "window_2", 0.0f);
						window.set_text_auto_position("boss_009_run_1","text_2",false);
						window.add_next_text_button("boss_009_run_2",new Vector3(56,-16,0));
						
						state="event";

					}else{

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/run_confirm_UI",  typeof(Transform)) , new Vector3(0,0,-0.61f),Quaternion.identity );
						temp.name="run_confirm_UI";
						temp.parent=GameObject.Find ("battle_menu_UI").transform;
						state="run_confirm";

						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					}

				}
				
			}else if(state=="about_hero"){
				if (input.type=="close_about_hero_UI"){
					
					Destroy (GameObject.Find ("about_hero_UI"));
					state="battle_menu";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("battle_menu_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					state="show_item_list";
					
					Time.timeScale=0;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="battle_menu"){
					
					Destroy(GameObject.Find ("battle_menu_UI"));
					state="normal";
					
					Time.timeScale=1.0f;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}
				
				
			}else if(state=="audio"){
				if(input.type=="BGM_button"){

					GameObject.Find ("BGM").GetComponent<BGM_controller>().switch_mute();
					GameObject.Find ("BGM_text").GetComponent<BGM_text_controller>().refresh_text();

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="SE_button"){

					GameObject.Find ("SE").GetComponent<SE_controller>().switch_mute();
					GameObject.Find ("SE_text").GetComponent<SE_text_controller>().refresh_text();
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="audio_back"){

					Destroy(GameObject.Find ("audio_UI"));
					state="battle_menu";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");




				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("battle_menu_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					state="show_item_list";
					
					Time.timeScale=0;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="battle_menu"){
					
					Destroy(GameObject.Find ("battle_menu_UI"));
					state="normal";
					
					Time.timeScale=1.0f;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}
				
			}else if(state=="title_back"){

				if(input.type=="title_back_yes"){
					
					Time.timeScale=1.0f;
					if(GameObject.Find ("hero").GetComponent<hero_controller>().is_using_sword){
						save_data.sword_use_end ();
					}
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

					FadeManager.Instance.LoadLevel("title", 0.5f);
					state="title_start_waiting";
					
				}else if(input.type=="title_back_no"){
					
					Destroy(GameObject.Find ("title_back_UI"));
					state="battle_menu";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}
				
			}else if(state=="run_confirm"){

				if(input.type=="run_yes"){

					Time.timeScale=1.0f;
					if(GameObject.Find ("hero").GetComponent<hero_controller>().is_using_sword){
						save_data.sword_use_end ();
					}

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

					GameObject.Find ("controller").GetComponent<battle_controller>().return_to_map_pressed(0.0f);
					state="map_start_waiting";

				}else if(input.type=="run_no"){

					Destroy(GameObject.Find ("run_confirm_UI"));
					state="battle_menu";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				
				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("battle_menu_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					state="show_item_list";
					
					Time.timeScale=0;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="battle_menu"){
					
					Destroy(GameObject.Find ("battle_menu_UI"));
					state="normal";
					
					Time.timeScale=1.0f;

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}

		}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		}else if (type=="map"){
			if(input.type=="shop"){
				if(state!="shop_start_waiting"){
					FadeManager.Instance.LoadLevel("shop", 1.0f);
					save_data.SetInt ("shop_stage_no", map_shown*4-3);
					GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("shop");
					state="shop_start_waiting";
				}

			}else if(state == "normal") {
				
				if(input.type=="show_item_list"){
					
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="stage_select_button"){;

					save_data.SetInt ("stage_no", input.stage_no);
					save_data.SetInt("stage_reached",input.stage_no);
					FadeManager.Instance.LoadLevel("battle", 1.0f);
					
					GameObject.Find ("SE").GetComponent<SE_controller>().play_audio("start");

					state="battle_start_waitng";
					
				}else if(input.type=="map_rotate_right"){
					
					transform.FindChild ("map_cube").GetComponent<map_cube_controller>().start_rotation (input.type,1);
					state="map_rotate_right";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="menu"){
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/menu_UI",  typeof(Transform)) , new Vector3(0,0,-0.6f),Quaternion.identity );
					temp.name="menu_UI";
					temp.parent=transform;
					state="menu";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="map_hinoki"){

					show_hinoki_comment(input);
					state="hinoki_comment";
				}
				
				
				
			}else if(state=="show_item_list"){
				
				if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="menu"){

					Destroy(GameObject.Find ("item_UI"));
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/menu_UI",  typeof(Transform)) , new Vector3(0,0,-0.6f),Quaternion.identity );
					temp.name="menu_UI";
					temp.parent=transform;
					state="menu";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				
				}else if(input.type=="map_hinoki"){

					Destroy(GameObject.Find ("item_UI"));

					show_hinoki_comment(input);
					state="hinoki_comment";

				} else if (input.type=="item_box"){
					item_selected(input);

				}
				

				
			}else if(state=="item_selected"){
				
				if(input.type=="equipment_yes"){
				
					save_data.SetInt("sword_equipped", previous_slot);
					//print(input.item_slot_no);
					Destroy(input_transform.parent.parent.gameObject);
					
					GameObject.Find ("controller").GetComponent<item_data_controller>().refresh_equipment();
					Destroy (GameObject.Find ("item_box_selected"));
					
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="use_item"){
					
					Destroy(GameObject.Find ("item_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list_for_use(previous_item_type,previous_slot);
					state="use_item";

				}else if(input.type=="about"){
					
					if(previous_item_type=="sword"){
						
						Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/about_sword_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
						temp.name="about_UI";
						temp.parent=GameObject.Find ("item_UI").transform;
						temp.GetComponent<about_sword_UI_controller>().set_text(
							previous_sword_no,save_data.get_power_up(previous_slot));
						
						state="about";

						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

					}else if(save_data.is_item (previous_item_type)){
						
						Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/about_item_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
						temp.name="about_UI";
						temp.parent=GameObject.Find ("item_UI").transform;
						temp.GetComponent<about_item_UI_controller>().set_text (previous_item_type);
						
						state="about";
						
						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
						
					}
					
				}else if(input.type=="replace"){
					
					Destroy (GameObject.Find ("item_selected_UI"));
					state="replace";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if (input.type=="close_item_selected_UI"){
					
					Destroy (GameObject.Find ("item_selected_UI"));
					Destroy (GameObject.Find ("item_box_selected"));
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="menu"){

					Destroy(GameObject.Find ("item_UI"));
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/menu_UI",  typeof(Transform)) , new Vector3(0,0,-0.6f),Quaternion.identity );
					temp.name="menu_UI";
					temp.parent=transform;
					state="menu";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="map_hinoki"){
					
					Destroy(GameObject.Find ("item_UI"));
					
					show_hinoki_comment(input);
					state="hinoki_comment";

				}

			}else if(state=="use_item"){
				if (input.type=="item_box_for_use"){

					use_item (input);
				
				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					state="normal";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="map_hinoki"){
					
					Destroy(GameObject.Find ("item_UI"));
					
					show_hinoki_comment(input);
					state="hinoki_comment";

					
				}

			}else if(state=="replace"){

				if (input.type=="item_box"){

					replace_item(input);

				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				

				}else if(input.type=="menu"){

					Destroy(GameObject.Find ("item_UI"));
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/menu_UI",  typeof(Transform)) , new Vector3(0,0,-0.6f),Quaternion.identity );
					temp.name="menu_UI";
					temp.parent=transform;
					state="menu";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="map_hinoki"){
					
					Destroy(GameObject.Find ("item_UI"));
					
					show_hinoki_comment(input);
					state="hinoki_comment";

				}
				
			}else if(state=="about"){
				
				if (input.type=="close_about_UI"){
					
					Destroy (GameObject.Find ("about_UI"));
					state="item_selected";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="menu"){

					Destroy(GameObject.Find ("item_UI"));
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/menu_UI",  typeof(Transform)) , new Vector3(0,0,-0.6f),Quaternion.identity );
					temp.name="menu_UI";
					temp.parent=transform;
					state="menu";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="map_hinoki"){
					
					Destroy(GameObject.Find ("item_UI"));
					
					show_hinoki_comment(input);
					state="hinoki_comment";

				}
				
				
				
				
				
				
				
			}else if(state=="menu"){

				if(input.type=="menu"){
					
					Destroy(GameObject.Find ("menu_UI"));
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="show_item_list"){

					Destroy(GameObject.Find ("menu_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="about_hero"){

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/about_hero_UI",  typeof(Transform)) , new Vector3(0,0,-0.61f),Quaternion.identity );
					temp.name="about_hero_UI";
					temp.parent=GameObject.Find ("menu_UI").transform;
					state="about_hero";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="audio"){
					
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/audio_UI",  typeof(Transform)) , new Vector3(0,0,-0.61f),Quaternion.identity );
					temp.name="audio_UI";
					temp.parent=GameObject.Find ("menu_UI").transform;
					state="audio";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				
				

				}else if(input.type=="share"){
					
					if (Application.platform == RuntimePlatform.Android){
						SocialConnector.Share("#さようなら、ひのきくん", "http://goo.gl/nzxJ6D", null);
					}else if(Application.platform == RuntimePlatform.IPhonePlayer){
						SocialConnector.Share("#さようなら、ひのきくん", "http://goo.gl/flga1Z", null);
					}
				}else if(input.type=="review"){
					
					
					if (Application.platform == RuntimePlatform.Android){
						Application.OpenURL("https://play.google.com/store/apps/details?id=jp.stimuli.goodbyehinoki&hl=ja");
					}else if(Application.platform == RuntimePlatform.IPhonePlayer){
						Application.OpenURL("https://itunes.apple.com/us/app/sayounara-hinokikun/id944515515?l=ja&ls=1&mt=8");
					}

				}else if(input.type=="title_back"){
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/title_back_UI",  typeof(Transform)) , new Vector3(0,0,-0.61f),Quaternion.identity );
					temp.name="title_back_UI";
					temp.parent=GameObject.Find ("menu_UI").transform;
					state="title_back";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="close_menu"){

					Destroy(GameObject.Find ("menu_UI"));
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="map_hinoki"){
					
					Destroy(GameObject.Find ("menu_UI"));
					
					show_hinoki_comment(input);
					state="hinoki_comment";

					
				}

			}else if(state=="about_hero"){

				if (input.type=="close_about_hero_UI"){

					Destroy (GameObject.Find ("about_hero_UI"));
					state="menu";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="show_item_list"){

					Destroy(GameObject.Find ("menu_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}else if(input.type=="menu"){
						
					Destroy(GameObject.Find ("menu_UI"));
					state="normal";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}else if(input.type=="map_hinoki"){
					
					Destroy(GameObject.Find ("menu_UI"));
					
					show_hinoki_comment(input);
					state="hinoki_comment";

				
				}
				
				
			}else if(state=="audio"){
				if(input.type=="BGM_button"){

					GameObject.Find ("BGM").GetComponent<BGM_controller>().switch_mute();
					GameObject.Find ("BGM_text").GetComponent<BGM_text_controller>().refresh_text();

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="SE_button"){

					GameObject.Find ("SE").GetComponent<SE_controller>().switch_mute();
					GameObject.Find ("SE_text").GetComponent<SE_text_controller>().refresh_text();
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="audio_back"){

					Destroy(GameObject.Find ("audio_UI"));
					state="menu";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="show_item_list"){

					Destroy(GameObject.Find ("menu_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}else if(input.type=="menu"){
					
					Destroy(GameObject.Find ("menu_UI"));
					state="normal";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="map_hinoki"){
					
					Destroy(GameObject.Find ("menu_UI"));
					
					show_hinoki_comment(input);
					state="hinoki_comment";


				}

			}else if(state=="title_back"){
				
				if(input.type=="title_back_yes"){
					/*
					Time.timeScale=1.0f;
					if(GameObject.Find ("hero").GetComponent<hero_controller>().is_using_sword){
						save_data.sword_use_end ();
					}*/
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
					FadeManager.Instance.LoadLevel("title", 0.5f);
					state="title_start_waiting";
					
				}else if(input.type=="title_back_no"){
					
					Destroy(GameObject.Find ("title_back_UI"));
					state="menu";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}
				
			}else if(state=="hinoki_comment"){
				
				if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("hinoki_comment_window"));
					GameObject.Find ("map_hinoki").GetComponent<UI_button_controller>().disable_detect_desplay_pressed();

					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					state="show_item_list";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="menu"){
					

					Destroy(GameObject.Find ("hinoki_comment_window"));
					GameObject.Find ("map_hinoki").GetComponent<UI_button_controller>().disable_detect_desplay_pressed();

					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/menu_UI",  typeof(Transform)) , new Vector3(0,0,-0.6f),Quaternion.identity );
					temp.name="menu_UI";
					temp.parent=transform;
					state="menu";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}else if(input.type=="map_hinoki"){

					if(input.display_pressed){
						Destroy(GameObject.Find ("hinoki_comment_window"));
						GameObject.Find ("map_hinoki").GetComponent<UI_button_controller>().disable_detect_desplay_pressed();
						state="normal";

					}else{
						window_controller window= GameObject.Find ("hinoki_comment_window").GetComponent<window_controller> ();
						window.change_hinoki_text_auto_position(map_shown,
						                                        save_data.get_stage_cleared());
					}

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				
				}


				
			}
			
			
			
			
			
			
			
			
			
		}else if(type=="shop"){
			if(input.type=="back_to_map"){

				if(state!="map_start_waiting"){

					FadeManager.Instance.LoadLevel("map", 0.5f);

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					state="map_start_waiting";
				}

			}else if(state=="normal"){
				
				if(input.type=="shop_item_box"){

					if(input.item_type!="null"){

						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text=
							item_data.item_name(input.item_type)+"は\n"+item_data.item_price_text(input.item_type)+"ゼニ。";


						Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/shop_yes_no_UI",  typeof(Transform)) , new Vector3(0,32,0),Quaternion.identity );
						temp.name="shop_yes_no_UI";
						temp.parent=GameObject.Find ("shop_UI").transform;

						previous_item_type=input.item_type;
						previous_slot=input.item_slot_no;
						
						state="shop_buy";

						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

					}
				
					
				}else if(input.type=="show_item_list"){

					Destroy(GameObject.Find ("shop_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				
				}
				
			}else if(state=="shop_buy"){

				if(input.type=="shop_buy_yes"){

					string result=save_data.buy_item(previous_item_type);

					if(result=="success"){
						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="まいどありゼニ！"; 
						Destroy (GameObject.Find ("shop_yes_no_UI"));
						state="normal";


						GameObject.Find ("money_UI").GetComponent<money_UI_controller>().refresh_money();


						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("buy");
					
					}else if(result=="waiting"){

						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="こうにゅうてつづきをするゼニ。"; 

						Destroy (GameObject.Find ("shop_yes_no_UI"));
						state="normal";

					}else if(result=="no_money"){

						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="おかねがたりないゼニ！"; 
						Destroy (GameObject.Find ("shop_yes_no_UI"));
						state="normal";
						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("error");
					}else if(result=="slot_full"){
						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="もちものがいっぱいゼニ！"; 
						Destroy (GameObject.Find ("shop_yes_no_UI"));
						state="normal";
						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("error");

					}else if(result=="match_fail"){

						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="まおうをたおしてから\nでなおしてくるゼニ！"; 
						Destroy (GameObject.Find ("shop_yes_no_UI"));
						state="normal";


						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("error");
					}else if(result=="match_success"){

						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="まいどありゼニ！"; 
						Destroy (GameObject.Find ("shop_yes_no_UI"));

						state="match_start_waiting";

						GameObject.Find ("money_UI").GetComponent<money_UI_controller>().refresh_money();
						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("buy");


						save_data.SetInt ("stage_no", -7);
						FadeManager.Instance.LoadLevel("battle", 1.0f);
					
					}else if(result=="secret"){

						//show_shop_secret(input);

						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="ラスボスは　ラストまえに\nたおすゼニ！"; 
						Destroy (GameObject.Find ("shop_yes_no_UI"));
						state="normal";

						GameObject.Find ("money_UI").GetComponent<money_UI_controller>().refresh_money();

						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("buy");
						//state="";

					}else if(result=="ticket"){
						
						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="まいどありゼニ！"; 
						Destroy (GameObject.Find ("shop_yes_no_UI"));
						
						state="match_start_waiting";
						
						GameObject.Find ("money_UI").GetComponent<money_UI_controller>().refresh_money();
						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("buy");
						
						save_data.SetInt ("stage_no", -9);
						FadeManager.Instance.LoadLevel("battle", 1.0f);
					}


				}else if(input.type=="shop_buy_no"){
					int rnd=Random.Range (0,7);
					if(rnd==0){

						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="あんたはケチゼニ。";

					}else if(rnd==1){
						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="ひやかしならかえれゼニ。";

					}else if(rnd==2){
						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="ねびきはしないゼニよ。";
					}else if(rnd==3){
						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="おきゃくさん、びんぼうゼニ？";
					}else if(rnd==4){
						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="……チッ。";
					}else if(rnd==5){
					
						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="ガクッ。";
					}else if(rnd==6){
						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="おこるぞ　コラァ！";

					}else{
						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text="なにかかってくれゼニ。";

					}
					Destroy (GameObject.Find ("shop_yes_no_UI"));
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="shop_item_about"){

					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/about_item_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.name="about_UI";
					temp.parent=GameObject.Find ("shop_UI").transform;
					temp.GetComponent<about_item_UI_controller>().set_text (previous_item_type);
					
					state="shop_item_about";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="shop_item_box"){

					if(input.item_type!="null"){
					
						Destroy(GameObject.Find("shop_yes_no_UI"));

						GameObject.Find ("shop_UI").transform.FindChild("window").transform.FindChild("text_mesh").GetComponent<TextMesh>().text=
							item_data.item_name(input.item_type)+"は\n"+item_data.item_price_text(input.item_type)+"ゼニ。";

						Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/shop_yes_no_UI",  typeof(Transform)) , new Vector3(0,32,0),Quaternion.identity );
						temp.name="shop_yes_no_UI";
						temp.parent=GameObject.Find ("shop_UI").transform;
				
						previous_item_type=input.item_type;
						previous_slot=input.item_slot_no;
						
						state="shop_buy";

						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
						
					}
					
		
				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("shop_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}




			}else if(state=="shop_item_about"){
				if(input.type=="close_about_UI"){

					Destroy(GameObject.Find("about_UI"));
					state="shop_buy";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}
			



			}else if(state=="shop_secret"){

				Destroy(GameObject.Find("shop_secret_window"));
				//state="normal";

			}else if(state=="show_item_list"){
				
				if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					GameObject.Find ("controller").GetComponent<shop_controller>().show_shop_item_list_again();
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				
				} else if (input.type=="item_box"){
					
					item_selected(input);
					
				}

			}else if(state=="item_selected"){
				
				if(input.type=="equipment_yes"){

					save_data.SetInt("sword_equipped", previous_slot);
					Destroy(input_transform.parent.parent.gameObject);
					
					GameObject.Find ("controller").GetComponent<item_data_controller>().refresh_equipment();
					Destroy (GameObject.Find ("item_box_selected"));
					
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="use_item"){

					Destroy(GameObject.Find ("item_UI"));
					GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list_for_use(previous_item_type,previous_slot);
					state="use_item";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");


				}else if(input.type=="about"){
					
					if(previous_item_type=="sword"){
						
						Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/about_sword_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
						temp.name="about_UI";
						temp.parent=GameObject.Find ("item_UI").transform;
						temp.GetComponent<about_sword_UI_controller>().set_text(
							previous_sword_no,save_data.get_power_up(previous_slot));

						state="about";

						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

					}else if(save_data.is_item (previous_item_type)){
						
						Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/about_item_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
						temp.name="about_UI";
						temp.parent=GameObject.Find ("item_UI").transform;
						temp.GetComponent<about_item_UI_controller>().set_text (previous_item_type);
						
						state="about";
						
						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
						
					}

				}else if(input.type=="replace"){
					
					Destroy (GameObject.Find ("item_selected_UI"));
					state="replace";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if (input.type=="close_item_selected_UI"){
					
					Destroy (GameObject.Find ("item_selected_UI"));
					Destroy (GameObject.Find ("item_box_selected"));
					state="show_item_list";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					GameObject.Find ("controller").GetComponent<shop_controller>().show_shop_item_list_again();
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}
			

			}else if(state=="use_item"){



				if (input.type=="item_box_for_use"){


					use_item(input);



				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					GameObject.Find ("controller").GetComponent<shop_controller>().show_shop_item_list_again();
					state="normal";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				
				}


			}else if(state=="replace"){
				if (input.type=="item_box"){

					replace_item(input);

				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					GameObject.Find ("controller").GetComponent<shop_controller>().show_shop_item_list_again();

					state="normal";
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
				}
				
			}else if(state=="about"){
				
				if (input.type=="close_about_UI"){
					
					Destroy (GameObject.Find ("about_UI"));
					state="item_selected";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}else if(input.type=="show_item_list"){
					
					Destroy(GameObject.Find ("item_UI"));
					GameObject.Find ("controller").GetComponent<shop_controller>().show_shop_item_list_again();
					state="normal";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

				}

			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		}else if(type=="title"){
			
			if(state=="title"){
				if(input.type=="title_continue"){
					
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("start");
					FadeManager.Instance.LoadLevel("map",1.0f);
					state="title_start_waiting";
					
				}else if(input.type=="title_new_game"){
					
					if(PlayerPrefs.HasKey ("is_initialized")){
					
						Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/title/2nd_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
						temp.parent=GameObject.Find("start_UI").transform;
						temp.localPosition= new Vector3(0,0,0);
						state="title_delete";

						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

						Destroy(input_transform.parent.gameObject);

					}else{
						PlayerPrefs.DeleteAll();
						save_data.initialize();
						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("start");
						state="title_start_waiting";
						FadeManager.Instance.LoadLevel("battle",1.0f);

					}
					
					
				}else if(input.type=="title_about"){

					Application.OpenURL("http://stimuli.jp/");
				}
				
			} else if(state=="title_delete"){
				if(input.type=="title_delete_yes"){

					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/title/3rd_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find("start_UI").transform;
					temp.localPosition= new Vector3(0,0,0);
					state="title_delete_confirm";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					Destroy(input_transform.parent.gameObject);
				}else if(input.type=="title_delete_no"){

					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/title/1st_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find("start_UI").transform;
					temp.localPosition= new Vector3(0,0,0);
					state="title";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

					Destroy(input_transform.parent.gameObject);
				}
				
				
			}else if(state=="title_delete_confirm"){
				
				if(input.type=="title_delete_confirm_yes"){

					PlayerPrefs.DeleteAll();
					save_data.initialize();
					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("start");
					FadeManager.Instance.LoadLevel("battle",1.0f);
					state="title_start_waiting";

				}else if(input.type=="title_delete_confirm_no"){
										
					Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/title/1st_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find("start_UI").transform;
					temp.localPosition= new Vector3(0,0,0);
					state="title";

					GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
					
					Destroy(input_transform.parent.gameObject);
				}
				
			}
		}
		
	}

	public void use_item(UI_button_controller input){

		string result=save_data.use_item(previous_item_type,input.item_slot_no,previous_slot);
		Destroy(GameObject.Find ("item_UI"));

		Transform parent =GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();

		state="show_item_list";
		
		Transform temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/etc/up",  typeof(Transform)) , input.transform.position,Quaternion.identity );
		temp.parent = parent;
		temp=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/text/sword_up_count",  typeof(Transform)) , input.transform.position-new Vector3(0,-8,0),Quaternion.identity );
		temp.parent = parent;
		//temp.renderer.sortingLayerName="text_2";
		temp.GetComponent<TextMesh>().text=result;

		GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("power_up");
	}

	public void item_selected(UI_button_controller input){


		if(input.item_type=="sword"){

			bool cannot_equip_flag = false;
			
			if(type=="battle"){
				
				cannot_equip_flag = GameObject.Find ("hero").GetComponent<hero_controller>().is_using_sword;
			}

				if(save_data.GetInt("sword_equipped")==input.item_slot_no||cannot_equip_flag){

				Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/equipment_no_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
				temp.parent=GameObject.Find ("item_UI").transform;
				temp.name="item_selected_UI";
				
				previous_slot=input.item_slot_no;
				previous_item_type=input.item_type;
				previous_sword_no=input.item_sword_no;
				
				
				state="item_selected";

			
				}else{
					
					Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/equipment_yes_no_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
					temp.parent=GameObject.Find ("item_UI").transform;
					temp.name="item_selected_UI";
					previous_slot=input.item_slot_no;
					previous_item_type=input.item_type;
					previous_sword_no=input.item_sword_no;
					
					state="item_selected";
					
				}
		
		
		
		
		}else if(save_data.is_item (input.item_type)){
			
			Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/use_item_UI",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
			temp.parent=GameObject.Find ("item_UI").transform;
			temp.name="item_selected_UI";
			previous_slot=input.item_slot_no;
			previous_item_type=input.item_type;
			previous_sword_no=input.item_sword_no;
			
			state="item_selected";
			
		}
	
		GameObject[] gs= GameObject.FindGameObjectsWithTag("item_box_selected");
		
		foreach(GameObject g in gs){
			Destroy (g);
		}
		
		GameObject[] temps=GameObject.FindGameObjectsWithTag("item_box");
		
		for (int i=0; i<temps.Length; i++) {
			if(temps[i].GetComponent<UI_button_controller>()!=null){
				if(temps[i].GetComponent<UI_button_controller>().item_slot_no==input.item_slot_no){
					
					
					Transform selected=(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/common_windows/selected",  typeof(Transform)) , temps[i].transform.position,Quaternion.identity );
					selected.name="item_box_selected";
					selected.parent=GameObject.Find ("item_UI").transform;
				}
			}
			
		}
		
		GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select"); 
	


	}


	public void disable_stop_enemies(){

		//GameObject.Find ("hero").GetComponent <hero_controller>().disable_is_event();
		
		GameObject[] gc=GameObject.FindGameObjectsWithTag("enemy");
		
		foreach (GameObject g in gc){
			g.GetComponent<enemy_controller>().disable_stop();
		}
	}
	public void replace_item(UI_button_controller input){

		save_data.replace_item(previous_slot,input.item_slot_no);
		
		GameObject[] gs= GameObject.FindGameObjectsWithTag("item_box_selected");
		
		foreach(GameObject g in gs){
			Destroy (g);
		}
		
		state="show_item_list";
		
		GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
	}

	public void show_shop_secret(UI_button_controller input){
		
		input.enable_detect_desplay_pressed();
		input.display_pressed_to="OnMouseUpAsButton";
		
		Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
		temp.parent=GameObject.Find ("UI").transform;
		temp.name="shop_secret_window";
		window_controller window= temp.GetComponent<window_controller> ();
		
		window.show_window(8,3,new Vector3(0,0,0), "window_3", 0.0f);
		
		window.set_shop_secret_text_auto_position("text_3");
		window.add_hinoki_next_text_button(new Vector3(56,-16,0));
		
		
		state="shop_secret";
		GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
	}

	public void show_hinoki_comment(UI_button_controller input){

		input.enable_detect_desplay_pressed();
		input.display_pressed_to="OnMouseUpAsButton";
		
		Transform temp =(Transform) Instantiate(( Transform) Resources.Load ("prefab/UI/window/window",  typeof(Transform)) , new Vector3(0,0,0),Quaternion.identity );
		temp.parent=GameObject.Find ("UI").transform;
		temp.gameObject.layer = 5;
		temp.name="hinoki_comment_window";
		window_controller window= temp.GetComponent<window_controller> ();
		
		window.show_window(8,3,new Vector3(0,0,0), "window_2", 0.0f);
		
		window.set_hinoki_text_auto_position(map_shown,
		                                     save_data.get_stage_cleared(),"text_2");
		window.add_hinoki_next_text_button(new Vector3(56,-16,0));
		
		
		state="hinoki_comment";
		GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
	}
	

	public void UI_OnMouseDrag(UI_button_controller input, Transform input_transform){
		
		if(type=="battle"){
			if(state=="show_item_list_pressed"){
				if(input.type=="show_item_list"){
					if(!is_battle_equipment_button_long_pressed){
						if(Time.time-battle_equipment_button_press_start_time>=1){
							is_battle_equipment_button_long_pressed=true;
							GameObject.Find ("hero").GetComponent<hero_controller>().start_using_sword();
							//state="equipment";
						}
					}else{
						
					}
					
				}
			}
			
			
			
		} else if (type=="map"){
			if (state == "normal") {
				if(input.type=="map_rotate"){
					
					Vector3 temp=Input.mousePosition;
					bool enable_up=true;

					if(map_shown>1){
						if(temp.x-button_pressed_position.x<-next_map_length){
							//print (temp.x-button_pressed_position.x);
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("left","selected");
							enable_up=false;
						}else{
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("left","pressed");

						}
					}
					
					if(map_shown<max_map_no){
						if(temp.x-button_pressed_position.x>next_map_length){
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("right","selected");
							enable_up=false;
						}else{
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("right","pressed");

						}
					}

					if(map_shown==8&&save_data.get_stage_cleared()==32){
						if(temp.y-button_pressed_position.y>next_map_length&&enable_up){
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("up","selected");
						}else{
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("up","pressed");
						}
					}


				}
				
			}
		}
	}
	
	public void UI_OnMouseDown(UI_button_controller input, Transform input_transform){
		if(type=="battle"){
			if(state=="normal"){
				if(input.type=="show_item_list"){
					is_battle_equipment_button_long_pressed=false;
					battle_equipment_button_press_start_time=Time.time;
					state="show_item_list_pressed";
				}
				
			}else if(state=="battle_menu"){
				if(input.type=="show_item_list"){
					state="battle_menu_show_item_list_pressed";
				}
			}
			
			
		}else if (type=="map"){
			if (state == "normal") {
				if(input.type=="map_rotate"){
					
					//transform.FindChild ("map_cube").GetComponent<map_cube_controller>().start_rotation (input.type,1);
					//state="map_rotate_right";
					//print ("down");
					
					button_pressed_position=Input.mousePosition;
					if(map_shown>1){
						GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("left","pressed");
					}
					if(map_shown<max_map_no){
						GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("right","pressed");
					}
					
				}
				
			}
		}
	}
	
	public void UI_OnMouseUp(UI_button_controller input, Transform input_transform){
		if(type=="battle"){
			if(state=="show_item_list_pressed"){
				if(input.type=="show_item_list"){
					
					if(!is_battle_equipment_button_long_pressed){
						
						GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
						Time.timeScale=0;
						state="show_item_list";

						GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");

					}else{
						is_battle_equipment_button_long_pressed=false;
						
						state="normal";
					}
				}
			}else if(state=="show_item_list"){

			}else if(state=="battle_menu_show_item_list_pressed"){
				Destroy (GameObject.Find ("battle_menu_UI"));
				GameObject.Find ("controller").GetComponent<item_data_controller>().show_item_list();
				state="show_item_list";

				GameObject.Find ("SE").GetComponent<SE_controller> ().play_audio ("select");
			}
			
			
		}else if (type=="map"){
			if(state=="normal"){
				if(input.type=="map_rotate"){
					
					//transform.FindChild ("map_cube").GetComponent<map_cube_controller>().start_rotation (input.type,1);
					//state="map_rotate_right";
					
					Vector3 temp=Input.mousePosition;
					bool enable_up=true;
					if(temp.x-button_pressed_position.x<-next_map_length){
						if(map_shown>1){
							state="map_rotate_left";
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("left","unpressed");
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("right","unpressed");
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("up","unpressed");
							map_cube.GetComponent<map_cube_controller>().start_rotation ("map_rotate_left",map_shown);
							map_shown--;
							enable_up=false;

						}
					}else if(temp.x-button_pressed_position.x>next_map_length){
						
						if(map_shown<max_map_no){
							state="map_rotate_right";
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("left","unpressed");
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("right","unpressed");
							GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("up","unpressed");
							map_cube.GetComponent<map_cube_controller>().start_rotation ("map_rotate_right",map_shown);
							map_shown++;
							enable_up=false;


						}
					}

					if(map_shown==8&&save_data.get_stage_cleared()==32&&temp.y-button_pressed_position.y>next_map_length&&enable_up){
						state="map_move_up";
						GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("left","unpressed");
						GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("right","unpressed");
						GameObject.Find ("next_map").GetComponent<next_map_controller>().change_state("up","unpressed");
						map_cube.GetComponent<map_cube_controller>().move_up();

					}

					//print ("UP");
				}
			}	
		}
		
	}
	
	
	
	
	
}
