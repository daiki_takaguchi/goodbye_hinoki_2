﻿using UnityEngine;
using System.Collections;

public class level_data_controller : MonoBehaviour {

	private float level_up_rate=1.2f;
	public long next_level_up_exp=10;
	public save_data_controller save_data;

	// Use this for initialization
	void Start () {
		//save_data = GetComponent<save_data_controller> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public int get_data(string type,int level){
		
		int output = 0;
	

		if(type=="HP"){
			output=20+(level-1)*5;
		}else if(type=="attack_point"){
			output=10 +(level-1);
		}
		
		return output;
	}

	public int get_level(){
		//print (save_data.get_total_exp ());
		return total_exp_to_level(save_data.get_total_exp ());
	}

	public int get_max_HP(){

		return total_exp_to_max_HP(save_data.get_total_exp ());
	}

	public int get_attack_point(){
		return total_exp_to_attack_point(save_data.get_total_exp ());
	}

	public long get_total_exp(){
		return save_data.get_total_exp ();
	}

	public long get_next_level_up_exp(){
		return total_exp_to_next_level_up_exp(save_data.get_total_exp ());
	}

	public int total_exp_to_max_HP(long total_exp){
		int level = 0;
		long remaining_exp = total_exp;
		//int  next_level_up_exp = 10;
		
		do {
			remaining_exp-=level_to_next_level_up_exp(level);
			level++;
		} while (remaining_exp>=0);

		return 20+(level-1)*5;

	}

	public int total_exp_to_attack_point(long total_exp){
		int level = 0;
		long remaining_exp = total_exp;
		//int next_level_up_exp = 10;
		
		do {
			remaining_exp-=level_to_next_level_up_exp(level);
			level++;
		} while (remaining_exp>=0);
		
		return 30+(level*4-4);
		
	}

	public int total_exp_to_level(long total_exp){

		int level = 0;
		long remaining_exp = total_exp;
		//int next_level_up_exp = 10;

		do {
			remaining_exp-=level_to_next_level_up_exp(level);
			level++;
		} while (remaining_exp>=0);


		return level;

	}


	public long total_exp_to_next_level_up_exp(long total_exp){
		//return total_exp-PlayerPrefs.GetInt ("level")

		int level=0;
		long remaining_exp = total_exp;
		//int next_level_up_exp = 10;
		
		do {
			remaining_exp-=level_to_next_level_up_exp(level);
			level++;
		} while (remaining_exp>=0);
		
		
		return -remaining_exp;
	}

	public long level_to_next_level_up_exp(int level ){
		return (long) Mathf.Round(next_level_up_exp*Mathf.Pow(level_up_rate,(float) level));
	}
	
}
