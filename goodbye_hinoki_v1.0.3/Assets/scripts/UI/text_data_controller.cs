﻿using UnityEngine;
using System.Collections;

public class text_data_controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public string get_hinoki_comment(int map_no,int stage_cleared){

		string text = "ひのきのぼう：\n……。";
		int prob = 4;
		if(GameObject.Find ("controller").GetComponent<save_data_controller>().GetString ("map_hinoki")=="sword_001_04"){
			text = "しゃべらないひのきのぼう：\n……。";
		}else if(GameObject.Find ("controller").GetComponent<save_data_controller>().GetString ("map_hinoki")=="sword_001_02"){
			text = "よごれちまった ひのきくん：\n……。";
		}else{


			if(map_no==1&&stage_cleared==4){

				text="ひのきくん：\nマップをフリックで\nつぎのワールドにいけるぞ。";
				prob=1;
			}else if(map_no==1){
				
				text="ひのきくん：\nメニューがめんで\nいろいろ　せっていできるぞ。";

			}else if(map_no==2&&stage_cleared>=5){

				text="ひのきくん：\nあのバッチィやつ、きっと\nどこかに　ひぞんでいるぞ！";
			
			//}else if(map_no==3&&stage_cleared>=11){
			//	text="ひのきくん：\nみせのおっさんは　じぶんのキャラを\nはやらせてひともうけしたいらしい。";
			/*
			}else if(map_no==4){
			
				text="ひのきくん：\nきもちわるい　てきは\nはやくたおそうぜ。";
			}else if(map_no==5){
				text="ひのきくん：\nこおりには  ひを\nくらわせてやろうぜ。";
			}else if(map_no==6){
				text="ひのきくん：\nゾンビには　せいなるけんが\nききそうだな。";
			}else if(map_no==7){
				text="ひのきくん：\nてつには　てつを\nぶつけてやろうぜ！";
			}else if(map_no==8){
				text="ひのきくん：\nたまには　やみのけんを\nつかってみたいな。";
			*/
			}else{
				text="ひのきくん：\nメニューがめんで\nいろいろ　せっていできるぞ。";
				prob=100000;
			}


			if(Random.Range(0,prob)==0){

			}else{
				int temp = get_next_comment();
				text= get_data ("hinoki_comment_"+temp);
			}

		
		
		}

		return text;
	}

	public int get_next_comment(){
		int output = 1;
		int max_comment = 33;

		if(PlayerPrefs.HasKey("hinoki_comment")){
			output=PlayerPrefs.GetInt("hinoki_comment")+1;

			if(output> max_comment){
				output=1;
			}

			PlayerPrefs.SetInt("hinoki_comment",output);
		}else{
			PlayerPrefs.SetInt("hinoki_comment",1);
		}

		return output;

	}

	public string get_data_animation(string text_label,out string output_who, out string output_ref){
	
		return get_data_original (text_label, out output_who, out output_ref);
	}


	public string get_data(string text_label){

		string out_1;
		string out_2;

		return get_data_original(text_label,out out_1, out out_2);
	}


	public string get_data_original(string text_label,out string output_who, out string output_ref){

		string output_text = "";

		string hero_name = PlayerPrefs.GetString ("hero_name");
		
		string who = "";

		if (text_label=="equipment"){
			output_text ="そうびしますか？";
		}else if(text_label=="yes"){
			output_text = "";
		}else if(text_label=="no"){
			output_text = "";
		}else if(text_label=="tutorial_1"){

			who=hero_name;
			output_text ="ハァ、ハァ……。\nしつこいやつらだ。";
		
		}else if(text_label=="tutorial_2"){

			who=hero_name;
			output_text ="くっ、かこまれてしまった。\nもうダメだ……。";

		}else if(text_label=="tutorial_3"){

			who="？";
			output_text ="あきらめるんじゃねぇ！";

		}else if(text_label=="tutorial_4"){

			who=hero_name;
			output_text ="（……？！ どこからか\nこえがきこえる？！）";

		}else if(text_label=="tutorial_5"){

			who="？";
			output_text ="あきらめるんじゃねぇって\nいってんだよ！";

		}else if(text_label=="tutorial_6"){

			who="？";
			output_text ="ジャジャーン！！";

		}else if(text_label=="tutorial_7"){

			who=hero_name;
			output_text ="なっ、ぼうがしゃべるとは\nあやしいやつ！ ";

		}else if(text_label=="tutorial_8"){

			who=hero_name;
			output_text ="きさまから たいじしてくれる！!";

		}else if(text_label=="tutorial_9"){

			who="？";
			output_text ="ちっがーう！\nみ・か・た！";
		
		}else if(text_label=="tutorial_10"){
			
			who="？";
			output_text ="さっきまで ピンチだったんじゃ\nないんかい！";
		
		}else if(text_label=="tutorial_11"){
			
			who=hero_name;
			output_text ="おお、そうだった……。";

		}else if(text_label=="tutorial_12"){

			who="？"; 
			output_text ="ほら、ぼうけんのはじめに てにはいる\nそうび あるじゃないですか。";

		}else if(text_label=="tutorial_13"){
			
			who=hero_name;
			output_text ="てつのけんとか、ひのきのぼうとか？";
		}else if(text_label=="tutorial_14"){

			who="ひのきのぼう";
			output_text ="そうそう！\nそれ、オレ。ひのきのぼう！！";

		}else if(text_label=="tutorial_15"){
			
			who=hero_name;
			output_text ="おお、きみが、ひのきのぼうか！";
		
		}else if(text_label=="tutorial_16"){
				
			who=hero_name;
			output_text ="では、かくごしろ！\nひのきのぼう！";

		}else if(text_label=="tutorial_17"){
			
			who="ひのきのぼう";
			output_text ="だから ちっがーう！\nみ・か・た！";

		}else if(text_label=="tutorial_18"){
			
			who="ひのきのぼう";
			output_text ="さぁ、おれをてにもって\nたたかうんだ！";

		

		}else if(text_label=="tutorial_clear_1"){
			who="ひのきのぼう";
			output_text ="ふぅ……。\nやったな！";
		}else if(text_label=="tutorial_dead_1"){
			who="ひのきのぼう";
			output_text ="おお、しんでしまうとは\nなさけない！！";

		}else if(text_label=="tutorial_run_1"){

			who="ひのきのぼう";
			output_text ="このチキンやろうが！！";

		}else if(text_label=="tutorial_clear_2"){

			who="ひのきのぼう";
			output_text ="よし、おまえはこれから\nせかいをめぐり";

		}else if(text_label=="tutorial_clear_3"){
			
			who="ひのきのぼう";
			output_text ="わるいまおうをたおすのだ。";
		

		}else if(text_label=="tutorial_clear_4"){

			who=hero_name;
			output_text ="えー、なんで？";

		}else if(text_label=="tutorial_clear_5"){
			
			who=hero_name;
			output_text ="もんくを　いわない！\nゲームが　はじまらないだろ！";

		}else if(text_label=="tutorial_clear_6"){
			
			who=hero_name;
			output_text ="めんどうくさいなぁ……。\nじゃあ……。";
		
		}else if(text_label=="tutorial_clear_7"){
			
			who=hero_name;
			output_text ="せいじかつどうひの ふせい\nしようの しょうこをつかんで";

		}else if(text_label=="tutorial_clear_8"){

			who=hero_name;
			output_text ="わるいまおうを じにんに\nおいこもうぜ！";

		}else if(text_label=="tutorial_clear_9"){

			who="ひのきのぼう";
			output_text ="なんでやねん！\nふつうにたおさんかい！";


		}else if(text_label=="tutorial_clear_10"){

			who="ひのきのぼう";
			output_text ="とにかく、いくぞ！\nまおうをたおしに！！";

		}else if(text_label=="tutorial_clear_11"){

			who=hero_name;
			output_text ="しかたない、いざゆかん！！\nよろしく、ひのきくん！！";

		}else if(text_label=="tutorial_clear_12"){
			
			who="ひのきくん";
			output_text ="さっそく「くん」づけとは こころの\nきょり グイグイちぢめるね……。";
		
		}else if(text_label=="tutorial_clear_13"){
			
			who="ひのきくん";
			output_text ="こちらこそ、よろしくな！";

		
		}else if(text_label=="map_001_talk_1"){
		
			who=hero_name;
			output_text ="ところで、このゲームには\n"+
				"「ぼうぐ」はないの？";

		}else if(text_label=="map_001_talk_2"){
			
			who="ひのきくん";
			output_text="ない。";

		}else if(text_label=="map_001_talk_3"){

			who=hero_name;
			output_text="てきのこうげきが\n" +
				"けっこう　いたいんですけど。";

		}else if(text_label=="map_001_talk_4"){

			who="ひのきくん";
			output_text ="がまんしろ。";

		}else if(text_label=="map_001_talk_5"){
			who=hero_name;
			output_text="ひのきのたて　とか\n"+
				"てづくりしてみようかな……。"; 

		}else if(text_label=="map_001_talk_6"){

			who=hero_name;
			output_text="ざいりょうはあるし……。";

		}else if(text_label=="map_001_talk_7"){

			who="ひのきくん";
			output_text="て、てきがいるぞ！\n" +
				"さ、さぁ、たたかおう！";
		
		
		
		
		
		
		
		
		
		
		
		
		
		}else if(text_label=="shop_1"){
			output_text = "いらっしゃいゼニ。";
		}else if(text_label=="shop_2"){
			output_text = "どれにするゼニ？";
		}else if(text_label=="unchi_1"){

			who="ひのきくん";
			output_text = "げげっ！";
		
		}else if(text_label=="unchi_2"){

			who="ひのきくん";
			output_text = "アイツは……。";
		
		}else if(text_label=="unchi_3"){

			who="ひのきくん";
			output_text = "バッチィやつだ……。";

		}else if(text_label=="unchi_4"){
			who="ひのきくん";
			output_text = "おい、あたらしいボタンをおして\nそうびをかえろ！";
		}else if(text_label=="unchi_5"){

			who="ひのきくん";
			output_text = "くれぐれもおれをつかって\nアイツをこうげきするなよ！！！";

		}else if(text_label=="unchi_end_clean_1"){

			who="ひのきくん";
			output_text = "よし、よくやった！！";

		}else if(text_label=="unchi_end_clean_2"){

			who="ひのきくん";
			output_text = "これからも、そうびをかえながら\nてきとたたかうんだ！！";

		}else if(text_label=="unchi_end_dirty_1"){

			who="ひのきくん";
			output_text = "バ、バッチくなったじゃないか！\nこのやろー！！！！！！";

		}else if(text_label=="unchi_end_dirty_2"){

			who="ひのきくん";
			output_text = "これからはそうびをかえながら\nてきとたたかうんだ！！";

		}else if(text_label=="tutorial_sword_1"){
			
			who="ひのきくん";
			output_text = "フッフッフ……。";

		}else if(text_label=="tutorial_sword_2"){
				
			who="ひのきくん";
			output_text = "ところで、きみはきづいたかね。\nけんをすてるボタンがないことを。";

		}else if(text_label=="tutorial_sword_3"){
			
			who=hero_name;
			output_text = "さくしゃのミスじゃないの？";
		
		}else if(text_label=="tutorial_sword_4"){
			
			
			who="ひのきくん";
			output_text = "ちがーう！";

		}else if(text_label=="tutorial_sword_5"){

			who="ひのきくん";
			output_text = "じつは　おまえには\nとくしゅなちからがあるのだ。";

		}else if(text_label=="tutorial_sword_6"){

			who="ひのきくん";
			output_text = "けんの　せんざいのうりょくを\nかいほうできるのだ。";

		
		}else if(text_label=="tutorial_sword_7"){
			
			who="ひのきくん";
			output_text = "けんのボタンを　ながおしで\nのうりょくはつどうできるぞ。";

		}else if(text_label=="tutorial_sword_8"){
			
			who= hero_name;
			output_text = "おおー！\nさすがじぶん！";
		
		}else if(text_label=="tutorial_sword_9"){
			
			who= hero_name;
			output_text = "なまえが　しゅじんこう　だから\nできそうなきがしてたんだ。";
		
		}else if(text_label=="tutorial_sword_10"){
			
			who="ひのきくん";
			output_text = "（それをいっちゃあ\nおしめぇよ……。）";

		}else if(text_label=="tutorial_sword_11"){
			
			who="ひのきくん";
			output_text = "けんは、つかったら\nなくなるから　きをつけろよ！";

		}else if(text_label=="shop_help_1"){
			
			who="おっさん";
			output_text = "ぎえー、ゼニ！";

		}else if(text_label=="shop_help_end_1"){
			
			who="おっさん";
			output_text = "いやー、たすかったゼニ。\n";

		}else if(text_label=="shop_help_end_2"){
			
			who="おっさん";
			output_text = "あのでっかいXXXにかこまれて\nこわかったゼニ。";

		}else if(text_label=="shop_help_end_3"){
			
			who="おっさん";
			output_text = "テラフォーナントカもびっくりゼニ。\nありがとうゼニ！";

		}else if(text_label=="shop_help_end_4"){
			
			who="おっさん";
			output_text = "わたしはちかくでおみせを\nやっているものゼニ。";

		}else if(text_label=="shop_help_end_5"){
			
			who="おっさん";
			output_text = "よかったら\nおみせによってくださいゼニ！";
		

		}else if(text_label=="shop_help_end_6"){
			
			who="ひのきくん";
			output_text = "キャラが ヘンな\nおっさんだったな……。";


		}else if(text_label=="shop_help_end_7"){
			
			who=hero_name;
			output_text = "ゆるキャラてきな　なにかを\nめざしてるんじゃないの？";

		}else if(text_label=="shop_help_end_8"){
			
			who="ひのきくん";
			output_text = "いっぱつ　あてると\nデカい　らしいからな……。";
			
			
		}else if(text_label=="shop_help_end_9"){
			
			who="ひのきくん";
			output_text = "まぁ、とにかく\nおみせ、いってみようぜ！";





		}else if(text_label=="last_boss_at_stage_17_1"){
			
			who="ひのきくん";
			output_text = "あ、あいつは……。";

		}else if(text_label=="last_boss_at_stage_17_2"){
			
			who="ひのきくん";
			output_text = "なぜここに……？";
		}else if(text_label=="last_boss_at_stage_17_3"){
			
			who="まおう";
			output_text = "みちにまよった。";
		}else if(text_label=="last_boss_at_stage_17_4"){
			
			who="ひのきくん";
			output_text = "ドジっこか！！";

		}else if(text_label=="last_boss_at_stage_17_5"){
		
			who="ひのきくん";
			output_text = "いまのおまえに かなうあいて\nじゃない。にげろ！！";
		



		}else if(text_label=="map_005_talk_1"){
			
			who="ひのきくん";
			output_text ="ところで、なんとなく\nわかると　おもうが";
			
		}else if(text_label=="map_005_talk_2"){
			
			who="ひのきくん";
			output_text="とくていの　けんや　こうげきに\nよわいてきも　いるぞ。";
			
		}else if(text_label=="map_005_talk_3"){
			
			who=hero_name;
			output_text="なるほど。\nゲームあるあるだね。";
			
		}else if(text_label=="map_005_talk_4"){
			
			who=hero_name;
			output_text ="なのに、ぼうぐはないんだね。";
			
		}else if(text_label=="map_005_talk_5"){

			who="ひのきくん";
			output_text="こだわる　やっちゃな……。"; 





			
		}else if(text_label=="map_006_talk_1"){
			
			who=hero_name;
			output_text ="……。";
			
		}else if(text_label=="map_006_talk_2"){
			
			who=hero_name;
			output_text="…………。";
			
		}else if(text_label=="map_006_talk_3"){
			
			who="ひのきくん";
			output_text="さっきから\nなにをしているんだ？";
			
		}else if(text_label=="map_006_talk_4"){
			
			who=hero_name;
			output_text ="てっとりばやく　おかねを\nかせぎたいから";
			
		}else if(text_label=="map_006_talk_5"){
			
			who=hero_name;
			output_text="スマホで　しらべてたんだ。"; 

		}else if(text_label=="map_006_talk_6"){
			
			who=hero_name;
			output_text="FX　っていうのが\nいいらしいよ。"; 


		}else if(text_label=="map_006_talk_7"){
			
			who="ひのきくん";
			output_text="なんでやねん！"; 
		
		}else if(text_label=="map_006_talk_8"){
			
			who="ひのきくん";
			output_text="そういうときは　ちょっと\nまえの　マップにもどって"; 

		}else if(text_label=="map_006_talk_9"){
			
			who="ひのきくん";
			output_text="よわいてきを　たおすと\nいいぞ。"; 

		}else if(text_label=="map_006_talk_10"){
			
			who="ひのきくん";
			output_text="きいろや　あかいてきは\nすこし　てごわいが"; 

		}else if(text_label=="map_006_talk_11"){
			
			who="ひのきくん";
			output_text="てにいれられる　おかねは\nおおきいぞ。";

		}else if(text_label=="map_006_talk_12"){
			
			who=hero_name;
			output_text="ああ、らくして　かせぎたい……。";



			
		}else if(text_label=="map_007_talk_1"){
			
			who=hero_name;
			output_text ="ところで、このゲームに　かわいい\nヒロインとか　いないの？";

		}else if(text_label=="map_007_talk_2"){
			
			who=hero_name;
			output_text ="イマイチ、モチベーションが\nあがらないんだよね……。";

		}else if(text_label=="map_007_talk_3"){
			
			who="ひのきくん";
			output_text ="もちろん、いるぞ。";

		}else if(text_label=="map_007_talk_4"){
			
			who=hero_name;
			output_text ="おおっ、どこに？";

		}else if(text_label=="map_007_talk_5"){
			
			who="ひのきくん";
			output_text ="ウフッ。";

		}else if(text_label=="map_007_talk_6"){
			
			who=hero_name;
			output_text ="ああ、むしょうに　ぼうぐが\nほしくなってきた……。";

		}else if(text_label=="map_007_talk_7"){
			
			who=hero_name;
			output_text ="さようなら、ひのきくん……。";

		}else if(text_label=="map_007_talk_8"){

			who="ひのきくん";
			output_text="て、てきがいるぞ！\n" +
				"さ、さぁ、たたかおう！";
		

		
		}else if(text_label=="map_008_talk_1"){
			
			who=hero_name;
			output_text ="ここまできて　なんだけど……。";


		}else if(text_label=="map_008_talk_2"){
			
			who=hero_name;
			output_text ="まおうは　どんな\n" +
				"わるいことをしたの？";

		}else if(text_label=="map_008_talk_3"){
			
			who="ひのきくん";
			output_text ="ひどいはなしさ……。";

		}else if(text_label=="map_008_talk_4"){
			
			who="ひのきくん";
			output_text ="はるかなる　むかし……。このよに\nかふんしょうはなかった。";

		}else if(text_label=="map_008_talk_5"){
			
			who="ひのきくん";
			output_text ="われわれ、ひのきや　すぎは\nにんげんと　ともに";

		}else if(text_label=="map_008_talk_6"){
			
			who="ひのきくん";
			output_text ="へいわに　くらしていたのだ。";


		}else if(text_label=="map_008_talk_7"){
			
			who="ひのきくん";
			output_text ="ところが　あるひ　とつぜん\nヤツがあらわれ";


		}else if(text_label=="map_008_talk_8"){
			
			who="ひのきくん";
			output_text ="よに　かふんしょうを\nまんえんさせたのだ。";

		}else if(text_label=="map_008_talk_9"){
			
			who="ひのきくん";
			output_text ="ひとびとは　せき　はなみず\nめのかゆみ　などに　くるしみ";

		}else if(text_label=="map_008_talk_10"){
			
			who="ひのきくん";
			output_text ="ひのきや　すぎは　いみきらわれた。";
		
		}else if(text_label=="map_008_talk_11"){
			
			who=hero_name;
			output_text ="そんなことが……。";


		}else if(text_label=="map_008_talk_12"){
			
			who=hero_name;
			output_text ="ま、まさか　まおうの\nほんみょうって　ハ……。";

		}else if(text_label=="map_008_talk_13"){
			
			who="ひのきくん";
			output_text ="それいじょうは　いうな！\nタツノコプロに　おこられるぞ！";


		}else if(text_label=="map_008_talk_14"){
			
			who="ひのきくん";
			output_text ="さあ、ヤツを たおしにいくぞ！！";

		}else if(text_label=="map_008_talk_15"){
				
			who=hero_name;
			output_text ="わかったで　ごじゃる！";

		
		

		}else if(text_label=="boss_008_end_1"){


			who="ひのきくん";
			output_text = "やったな……。";

		}else if(text_label=="boss_008_end_2"){
			
			who="ひのきくん";
			output_text = "おそらく、つぎのあいてが\nまおうだ！";

		}else if(text_label=="boss_008_end_3"){
			
			who="ひのきくん";
			output_text = "きをひきしめていけよ！！";

		}else if(text_label=="boss_008_end_4"){
			
			who=hero_name;
			output_text = "やっぱり　ぼうぐを……。";

		}else if(text_label=="boss_008_end_5"){
			
			who="ひのきくん";
			output_text = "いらん！";


		}else if(text_label=="boss_009_start_1"){
			
			who="ひのきくん";
			output_text = "ようやくここまできたな。";

		}else if(text_label=="boss_009_run_1"){
			
			who=hero_name;
			output_text = "にげちゃダメだ にげちゃダメだ\nにげちゃダメだ にげちゃダメだ";

		}else if(text_label=="boss_009_start_2"){
			
			who="ひのきくん";
			output_text = "かつぞ！\nあきれめるんじゃねぇぞ！！";
		
		}else if(text_label=="hinoki_used_1"){
			
			who="ひのきくん";
			output_text = "ついにこのときがきたか……。";

		}else if(text_label=="hinoki_used_2"){
			
			who="ひのきくん";
			output_text = "おれのしんのすがたを\nみせるときが……。";
		}else if(text_label=="hinoki_used_3"){
			
			who="ひのきくん";
			output_text = "しかし、わかっていると\nおもうが、";

		}else if(text_label=="hinoki_used_4"){
			
			who="ひのきくん";
			output_text = "けんのせんざいのうりょくを\nかいほうすれば";

		}else if(text_label=="hinoki_used_5"){
			
			who="ひのきくん";
			output_text = "けんは、なくなる……。";

		}else if(text_label=="hinoki_used_6"){
			
			who="ひのきくん";
			output_text = "そういう　うんめいだ……。";

		}else if(text_label=="hinoki_used_7"){
			
			who="ひのきくん";
			output_text = "さらばだ……。\nまた、どこかであおう！";

		}else if(text_label=="hinoki_used_8"){
			
			who="ひのきくん";
			output_text = "このゲームのさくしゃの\nモチベーションしだいだがな！";

		}else if(text_label=="boss_009_end_1"){
				
			output_text = "こうして、しゅじんこうは\nわるいまおうをたおした。";

		}else if(text_label=="boss_009_end_2"){
			
			output_text = "しかし、あの、うるさかった\nひのきのぼうはもういない。";

		}else if(text_label=="boss_009_end_3"){
			
			output_text = "さようなら、ひのきくん。";

		}else if(text_label=="boss_009_end_6"){
			
			output_text = "しゃべらないひのきのぼうを\nてにいれた……。";

		}else if(text_label=="hinoki_comment_1"){
			
			who="ひのきくん";
			output_text = "キティちゃんには\nネコのペットがいるぞ。";

		}else if(text_label=="hinoki_comment_2"){
			
			who="ひのきくん";
			output_text = "コラーゲンの びはだこうかは\nしょうめいされていないぞ。";

		}else if(text_label=="hinoki_comment_3"){
			
			who="ひのきくん";
			output_text = "にほんにも ソビエトがあるぞ。\nくわしくはググれ。";

		}else if(text_label=="hinoki_comment_4"){
			
			who="ひのきくん";

			output_text ="ある　てきをたおすと かならず\nレベルアップするらしい。";

		}else if(text_label=="hinoki_comment_5"){
			
			who="ひのきくん";
			output_text = "うちゅうにいった さいしょの\nせいぶつは ハエらしいぞ。";

		}else if(text_label=="hinoki_comment_6"){
			
			who="ひのきくん";
			output_text = "デングねつの ごげんは\n「ダンディ」らしいぞ。";
		
		}else if(text_label=="hinoki_comment_7"){
			
			who="ひのきくん";
			output_text = "ひのきの「ひ」は\nひみつの「ひ」さ。";
		}else if(text_label=="hinoki_comment_8"){
			
			who="ひのきくん";
			output_text = "ブラッド・ピットとバラク・オバマは\nとおい　しんせきだぞ。";
		}else if(text_label=="hinoki_comment_9"){
			
			who="ひのきくん";
			output_text = "アンパンマンのあんは\nつぶあんだぞ。";

		}else if(text_label=="hinoki_comment_10"){
			
			who="ひのきくん";
			output_text = "ゆせいマジックの らくがきは\nみかんのかわで けせるぞ。";

		}else if(text_label=="hinoki_comment_11"){
			
			who="ひのきくん";

			output_text ="きいろいてきは　つよいぞ。\nあかいてきは　もっとつよいぞ。";

		}else if(text_label=="hinoki_comment_12"){

			
			who="ひのきくん";
			output_text = "おむすびは さんかくけいの\nおにぎりだぞ。";

		}else if(text_label=="hinoki_comment_13"){
			
			who="ひのきくん";
			output_text = "さとうは　くさらないぞ。";

		}else if(text_label=="hinoki_comment_14"){
			
			who="ひのきくん";
			output_text = "カントリーマアムには\nあんこが はいっているぞ。";

		}else if(text_label=="hinoki_comment_15"){
			
			who="ひのきくん";
			output_text = "スチュワーデスは「ぶたのばんにん」\nといういみだぞ。";

		}else if(text_label=="hinoki_comment_16"){
			
			who="ひのきくん";
			output_text = "ガラスを ハサミできりたいときは\nみずのなかで きるといいぞ。";

		}else if(text_label=="hinoki_comment_17"){
			
			who="ひのきくん";
			output_text = "カバのあせは ピンクいろだぞ。\n";

		}else if(text_label=="hinoki_comment_18"){

			who="ひのきくん";
			output_text = "ほのおは ひの せんたんの\nことをいうぞ。";

		}else if(text_label=="hinoki_comment_19"){
			
			who="ひのきくん";
			output_text = "ラムネのたまは エーだまだぞ。";
		}else if(text_label=="hinoki_comment_20"){
			
			who="ひのきくん";
			output_text = "せかいいち おおきいせいぶつは\nキノコだぞ。";
		}else if(text_label=="hinoki_comment_21"){
			
			who="ひのきくん";
			output_text = "「エリーゼのために」は\nテレーゼのためにつくられたぞ。";
		}else if(text_label=="hinoki_comment_22"){
			
			who="ひのきくん";
			output_text = "マウスの　いどうたんいは\n「ミッキー」だぞ。";
		}else if(text_label=="hinoki_comment_23"){
			
			who="ひのきくん";
			output_text = "ひのきの「の」は\nノスタルジーの「の」だ。";

		}else if(text_label=="hinoki_comment_24"){
			
			who="ひのきくん";
			output_text = "カンガルーの　ふくろのなかは\nすごく　くさいぞ。";

		}else if(text_label=="hinoki_comment_25"){
			
			who="ひのきくん";
			output_text = "オリンピックの　マラソンの\nさいちょうきろくは　54ねんだぞ。";

		}else if(text_label=="hinoki_comment_26"){
			
			who="ひのきくん";
			output_text = "アンコールワットには　えどじだいの\nぶしの らくがきがあるぞ。";
		}else if(text_label=="hinoki_comment_27"){
			
			who="ひのきくん";
			output_text = "ひのきの「き」は きぼうの「き」に\nきまってるだろ！";
		}else if(text_label=="hinoki_comment_28"){
			
			who="ひのきくん";
			output_text = "おまえ、みみのうら\nちゃんと あらっているか？";
		}else if(text_label=="hinoki_comment_29"){
			who="ひのきくん";
			output_text = "あめのひは　しっけで\nからだのちょうしがわるいぜ。";
		}else if(text_label=="hinoki_comment_30"){
			who="ひのきくん";
			output_text ="きょうは、ぜっこうの\nひのきびよりだな。";
		}else if(text_label=="hinoki_comment_31"){
			who="ひのきくん";

			output_text = "かのゆうめいな　ほうりゅうじは\nひのきづくりだぞ。";

		}else if(text_label=="hinoki_comment_32"){
			who="ひのきくん";

			output_text = "「シンガポール」は「ししのまち」\nといういみだぞ。";

		}else if(text_label=="hinoki_comment_33"){
			who="ひのきくん";
			output_text ="レベルがあがると　きいろや\nあかのてきに　あいやすくなるぞ。";
		}
		output_who = who + "：\n";
		output_ref=output_text;
		if (who != "") {

			output_text = who + "：\n" + output_text;


		} else {
			output_who ="";
		}




		return output_text;
	}

	/*
	public string text_data_original(int text_no){

		string output_text = "";
		string hero_name = PlayerPrefs.GetString ("hero_name");

		string who = "";

		if (text_no == 0) {

			output_text ="そうびしますか？";

		}else if (text_no == 1) {

			output_text ="はい";

		}else if (text_no == 2) {

			output_text ="いいえ";
		}else if (text_no == 3) {
			who=hero_name;
			output_text ="ハァ、ハァ……。\nしつこいやつらだ。";
		}else if (text_no == 4) {
			who=hero_name;
			output_text ="くっ、かこまれてしまった。\nもうダメだ……。";
		}else if (text_no == 5) {
			who="？";
			output_text ="あきらめるんじゃねぇ！";
		}else if (text_no == 6) {
			who=hero_name;
			output_text ="？？";
		}else if (text_no == 7) {
			who="？";
			output_text ="あきらめるんじゃねぇって\nいってんだよ！！";
		}else if (text_no == 8) {
			who="？";
			output_text ="ジャジャーン！！";
		}else if (text_no == 9) {
			who=hero_name;
			output_text ="……。";
		}else if (text_no == 10) {
			who=hero_name;
			output_text ="（きのぼうがひかって\nしゃべってる……）";
		}else if (text_no == 11) {
			who="？";
			output_text ="キモくない！";
		}else if (text_no == 12) {
			who="？";
			output_text ="いいか、あきらめるんじゃねぇ。\nたたかうんだ！";
		}else if (text_no == 13) {
			who="？";
			output_text ="さぁ、おれをてにもて！！";
		}else if (text_no == 14) {
			who="？";
			output_text ="ふぅ……。やったな！";
		}else if (text_no == 15) {
			who="？";
			output_text ="よし、おまえはこれから\nせかいをめぐり";
		}else if (text_no == 16) {
			who="？";
			output_text ="わるいまおうをたおすのだ。";
		}else if (text_no == 17) {
			who=hero_name;
			output_text ="えぇ！？";
		}else if (text_no == 18) {
			who="？";
			output_text ="えぇ！？　じゃない。";
		}else if (text_no == 19) {
			who="？";
			output_text ="そうしないと、\nゲームがはじまらないだろ。";
		}else if (text_no == 20) {
			who="？";
			output_text ="まぁ、よくあるてんかいだ。";
		}else if (text_no == 21) {
			who="？";
			output_text ="さあ、いざゆかん！！\nまおうをたおしに！";
		}else if (text_no == 22) {
			who=hero_name;
			output_text ="よくわかんないけど……。\n……いざゆかん！！";
		}else if (text_no == 23) {
			who=hero_name;
			output_text ="ところで、きみは\nなんのきのぼう？";
		}else if (text_no == 24) {
			who=hero_name;
			output_text ="ひのきだが……それがなにか？";
		}else if (text_no == 25) {
			who=hero_name;
			output_text ="そっか、よろしく！\nひのきくん！！";
		}else if (text_no == 26) {
			who=hero_name;
			output_text ="（キモい……）"; 
		}


		output_text = who + "：\n" + output_text;



		return output_text;

	}*/
}
