﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;


public class arrows_controller : MonoBehaviour {

	public bool is_one_button;
	public bool is_attack_button;
	public Vector3 clicked_mouse_position;

	//private Vector3 screen_point;
	//private Vector3 offset;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	
	}

	void OnMouseDown() {

		//if (is_attack_button)

		if(GameObject.Find ("UI").GetComponent<UI_controller>().state=="normal"){
			GameObject.Find ("hero").GetComponent<hero_controller> ().attack ();
			clicked_mouse_position=GameObject.Find("Main Camera").camera.ScreenToWorldPoint(Input.mousePosition);
		}
		//}

	}

	void OnMouseDrag() {

		//print (is_attack_button);
		if(GameObject.Find ("UI").GetComponent<UI_controller>().state=="normal"){
			if(Vector3.Distance(GameObject.Find ("arrows").transform.position,GameObject.Find("Main Camera").camera.ScreenToWorldPoint(Input.mousePosition))>=8.0f){


				if(! is_attack_button){
					GameObject.Find ("hero").GetComponent<hero_controller> ().start_move();
				}else{
					if(Vector3.Distance(clicked_mouse_position,GameObject.Find("Main Camera").camera.ScreenToWorldPoint(Input.mousePosition))>3){
						GameObject.Find ("hero").GetComponent<hero_controller> ().start_move();
					}else{
						GameObject.Find ("hero").GetComponent<hero_controller> ().stop_move();
					}
				}
			
			}else{
				GameObject.Find ("hero").GetComponent<hero_controller> ().stop_move ();
			}
			
		}

	}



	void OnMouseUp() {
		if(GameObject.Find ("UI").GetComponent<UI_controller>().state=="normal"){
			GameObject.Find ("hero").GetComponent<hero_controller> ().stop_move();
			GameObject.Find ("hero").GetComponent<hero_controller> ().stop_attack();
		}
	}

}
